# SkittyBot.js
This file is where the global SkittyBot object is created for use in the plugins.
API class has all methods of interest.

# SSL enabled server
The bots server tries to check for the existence of `domain-crt.txt` and `domain-key.txt` before enabling the server, otherwise it defaults to HTTP.
Create those files with your certs and the bot will read them and enable HTTPS access.

# Further setup
If you plan on making the web app public and you're behind a firewall or something make sure both port `80` and `443` are forwarded to your machine so you can access it from the outside.
Running the bot in development mode switches to using ports `5080` amd `5443` instead to avoid collision with an instance of the bot running normally.