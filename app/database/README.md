# The Database Directory
This is where all the bots SQLite databases will be created.
Configurations, guild and channel settings, your bot token. It's all going to be here.

Never share a database as it can contain highly sensitive information that will compromise your bot.