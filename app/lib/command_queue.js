'use strict';

const msg_denied = [
    `You're doing this too much!`,
    `You need to slow down!`,
    `You need to STOP!`,
    `STOP!?`,
    `pls stop...`,
    `You can't keep doing this...`,
    `Can you slow down... please?`,
    `Please slow down...`,
    `IT'S TIME TO STOP`,
    `tbh just stop`,
    `STOP IT!`,
    `I CAN ONLY TAKE SO MUCH!`,
    `I can only handle so much...`
];

class command_queue {

    constructor(concurrency = 1) {
        this.command = [];
        this.queue_check = null;
        this.running = 0;
        this.concurrency = concurrency;

        this.last_check = 0;
        this.check_every = 1000 * 5;
        this.max_time = 1000 * 30;

        this.awaiting = new Map();
        this.users_enqueue = new Map();
    }

    get now() {
        return +new Date();
    }

    init_check() {
        this.queue_check = setInterval(() => {
            if (this.command.length && this.running < this.concurrency) {
                this.run(this.command.shift());
            } else if (!this.command.length && !this.running) {
                clearInterval(this.queue_check);
                this.queue_check = null;
            }
            if ((this.now - this.last_check) > this.check_every) {
                let params;
                for (let key of this.awaiting.keys()) {
                    params = this.awaiting.get(key);
                    if ((this.now - params.msg.createdTimestamp) > this.max_time) {
                        params.msg.channel.stopTyping();
                        this.awaiting.delete(key);
                        --this.running;
                    }
                }
            }
        }, 100);
        return this.queue_check;
    }

    queue(callback, msg, ...args) {
        if (this.concurrency <= 0) {
            msg.reply([
                `I'm sorry, but I can't execute this command right now.`,
                `I'm likely under maintenance or an unforseen bug arose during queuing.`
            ].join('\n'));
            return;
        }
        let pending = this.users_enqueue.get(msg.author.id) || 0;
        if (pending > 5) {
            msg.reply(msg_denied[Math.floor(Math.random() * msg_denied.length)]);
            return;
        }
        this.users_enqueue.set(msg.author.id, ++pending);
        let parameters = { cb: callback, msg: msg, args: args };
        msg.channel.startTyping();
        this.awaiting.set(msg.id, parameters);
        this.command.push(parameters);
        if (!this.queue_check) this.queue_check = this.init_check();
    }

    reduce(msg) {
        let pending = this.users_enqueue.get(msg.author.id);
        if (pending > 0) this.users_enqueue.set(msg.author.id, --pending);
        this.awaiting.delete(msg.id);
        --this.running;
        msg.channel.stopTyping();
    }

    run(cmd) {
        let processed = false;
        ++this.running;
        cmd.cb(cmd.msg, ...cmd.args)
            .catch(err => {
                if (processed) return;
                processed = true;
                this.reduce(cmd.msg);
                if (err) {
                    console.error(err);
                }
            })
            .then(() => {
                if (processed) return;
                processed = true;
                this.reduce(cmd.msg);
            });
    }
}

module.exports = command_queue;
