// https://github.com/Ulflander/compendium-js
(function(compendium){
    'use strict';

    // Prepare namespace
    // Global shortcuts
    var cpd = {},
        lexer = {},
        factory = {},
        analyser = {},
        detectors = {},
        dependencies = {},
        // dependency parsing v2
        parser = {},
        inflector = {},
        pos = {},
        iA = Array.isArray,
        config = {
            profile: {
                negative_threshold: -0.3,
                positive_threshold: 0.3,
                amplitude_threshold: 0.3,
                polite_threshold: 0.2,
                dirty_threshold: 0.3
            },
            parser: ['v1', 'v2']
        },

        // Various types
        T_FOREIGN = 'foreign',
        T_INTERROGATIVE = 'interrogative',
        T_EXCLAMATORY = 'exclamatory',
        T_HEADLINE = 'headline',
        T_IMPERATIVE = 'imperative',
        T_APPROVAL = 'approval',
        T_REFUSAL = 'refusal';


    compendium.detect = detectors;
    compendium.dependencies = dependencies;
    compendium.inflector = inflector;
    compendium.compendium = cpd;
    compendium.lexer = lexer;
    compendium.parser = parser;
    compendium.factory = factory;
    compendium.pos = pos;
    compendium.config = config;

    function extend(target, object) {
        var k;
        for (k in object) {
            if (object.hasOwnProperty(k)) {
                target[k] = object[k];
            }
        };
    };

    // escape regexp specific chars from a string
    function regexpEscape(str) {
        return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&');
    }



/**
 * Analyser - High level utility that goes through the full analysis steps
 * (lexer, PoS, detectors) and returns an analysis result object.
 */

!function() {

    var SUPPORTED_LANGUAGES = ['en', 'fr'];

    analyser.toObject = function(sentence, language) {
        return ;
    };

    analyser.applyPOS = function(s, tokens, language) {
        var i,
            l,
            pos,
            token;

        // Get part of speech
        pos = compendium.tag(tokens, language);

        s.tags = pos.tags;
        s.stats.confidence = pos.confidence;
        for (i = 0, l = tokens.length; i < l; i ++) {
            s.tokens.push(factory.token(tokens[i], pos.norms[i], pos.tags[i]));
        }
        s.length = l;
        return s;
    };

    /**
     * Analyse an array of tokenized sentences.
     *
     * @memberOf compendium.analyser
     * @param  {Array} sentences  Matrix of tokens per sentences
     * @return {Array}            An array of analysis object, one for each sentence
     */
    analyser.analyse = function(raw, language, skipDetectors) {
        var res = [],
            i,
            lexed,
            sentences,
            l,
            d,
            s,
            pos,
            j,
            context,
            m;


        // If provided a string, let's decode and lex it into sentences before analysis
        lexed = lexer.advanced(compendium.decode(raw), language);
        sentences = lexed.sentences;

        // For each sentence
        for (i = 0, l = sentences.length; i < l; i ++) {
            d = Date.now();

            // Convert to object
            s = factory.sentence(lexed.raws[i], language);

            // Apply POS to sentence object
            analyser.applyPOS(s, sentences[i], language);

            // Generate statistics
            compendium.stat(s);

            // Create detectors context
            context = detectors.context();

            // Apply token level detection before dep parsing
            for (j = 0, m = s.tokens.length; j < m; j ++) {
                detectors.apply('t', true, skipDetectors, s.tokens[j], j, s, context);
            }

            detectors.apply('s', true, skipDetectors, s, i, res, context);

            // Create dependency tree
            if (config.parser.indexOf('v1') > -1) {
                dependencies.parse(s);
            }

            if (config.parser.indexOf('v1') > -1) {
                // Dependency parsing v2
                parser.parse(s);
            }

            // Apply token level detection
            for (j = 0, m = s.tokens.length; j < m; j ++) {
                detectors.apply('t', false, skipDetectors, s.tokens[j], j, s, context);
            }

            res.push(s);

            detectors.apply('s', false, skipDetectors, s, i, res, context);

            s.time = Date.now() - d;
        }
        return res;
    };

    /**
     * Process a text and returns an analysis object.
     *
     * @memberOf compendium
     * @param  {String} o A string that will be tokenized by the lexer
     * @param {String} language Language of given text
     * @return {Array}    An array of analysis objects, one for each sentence of the given text
     */
    compendium.analyse = function(o, language, skipDetectors) {
        var result = null;

        language = language || 'en';
        if (SUPPORTED_LANGUAGES.indexOf(language) === -1) {
            throw new Error('Compendium supports only the following languages: ' + SUPPORTED_LANGUAGES.join(', '));
        }

        result = analyser.analyse(o, language, skipDetectors);
        detectors.apply('p', false, skipDetectors, result);

        return result;
    };
}();



/**
 * Compendium (NLP knowledge base)
 */

!function() {
    extend(cpd, {

        // Regular verbs
        // Conjugated and expanded in compendium.parser.js
        verbs: 'accept add admire admit advise afford agree alert allow amuse analyse analyze announce annoy answer apologise appear applaud appreciate approve argue arrange arrest arrive ask attach attack attempt attend attract avoid back bake balance ban bang bare bat bathe battle beam beg behave belong bleach bless blind blink blot blush boast boil bolt bomb book bore borrow bounce bow box brake branch breathe bruise brush bubble bump burn bury buzz calculate call camp care carry carve cause challenge change charge chase cheat check cheer chew choke chop claim clap clean clear clip close coach coil collect colour comb command communicate compare compete complain complete concentrate concern confess confuse connect consider consist contain continue copy correct cough count cover crack crash crawl cross crush cry cure curl curve cycle dam damage dance dare decay deceive decide decorate delay delight deliver depend describe desert deserve destroy detect develop disagree disappear disapprove disarm discover dislike divide double doubt drag drain dream dress drip drop drown drum dry dust earn educate embarrass employ empty encourage end enjoy enter entertain escape examine excite excuse exercise exist expand expect explain explode extend face fade fail fancy fasten fax fear fence fetch file fill film fire fit fix flap flash float flood flow flower fold follow fool force form found frame frighten fry gather gaze glow glue grab grate grease greet grin grip groan guarantee guard guess guide hammer hand handle hang happen harass harm hate haunt head heal heap heat help hook hop hope hover hug hum hunt hurry identify ignore imagine impress improve include increase influence inform inject injure instruct intend interest interfere interrupt introduce invent invite irritate itch jail jam jog join joke judge juggle jump kick kill kiss kneel knit knock knot label land last laugh launch learn level license lick lie lighten like list listen live load lock long look love man manage mark marry match mate matter measure meddle melt memorise mend mess up milk mine miss mix moan moor mourn move muddle mug multiply murder nail name need nest nod note notice number obey object observe obtain occur offend offer open order overflow owe own pack paddle paint park part pass paste pat pause peck pedal peel peep perform permit phone pick pinch pine place plan plant play please plug point poke polish pop possess post pour practise practice pray preach precede prefer prepare present preserve press pretend prevent prick print produce program promise protect provide pull pump punch puncture punish push question queue race radiate rain raise reach realise receive recognise record reduce reflect refuse regret reign reject rejoice relax release rely remain remember remind remove repair repeat replace reply report reproduce request rescue retire return rhyme rinse risk rob rock roll rot rub ruin rule rush sack sail satisfy save scare scatter scold scorch scrape scratch scream screw scribble scrub seal search separate serve settle shade share shave shelter shiver shock shop shrug sigh sign signal sin sip ski skip slap slip slow smash smell smile smoke snatch sneeze sniff snore snow soak soothe sound spare spark sparkle spell spill spoil spot spray sprout squash squeak squeal squeeze stain stamp stare start stay steer step stir stitch stop store strap strengthen stretch strip stroke stuff subtract succeed suck suffer suggest suit supply support suppose surprise surround suspect suspend switch talk tame tap taste tease telephone tempt terrify test thank thaw tick tickle tie time tip tire touch tour tow trace trade train transport trap travel treat tremble trick trip trot trouble trust try tug tumble turn twist type undress unemploy unfasten unite unlock unpack untidy use vanish visit wail wait walk wander want warm warn wash waste watch water wave weigh welcome whine whip whirl whisper whistle wink wipe wish wobble wonder work worry wrap wreck wrestle wriggle yawn yell zip zoom'.split(' '),

        // Irregular verbs
        irregular: 'abide abode/abided abode/abided/abidden abides abiding	alight alit/alighted alit/alighted alights alighting	arise arose arisen arises arising	awake awoke awoken awakes awaking	be was/were been is being	bear bore born/borne bears bearing	beat beat beaten beats beating	become became become becomes becoming	begin began begun begins beginning	behold beheld beheld beholds beholding	bend bent bent bends bending	bet bet bet bets betting	bid bade bidden bids bidding	bid bid bid bids bidding	bind bound bound binds binding	bite bit bitten bites biting	bleed bled bled bleeds bleeding	blow blew blown blows blowing	break broke broken breaks breaking	breed bred bred breeds breeding	bring brought brought brings bringing	broadcast broadcast/broadcasted broadcast/broadcasted broadcasts broadcasting	build built built builds building	burn burnt/burned burnt/burned burns burning	burst burst burst bursts bursting	bust bust bust busts busting	buy bought bought buys buying	cast cast cast casts casting	catch caught caught catches catching	choose chose chosen chooses choosing	clap clapped/clapt clapped/clapt claps clapping	cling clung clung clings clinging	clothe clad/clothed clad/clothed clothes clothing	come came come comes coming	cost cost cost costs costing	creep crept crept creeps creeping	cut cut cut cuts cutting	dare dared/durst dared dares daring	deal dealt dealt deals dealing	dig dug dug digs digging	dive dived/dove dived dives diving	do did done does doing	draw drew drawn draws drawing	dream dreamt/dreamed dreamt/dreamed dreams dreaming	drink drank drunk drinks drinking	drive drove driven drives driving	dwell dwelt dwelt dwells dwelling	eat ate eaten eats eating	fall fell fallen falls falling	feed fed fed feeds feeding	feel felt felt feels feeling	fight fought fought fights fighting	find found found finds finding	fit fit/fitted fit/fitted fits fitting	flee fled fled flees fleeing	fling flung flung flings flinging	fly flew flown flies flying	forbid forbade/forbad forbidden forbids forbidding	forecast forecast/forecasted forecast/forecasted forecasts forecasting	foresee foresaw foreseen foresees foreseeing	foretell foretold foretold foretells foretelling	forget forgot forgotten forgets foregetting	forgive forgave forgiven forgives forgiving	forsake forsook forsaken forsakes forsaking	freeze froze frozen freezes freezing	frostbite frostbit frostbitten frostbites frostbiting	get got got/gotten gets getting	give gave given gives giving	go went gone/been goes going	grind ground ground grinds grinding	grow grew grown grows growing	handwrite handwrote handwritten handwrites handwriting	hang hung/hanged hung/hanged hangs hanging	have had had has having	hear heard heard hears hearing	hide hid hidden hides hiding	hit hit hit hits hitting	hold held held holds holding	hurt hurt hurt hurts hurting	inlay inlaid inlaid inlays inlaying	input input/inputted input/inputted inputs inputting	interlay interlaid interlaid interlays interlaying	keep kept kept keeps keeping	kneel knelt/kneeled knelt/kneeled kneels kneeling	knit knit/knitted knit/knitted knits knitting	know knew known knows knowing	lay laid laid lays laying	lead led led leads leading	lean leant/leaned leant/leaned leans leaning	leap leapt/leaped leapt/leaped leaps leaping	learn learnt/learned learnt/learned learns learning	leave left left leaves leaving	lend lent lent lends lending	let let let lets letting	lie lay lain lies lying	light lit lit lights lighting	lose lost lost loses losing	make made made makes making	mean meant meant means meaning	meet met met meets meeting	melt melted molten/melted melts melting	mislead misled misled misleads misleading	mistake mistook mistaken mistakes mistaking	misunderstand misunderstood misunderstood misunderstands misunderstanding	miswed miswed/miswedded miswed/miswedded misweds miswedding	mow mowed mown mows mowing	overdraw overdrew overdrawn overdraws overdrawing	overhear overheard overheard overhears overhearing	overtake overtook overtaken overtakes overtaking	pay paid paid pays paying	preset preset preset presets presetting	prove proved proven/proved proves proving	put put put puts putting	quit quit quit quits quitting	re-prove re-proved re-proven/re-proved re-proves re-proving	read read read reads reading	rid rid/ridded rid/ridded rids ridding	ride rode ridden rides riding	ring rang rung rings ringing	rise rose risen rises rising	rive rived riven/rived rives riving	run ran run runs running	say said said says saying	see saw seen sees seeing	seek sought sought seeks seeking	sell sold sold sells selling	send sent sent sends sending	set set set sets setting	sew sewed sewn/sewed sews sewing	shake shook shaken shakes shaking	shave shaved shaven/shaved shaves shaving	shear shore/sheared shorn/sheared shears shearing	shed shed shed sheds shedding	shine shone shone shines shining	shoe shod shod shoes shoeing	shoot shot shot shoots shooting	show showed shown shows showing	shrink shrank shrunk shrinks shrinking	shut shut shut shuts shutting	sing sang sung sings singing	sink sank sunk sinks sinking	sit sat sat sits sitting	slay slew slain slays slaying	sleep slept slept sleeps sleeping	slide slid slid/slidden slides sliding	sling slung slung slings slinging	slink slunk slunk slinks slinking	slit slit slit slits slitting	smell smelt/smelled smelt/smelled smells smelling	sneak sneaked/snuck sneaked/snuck sneaks sneaking	soothsay soothsaid soothsaid soothsays soothsaying	sow sowed sown sows sowing	speak spoke spoken speaks speaking	speed sped/speeded sped/speeded speeds speeding	spell spelt/spelled spelt/spelled spells spelling	spend spent spent spends spending	spill spilt/spilled spilt/spilled spills spilling	spin span/spun spun spins spinning	spit spat/spit spat/spit spits spitting	split split split splits splitting	spoil spoilt/spoiled spoilt/spoiled spoils spoiling	spread spread spread spreads spreading	spring sprang sprung springs springing	stand stood stood stands standing	steal stole stolen steals stealing	stick stuck stuck sticks sticking	sting stung stung stings stinging	stink stank stunk stinks stinking	stride strode/strided stridden strides striding	strike struck struck/stricken strikes striking	string strung strung strings stringing	strip stript/stripped stript/stripped strips stripping	strive strove striven strives striving	sublet sublet sublet sublets subletting	sunburn sunburned/sunburnt sunburned/sunburnt sunburns sunburning	swear swore sworn swears swearing	sweat sweat/sweated sweat/sweated sweats sweating	sweep swept/sweeped swept/sweeped sweeps sweeping	swell swelled swollen swells swelling	swim swam swum swims swimming	swing swung swung swings swinging	take took taken takes taking	teach taught taught teaches teaching	tear tore torn tears tearing	tell told told tells telling	think thought thought thinks thinking	thrive throve/thrived thriven/thrived thrives thriving	throw threw thrown throws throwing	thrust thrust thrust thrusts thrusting	tread trod trodden treads treading	undergo underwent undergone undergoes undergoing	understand understood understood understands understanding	undertake undertook undertaken undertakes undertaking	upsell upsold upsold upsells upselling	upset upset upset upsets upsetting	vex vext/vexed vext/vexed vexes vexing	wake woke woken wakes waking	wear wore worn wears wearing	weave wove woven weaves weaving	wed wed/wedded wed/wedded weds wedding	weep wept wept weeps weeping	wend wended/went wended/went wends wending	wet wet/wetted wet/wetted wets wetting	win won won wins winning	wind wound wound winds winding	withdraw withdrew withdrawn withdraws withdrawing	withhold withheld withheld withholds withholding	withstand withstood withstood withstands withstanding	wring wrung wrung wrings wringing	write wrote written writes writing	zinc zinced/zincked zinced/zincked zincs/zincks zincking'.split('\t').map(function(o){return o.split(' ')}),

        // Irregular infinitives
        // Populated in compendium.parser.js using `verbs` array and `irregular` matrix
        infinitives: [],

        // Common exceptions to the words ending with "ing"
        // but that are not the gerund of a verb
        ing_excpt: [
            'anything',
            'spring',
            'something',
            'thing',
            'king',
            'nothing'
        ],

        // All verbs ending
        ing_test: [],

        // Adverbs
        emphasis: [
            'totally',
            'fully',
            'really',
            'surprisingly',
            'absolutely',
            'actively',
            'clearly',
            'crazily',
            'greatly',
            'happily',
            'notably',
            'severly',
            'particularly',
            'highly',
            'quite',
            'pretty',
            'seriously',
            'very',
            'horribly',
            'even',
            'overly',
            'extremely'
        ],

        // Thanks to https://github.com/spencermountain/nlp_compromise
        // for initial abbreviations list.
        // Odd entries are abbreviations, even entries are replacement.
        // Potentially ambiguous abbrvs have no replacement.
        // Replacements are extracted from this array and
        // added in `abbrs_rplt` when lexicon is parsed (see lexicon.js).
        abbrs: [
            //honourifics
            'jr', 'junior',
            'mr', 'mister',
            'mrs', 'missus',
            'ms', 'miss',
            'dr', 'doctor',
            'prof', 'professor',
            'pr', 'professor',
            'sr', 'senior',
            'sen', 'senator',
            'sens', 'senators',
            'corp', 'corporation',
            'rep', '',
            'gov', 'governor',
            'atty', 'attorney',
            'supt', 'superintendent',
            'det', 'detective',
            'rev', '',
            'col', 'colonel',
            'gen', 'general',
            'lt', 'lieutenant',
            'cmdr', 'commander',
            'adm', 'administrative',
            'capt', 'captain',
            'sgt', 'sergent',
            'cpl', 'caporal',
            'maj', '',
            'esq', 'esquire',
            'phd', '',
            'adj', 'adjective',
            'adv', 'adverb',
            'asst', 'assistant',
            'bldg', 'building',
            'brig', 'brigade',
            'hon', '',
            'messrs', 'messeurs',
            'mlle', 'mademoiselle',
            'mme', 'madame',
            // 'op', '',
            'ord', 'order',
            'pvt', 'private',
            'reps', '',
            'res', '',
            // 'sens', '',
            'sfc', '',
            'surg', 'surgeon',
            'ph', '',
            'ds', '',
            //common abbreviations
            // 'arc', '',
            'ave', 'avenue',
            'blvd', 'boulevard',
            'cl', '',
            'ct', '',
            'cres', '',
            'exp', '',
            'rd', 'road',
            'st', 'street',
            // 'dist', '',
            'mt', 'mount',
            'ft', '',
            'fy', '',
            'hwy', 'highway',
            'la', '',
            'pd', '',
            'pl', '',
            'plz', '',
            'tce', '',
            'vs', '',
            'etc', '',
            'esp', '',
            'llb', '',
            'md', '',
            'bl', '',
            'ma', '',
            'ba', '',
            'lit', '',
            'fl', '',
            'ex', 'example',
            'eg', '',
            //place abbrevs
            'ala', 'alabama',
            'al', 'alabama',
            'ariz', 'arizona',
            'ark', 'arkansas',
            'cal', 'california',
            'calif', 'california',
            'col', 'coloradoa',
            'colo', 'colorado',
            'conn', 'connecticut',
            'del', 'delaware',
            'fed', 'federal',
            'fla', 'florida',
            'ga', 'georgia',
            'ida', 'idaho',
            'id', 'idaho',
            'ill', 'illinois',
            'ind', 'indiana',
            'ia', 'iowa',
            'kan', 'kansas',
            'kans', 'kansas',
            'ken', 'kentuky',
            'ky', 'kentuky',
            'la', '',
            // 'me', '',
            'md', '',
            'mass', 'massachussets',
            'mich', 'michigan',
            'minn', 'minnesota',
            'miss', 'mississippi',
            'mo', 'missouri',
            'mont', 'montana',
            'neb', 'nebraska',
            'nebr', 'nebraska',
            'nev', 'nevada',
            'mex', 'mexico',
            'okla', 'oklahoma',
            'ok', 'oklahoma',
            'ore', 'oregon',
            'penna', 'pennsylvania',
            'penn', 'pennsylvania',
            'pa', 'pennsylvania',
            'dak', 'dakota',
            'tenn', 'tennessee',
            'tex', 'texas',
            'ut', 'utah',
            'vt', 'vermont',
            'va', 'virginia',
            'wash', 'washington',
            'wis', 'wisconsin',
            'wisc', 'wisconsin',
            'wy', 'wyoming',
            'wyo', 'wyoming',
            'alta', 'alberta',
            'ont', 'ontario',
            'que', 'quebec',
            'sask', 'saskatchewan',
            'yuk', 'yukon',
            //date abbrevs
            'jan', 'january',
            'feb', 'february',
            'mar', 'march',
            'apr', 'april',
            'jun', 'june',
            'jul', 'july',
            'aug', 'august',
            'sep', 'september',
            'oct', 'october',
            'nov', 'november',
            'dec', 'december',
            'sept', 'september',
            //org abbrevs
            'dept', 'department',
            'univ', 'university',
            'assn', 'association',
            'bros', 'brothers',
            'inc', 'incorported',
            'ltd', 'limited',
            'co', ''
        ],

        synonyms: 'no nah nope n	yes yeah yep yup yah aye yea	seriously srlsy	ok k okay o.k. oki okey-dokey okey-doke	them \'em	you ya u	your ur yo	because cuz bcause	please pls plz	this tis dis	tomorrow tmorrow 2moro	tonight 2nite	today 2day	great gr8	later l8r	thanks thx thks tx	are \'re	am \'m	hello hi	love <3	',

        // Abbreviation replacements
        // (populated by parser)
        abbrs_rplt: [],

        //proper nouns with exclamation marks
        exclamations: [
            'yahoo',
            'joomla',
            'jeopardy'
        ],

        // Rules from `rules.txt`,
        // populated by gulpfile and parsed by compendium.parser.js
        rules: 'VBP VB 13 MD	VBZ POS 8 NN \'s	VBZ POS 8 NNS \'s	VBZ POS 8 NNP \'s	VBZ POS 8 NNPS \'s	VBZ POS 8 VB \'s	NNS POS 8 VB \'s	NNS POS 8 NN \'	NNS POS 8 NNS \'	NNS POS 8 NNP \'	NNS POS 8 NNPS \'	NNS POS 8 NN \'s	NNS POS 8 NNS \'s	NNS POS 8 NNP \'s	NNS POS 8 NNPS \'s	RB DT 8 VBN no	RB DT 8 VBG no	RB DT 8 VBD no	RB DT 8 VBP no	RB DT 8 VB no	RB DT 8 RB no	IN RP 8 VB out	IN RP 8 VBZ out	IN RP 8 VBG out	IN RP 8 VBN out	IN RP 8 VBP out	IN RP 8 VB off	IN RP 8 VBZ off	IN RP 8 VBG off	IN RP 8 VBN off	IN RP 8 VBP off	RB DT 8 VBD no	RB DT 8 IN no	VBZ NNS 2 PRP$	VBZ NNS 2 WP$	VBZ NNS 2 VBZ	VBZ NNS 2 VBP	VBZ NNS 2 JJ	VBZ NNS 2 JJS	VBZ NNS 2 JJR	VBZ NNS 2 POS	VBZ NNS 2 CD	VBZ NNS 51 the DT	VBZ NNS 15 is	VBZ NNS 15 are	VBZ NNS 5 the	VBZ NNS 5 those	VBZ NNS 5 these	VB VBP 8 NNP have	VB VBP 8 EX have	VB VBP 8 RB have	VB VBP 8 RBR have	IN WDT 8 NNS that	VBP IN 8 JJR like	VBP IN 8 RBR like	VBP IN 8 VBD like	VBP IN 8 VBN like	VBP IN 8 RB like	VBP IN 8 VBZ like	JJ JJS 8 DT latest	PRP$ PRP 8 VBD her	JJ NN 8 CD fine	JJ RB 8 IN much	NNP MD 8 PRP may	NNP MD 8 NNP may	NNP MD 8 NN may	NNP MD 8 NNS may	NNP MD 8 CC may	JJ NN 8 CC chief	JJ VB 8 TO slow	JJ VB 8 MD slow	VB NN 2 DT	VB NN 2 JJR	VB NN 2 JJ	VB NN 2 NN	VB NN 2 IN	VB NN 2 SYM	VB NN 2 VBD	VB NN 2 VBN	VB NN 172 MD VB NN	VB NN 172 MD VB IN	NN VB 1 MD	NN VB 172 VBN TO $	NN VB 172 NNS TO JJ	NN VBG 2 PRP	NNP MD 12 may VB	VBD VBN 21 be	JJR RBR 3 JJ	JJS RBS 12 most JJ	NN RB 121 kind of	JJ IN 8 NNP in	RB DT 8 VBZ no	JJ IN 8 NN in	JJ IN 8 JJ in	JJ IN 8 RB in	JJ IN 8 VB in	JJ IN 8 NNS in	JJ IN 8 VBN in	JJ IN 8 CD in	IN RB 12 as RB	WRB RB 12 when PRP	WRB RB 8 IN how	JJ RB 8 RB much	JJR RBR 12 more JJ	IN RP 8 VBD up	IN RB 8 CD up	IN RB 8 NN up	RP RB 8 VBD up	IN RP 8 VB up	RB RP 8 VB back	RB RP 8 VBD back	RB RP 8 VBG back	RB IN 81 years ago	RB IN 81 year ago	IN WDT 8 NN that	RB WRB 12 when PRP	IN DT 12 that NN	WDT IN 12 that NN	WDT IN 12 that DT	WDT IN 12 that PRP	WRB RB 12 when DT	VB NN 3 VBZ +	VBG MD 6 \'ll	VBG MD 6 wo	VBD VBN 13 VBZ	VBN VBD 3 .	VBN VBD 3 DT	VBG VBP 2 PRP	VBN VBD 2 PRP	VBN VBD 2 NNP	VBN VBD 2 NN	VBN VBD 2 NNS	VBN VBD 2 WP	VBN VBD 14 NN RB	VBN VBD 14 PRP DT	VBD VBN 2 VBD	VBD VBN 2 VB	VBD VBN 2 VBG	VBD VBN 2 VBZ	VBD VBN 2 VBZ	VBD VBN 2 VBP	VBD VBN 14 RB TO	VBD VBN 14 VBZ JJ	VBD VBN 14 VBP JJ	VBD VBN 171 TO PRP	VBD VBN 15 by	VBN VBD 15 that	VBN VBD 5 which	VBD VBN 5 has	VBD VBN 13 VBD	VB VBN 5 has	VBD VBN 17 MD	VBG NN 2 DT	VBG NN 2 JJ	VBG NN 2 PRP$	RBS JJS 2 IN	NN VB 14 TO DT	NN VB 14 TO IN	VB VBP 14 PRP DT	VB VBP 14 PRP IN	VBP VB 14 MD VBN	MD VBG 14 VBZ TO	MD VBG 14 VBP TO	VBP VB 14 MD TO	VBP VB 14 TO DT	VBN VBD 14 PRP PRP	VB VBD 14 PRP TO	VBN VBD 14 NNP DT	VB VBP 14 PRP PRP	VB VBP 2 NNS	VBP VB 51 to TO	NNS NN 51 a DT	WRB RB 51 and CC	VBP VB 81 may have	VBP MD 6 gon	VBP VB 8 MD have	NNS VBZ 8 PRP plans	NNS VBZ 8 NN plans	NNS VBZ 8 NNP plans	NNS VBZ 8 NNPS plans	RB NNP 12 south NNP	RB NNP 12 east NNP	RB NNP 12 west NNP	RB NNP 12 north NNP	FW NNP 12 la NNP	JJ NNP 12 american NNP	VBN NNP 12 united NNP	WRB RB 12 when NN	WRB RB 12 when NNS	WRB RB 12 when NNP	WRB RB 12 when NNPS	WRB RB 12 where NN	WRB RB 12 where NNS	WRB RB 12 where NNP	WRB RB 12 where NNPS	WRB RB 12 when PRP	RBR JJR 81 year earlier	RBR JJR 81 years earlier	NN VB 14 TO VBZ	VBG NN 2 JJR	CD NN 8 DT one	CD PRP 12 one VBZ	NNS VBZ 2 PRP	VB VBP 2 WDT	VB VBP 2 WP	VB VBP 2 PRP	VBZ NNS 2 VBG	VBZ NNS 2 VBN	VBZ NNS 2 VBD	NNS VBZ 2 WDT	NNS VBZ 2 WP	VBZ NNS 2 IN	WDT IN 12 that DT	IN WDT 12 that VBP	NN UH 12 hell UH	NN UH 12 hell RB	VB NN 8 PRP$ bid	VBN JJ 51 is VBZ	VBN JJ 51 are VBP	VBN JJ 14 NN JJ	VBN JJ 14 RB NN	NN VB 5 will	JJ VB 5 will	NN NNP 5 mr.	NNS VBZ 6 has	VB NN 5 the	VBD VBN 15 with	VBN VBD 6 was	NNS VBZ 6 is	NN VBP 6 have	VBD VBP 6 have	VBN VBD 6 were	CD NN 81 no one	VBG JJ 14 , JJ	VBG JJ 14 DT JJ	VBG JJ 14 , NN	VBG JJ 14 DT NN	NNS VBZ 8 WDT \'s	NNS VBZ 8 DT \'s	NNS VBZ 8 IN \'s	NNP UH 0 RT	NNP UH 0 MT	JJ VBN 14 VBZ IN	JJ NN 14 DT IN	JJ VBN 14 VBP IN	NN JJ 6 first	NN JJ 6 last	NN JJ 6 high	NN JJ 6 low	NN JJ 6 middle	',

        // Suffixes from `suffixes.txt`
        // populated by gulpfile and parsed by compendium.parser.js
        suffixes: 'rate VB	rates VBZ	late VB	lates VBZ	nate VB	nates VBZ	izes VBZ	ize VB	ify VB	ifies VBZ	ising VBG	ism NN	able JJ	ible JJ	ical JJ	esque JJ	ous JJ 	etic JJ	atic JJ	egic JJ	ophic JJ	ish JJ	ive JJ	gic JJ	tic JJ	mic JJ	phile JJ	less JJ	ful JJ	edelic JJ	adelic JJ	aholic JJ	oholic JJ	ilar JJ	ular JJ	ly RB	like JJ	wise RB	ise VB	some JJ	escent JJ	chy JJ	thy JJ	shy JJ	sty JJ	tty JJ	bby JJ	ssy JJ	zzy JJ	mmy JJ	ppy JJ	tary JJ	nary JJ	ial JJ	alous JJ	ally RB	vid JJ	rid JJ	wards RB	iest JJS	dest JJS	rth JJ',

        // Raw emoticons, populated by compendium.parser.js
        emots: [],

        // Float char
        floatChar: '.',
        // Thousands char
        thousandChar: ',',

        multipliers: ['hundred', 'thousand', 'million', 'billion', 'trillion'],

        // Numbers and their value
        // (PoS tag `CD`)
        numbers: {
            zero: 0,
            one: 1,
            two: 2,
            three: 3,
            four: 4,
            five: 5,
            six: 6,
            seven: 7,
            eight: 8,
            nine: 9,
            ten: 10,
            eleven: 11,
            twelve: 12,
            thirteen: 13,
            fourteen: 14,
            fifteen: 15,
            sixteen: 16,
            seventeen: 17,
            eighteen: 18,
            nineteen: 19,
            ninteen: 19,
            twenty: 20,
            thirty: 30,
            forty: 40,
            fourty: 40,
            fifty: 50,
            sixty: 60,
            seventy: 70,
            eighty: 80,
            ninety: 90,
            hundred: 100,
            thousand: 1e3,
            million: 1e6,
            billion: 1e9,
            trillion: 1e12
        },

        nationalities: 'afghan albanian algerian argentine armenian australian aussie austrian bangladeshi belgian bolivian bosnian brazilian bulgarian cambodian canadian chilean chinese colombian croat cuban czech dominican egyptian british estonian ethiopian finnish french gambian georgian german greek haitian hungarian indian indonesian iranian iraqi irish israeli italian jamaican japanese jordanian kenyan korean kuwaiti latvian lebanese liberian libyan lithuanian macedonian malaysian mexican mongolian moroccan dutch nicaraguan nigerian norwegian omani pakistani palestinian filipino polish portuguese qatari romanian russian rwandan samoan saudi scottish senegalese serbian singaporean slovak somali sudanese swedish swiss syrian taiwanese thai tunisian ugandan ukrainian american hindi spanish venezuelan vietnamese welsh african european asian californian',

        // Negation marks
        neg: {
            zero: 'CD',
            without: 'IN',
            except: 'IN',
            absent: 'JJ',
            unlike: 'IN',
            unable: 'JJ',
            unremarkable: 'JJ',
            unlikely: 'JJ',
            negative: 'JJ',
            hardly: 'RB',
            deny: 'VB',
            fail: 'VB',
            exclude: 'VB',
            lack: 'NN',
            absence: 'NN',
            none: 'NN',
            nothing: 'NN'
        },

        // Counter negation marks
        neg_neg: {
            only: 'RB',
            just: 'RB',
            solely: 'RB',
            uniquely: 'RB',
            exclusively: 'RB'
        },

        // Refusal keywords, also use as negation marks
        refusal: {
            not: 'RB',
            'n\'t':  'RB',
            '\'t':  'RB',
            no: 'RB',
            neither: 'DT',
            nor: 'DT',
            never: 'RB'
        },

        // Approval keyword
        approval: {
            yes: 'UH',
            ok: 'NN',
            agreed: 'VBN',
            agree: 'VBP',
            affirmative: 'JJ',
            approved: 'VBN',
            sure: 'JJ',
            roger: 'NN',
            indeed: 'RB',
            right: 'NN',
            alright: 'JJ'
        },

        approval_verbs: [
            'go', 'do'
        ],

        // Breakpoints words
        breakpoints: {

        },

        citations: {
            '"': '"',
            '\'': '"',
            '`': '"'
        },

        // Personal pronouns
        // filtered in lexicon (y?)
        p: {
            i: 'PRP',
            you: 'PRP'
        },

        // For date recognition
        months: {
            january: 'NNP',
            february: 'NNP',
            march: 'NNP',
            april: 'NNP',
            may: 'NNP',
            june: 'NNP',
            july: 'NNP',
            august: 'NNP',
            september: 'NNP',
            october: 'NNP',
            november: 'NNP',
            december: 'NNP'
        },
        days: {
            monday: 'NNP',
            tuesday: 'NNP',
            wednesday: 'NNP',
            thursday: 'NNP',
            friday: 'NNP',
            saturday: 'NNP',
            sunday: 'NNP'
        },

        indicators: {
            first: 'JJ',
            both: 'DT',
            second: 'JJ',
            third: 'JJ',
            last: 'JJ',
            previous: 'JJ',
            next: 'JJ',
            latest: 'JJ',
            earliest: 'JJ'
        },

        // Profiling
        dirty: 'anal anus arse ass asshole ballsack bastard bitch biatch bloody blowjob brojob bollock bollok boner horsecock boob bugger bum butt buttplug clitoris cock coon crap cunt damn dick dildo dyke fag feck fellate fellatio felching fuck fucking fudgepacker fudgepacker flange homo jerk jizz knobend knobend labia lmfao muff nigger nigga penis piss poop prick pube pussy queer scrotum sex shit sh1t slut smegma spunk tit tosser turd twat vagina wank whore crappy'.split(' '),

        polite: 'thanks thank please excuse pardon welcome sorry might ought'.split(' ')
    });
}();

/**
 * Compendium (NLP knowledge base)
 */

!function() {
    extend(cpd, {

        // Regular verbs
        // Conjugated and expanded in compendium.parser.js
        verbs: 'accept add admire admit advise afford agree alert allow amuse analyse analyze announce annoy answer apologise appear applaud appreciate approve argue arrange arrest arrive ask attach attack attempt attend attract avoid back bake balance ban bang bare bat bathe battle beam beg behave belong bleach bless blind blink blot blush boast boil bolt bomb book bore borrow bounce bow box brake branch breathe bruise brush bubble bump burn bury buzz calculate call camp care carry carve cause challenge change charge chase cheat check cheer chew choke chop claim clap clean clear clip close coach coil collect colour comb command communicate compare compete complain complete concentrate concern confess confuse connect consider consist contain continue copy correct cough count cover crack crash crawl cross crush cry cure curl curve cycle dam damage dance dare decay deceive decide decorate delay delight deliver depend describe desert deserve destroy detect develop disagree disappear disapprove disarm discover dislike divide double doubt drag drain dream dress drip drop drown drum dry dust earn educate embarrass employ empty encourage end enjoy enter entertain escape examine excite excuse exercise exist expand expect explain explode extend face fade fail fancy fasten fax fear fence fetch file fill film fire fit fix flap flash float flood flow flower fold follow fool force form found frame frighten fry gather gaze glow glue grab grate grease greet grin grip groan guarantee guard guess guide hammer hand handle hang happen harass harm hate haunt head heal heap heat help hook hop hope hover hug hum hunt hurry identify ignore imagine impress improve include increase influence inform inject injure instruct intend interest interfere interrupt introduce invent invite irritate itch jail jam jog join joke judge juggle jump kick kill kiss kneel knit knock knot label land last laugh launch learn level license lick lie lighten like list listen live load lock long look love man manage mark marry match mate matter measure meddle melt memorise mend mess up milk mine miss mix moan moor mourn move muddle mug multiply murder nail name need nest nod note notice number obey object observe obtain occur offend offer open order overflow owe own pack paddle paint park part pass paste pat pause peck pedal peel peep perform permit phone pick pinch pine place plan plant play please plug point poke polish pop possess post pour practise practice pray preach precede prefer prepare present preserve press pretend prevent prick print produce program promise protect provide pull pump punch puncture punish push question queue race radiate rain raise reach realise receive recognise record reduce reflect refuse regret reign reject rejoice relax release rely remain remember remind remove repair repeat replace reply report reproduce request rescue retire return rhyme rinse risk rob rock roll rot rub ruin rule rush sack sail satisfy save scare scatter scold scorch scrape scratch scream screw scribble scrub seal search separate serve settle shade share shave shelter shiver shock shop shrug sigh sign signal sin sip ski skip slap slip slow smash smell smile smoke snatch sneeze sniff snore snow soak soothe sound spare spark sparkle spell spill spoil spot spray sprout squash squeak squeal squeeze stain stamp stare start stay steer step stir stitch stop store strap strengthen stretch strip stroke stuff subtract succeed suck suffer suggest suit supply support suppose surprise surround suspect suspend switch talk tame tap taste tease telephone tempt terrify test thank thaw tick tickle tie time tip tire touch tour tow trace trade train transport trap travel treat tremble trick trip trot trouble trust try tug tumble turn twist type undress unemploy unfasten unite unlock unpack untidy use vanish visit wail wait walk wander want warm warn wash waste watch water wave weigh welcome whine whip whirl whisper whistle wink wipe wish wobble wonder work worry wrap wreck wrestle wriggle yawn yell zip zoom'.split(' '),

        // Irregular verbs
        irregular: 'abide abode/abided abode/abided/abidden abides abiding	alight alit/alighted alit/alighted alights alighting	arise arose arisen arises arising	awake awoke awoken awakes awaking	be was/were been is being	bear bore born/borne bears bearing	beat beat beaten beats beating	become became become becomes becoming	begin began begun begins beginning	behold beheld beheld beholds beholding	bend bent bent bends bending	bet bet bet bets betting	bid bade bidden bids bidding	bid bid bid bids bidding	bind bound bound binds binding	bite bit bitten bites biting	bleed bled bled bleeds bleeding	blow blew blown blows blowing	break broke broken breaks breaking	breed bred bred breeds breeding	bring brought brought brings bringing	broadcast broadcast/broadcasted broadcast/broadcasted broadcasts broadcasting	build built built builds building	burn burnt/burned burnt/burned burns burning	burst burst burst bursts bursting	bust bust bust busts busting	buy bought bought buys buying	cast cast cast casts casting	catch caught caught catches catching	choose chose chosen chooses choosing	clap clapped/clapt clapped/clapt claps clapping	cling clung clung clings clinging	clothe clad/clothed clad/clothed clothes clothing	come came come comes coming	cost cost cost costs costing	creep crept crept creeps creeping	cut cut cut cuts cutting	dare dared/durst dared dares daring	deal dealt dealt deals dealing	dig dug dug digs digging	dive dived/dove dived dives diving	do did done does doing	draw drew drawn draws drawing	dream dreamt/dreamed dreamt/dreamed dreams dreaming	drink drank drunk drinks drinking	drive drove driven drives driving	dwell dwelt dwelt dwells dwelling	eat ate eaten eats eating	fall fell fallen falls falling	feed fed fed feeds feeding	feel felt felt feels feeling	fight fought fought fights fighting	find found found finds finding	fit fit/fitted fit/fitted fits fitting	flee fled fled flees fleeing	fling flung flung flings flinging	fly flew flown flies flying	forbid forbade/forbad forbidden forbids forbidding	forecast forecast/forecasted forecast/forecasted forecasts forecasting	foresee foresaw foreseen foresees foreseeing	foretell foretold foretold foretells foretelling	forget forgot forgotten forgets foregetting	forgive forgave forgiven forgives forgiving	forsake forsook forsaken forsakes forsaking	freeze froze frozen freezes freezing	frostbite frostbit frostbitten frostbites frostbiting	get got got/gotten gets getting	give gave given gives giving	go went gone/been goes going	grind ground ground grinds grinding	grow grew grown grows growing	handwrite handwrote handwritten handwrites handwriting	hang hung/hanged hung/hanged hangs hanging	have had had has having	hear heard heard hears hearing	hide hid hidden hides hiding	hit hit hit hits hitting	hold held held holds holding	hurt hurt hurt hurts hurting	inlay inlaid inlaid inlays inlaying	input input/inputted input/inputted inputs inputting	interlay interlaid interlaid interlays interlaying	keep kept kept keeps keeping	kneel knelt/kneeled knelt/kneeled kneels kneeling	knit knit/knitted knit/knitted knits knitting	know knew known knows knowing	lay laid laid lays laying	lead led led leads leading	lean leant/leaned leant/leaned leans leaning	leap leapt/leaped leapt/leaped leaps leaping	learn learnt/learned learnt/learned learns learning	leave left left leaves leaving	lend lent lent lends lending	let let let lets letting	lie lay lain lies lying	light lit lit lights lighting	lose lost lost loses losing	make made made makes making	mean meant meant means meaning	meet met met meets meeting	melt melted molten/melted melts melting	mislead misled misled misleads misleading	mistake mistook mistaken mistakes mistaking	misunderstand misunderstood misunderstood misunderstands misunderstanding	miswed miswed/miswedded miswed/miswedded misweds miswedding	mow mowed mown mows mowing	overdraw overdrew overdrawn overdraws overdrawing	overhear overheard overheard overhears overhearing	overtake overtook overtaken overtakes overtaking	pay paid paid pays paying	preset preset preset presets presetting	prove proved proven/proved proves proving	put put put puts putting	quit quit quit quits quitting	re-prove re-proved re-proven/re-proved re-proves re-proving	read read read reads reading	rid rid/ridded rid/ridded rids ridding	ride rode ridden rides riding	ring rang rung rings ringing	rise rose risen rises rising	rive rived riven/rived rives riving	run ran run runs running	say said said says saying	see saw seen sees seeing	seek sought sought seeks seeking	sell sold sold sells selling	send sent sent sends sending	set set set sets setting	sew sewed sewn/sewed sews sewing	shake shook shaken shakes shaking	shave shaved shaven/shaved shaves shaving	shear shore/sheared shorn/sheared shears shearing	shed shed shed sheds shedding	shine shone shone shines shining	shoe shod shod shoes shoeing	shoot shot shot shoots shooting	show showed shown shows showing	shrink shrank shrunk shrinks shrinking	shut shut shut shuts shutting	sing sang sung sings singing	sink sank sunk sinks sinking	sit sat sat sits sitting	slay slew slain slays slaying	sleep slept slept sleeps sleeping	slide slid slid/slidden slides sliding	sling slung slung slings slinging	slink slunk slunk slinks slinking	slit slit slit slits slitting	smell smelt/smelled smelt/smelled smells smelling	sneak sneaked/snuck sneaked/snuck sneaks sneaking	soothsay soothsaid soothsaid soothsays soothsaying	sow sowed sown sows sowing	speak spoke spoken speaks speaking	speed sped/speeded sped/speeded speeds speeding	spell spelt/spelled spelt/spelled spells spelling	spend spent spent spends spending	spill spilt/spilled spilt/spilled spills spilling	spin span/spun spun spins spinning	spit spat/spit spat/spit spits spitting	split split split splits splitting	spoil spoilt/spoiled spoilt/spoiled spoils spoiling	spread spread spread spreads spreading	spring sprang sprung springs springing	stand stood stood stands standing	steal stole stolen steals stealing	stick stuck stuck sticks sticking	sting stung stung stings stinging	stink stank stunk stinks stinking	stride strode/strided stridden strides striding	strike struck struck/stricken strikes striking	string strung strung strings stringing	strip stript/stripped stript/stripped strips stripping	strive strove striven strives striving	sublet sublet sublet sublets subletting	sunburn sunburned/sunburnt sunburned/sunburnt sunburns sunburning	swear swore sworn swears swearing	sweat sweat/sweated sweat/sweated sweats sweating	sweep swept/sweeped swept/sweeped sweeps sweeping	swell swelled swollen swells swelling	swim swam swum swims swimming	swing swung swung swings swinging	take took taken takes taking	teach taught taught teaches teaching	tear tore torn tears tearing	tell told told tells telling	think thought thought thinks thinking	thrive throve/thrived thriven/thrived thrives thriving	throw threw thrown throws throwing	thrust thrust thrust thrusts thrusting	tread trod trodden treads treading	undergo underwent undergone undergoes undergoing	understand understood understood understands understanding	undertake undertook undertaken undertakes undertaking	upsell upsold upsold upsells upselling	upset upset upset upsets upsetting	vex vext/vexed vext/vexed vexes vexing	wake woke woken wakes waking	wear wore worn wears wearing	weave wove woven weaves weaving	wed wed/wedded wed/wedded weds wedding	weep wept wept weeps weeping	wend wended/went wended/went wends wending	wet wet/wetted wet/wetted wets wetting	win won won wins winning	wind wound wound winds winding	withdraw withdrew withdrawn withdraws withdrawing	withhold withheld withheld withholds withholding	withstand withstood withstood withstands withstanding	wring wrung wrung wrings wringing	write wrote written writes writing	zinc zinced/zincked zinced/zincked zincs/zincks zincking'.split('\t').map(function(o){return o.split(' ')}),

        // Irregular infinitives
        // Populated in compendium.parser.js using `verbs` array and `irregular` matrix
        infinitives: [],

        // Common exceptions to the words ending with "ing"
        // but that are not the gerund of a verb
        ing_excpt: [
        ],

        // All verbs ending
        ing_test: [],

        // Adverbs
        emphasis: [
        ],

        // Thanks to https://github.com/spencermountain/nlp_compromise
        // for initial abbreviations list.
        // Odd entries are abbreviations, even entries are replacement.
        // Potentially ambiguous abbrvs have no replacement.
        // Replacements are extracted from this array and
        // added in `abbrs_rplt` when lexicon is parsed (see lexicon.js).
        abbrs: [
            //honourifics
            'jr', 'junior',
            'mr', 'mister',
            'mrs', 'missus',
            'ms', 'miss',
            'dr', 'doctor',
            'prof', 'professor',
            'pr', 'professor',
            'sr', 'senior',
            'sen', 'senator',
            'sens', 'senators',
            'corp', 'corporation',
            'rep', '',
            'gov', '',
            'atty', 'attorney',
            'supt', 'superintendent',
            'det', 'detective',
            'rev', '',
            'col', 'colonel',
            'gen', 'general',
            'lt', 'lieutenant',
            'cmdr', 'commander',
            'adm', 'administrative',
            'capt', 'captain',
            'sgt', 'sergent',
            'cpl', 'caporal',
            'maj', '',
            'esq', 'esquire',
            'phd', '',
            'adj', 'adjective',
            'adv', 'adverb',
            'asst', 'assistant',
            'bldg', 'building',
            'brig', 'brigade',
            'hon', '',
            'messrs', 'messeurs',
            'mlle', 'mademoiselle',
            'mme', 'madame',
            'op', '',
            'ord', 'order',
            'pvt', 'private',
            'reps', '',
            'res', '',
            'sens', '',
            'sfc', '',
            'surg', 'surgeon',
            'ph', '',
            'ds', '',
            //common abbreviations
            'arc', '',
            'al', '',
            'ave', 'avenue',
            'blvd', 'boulevard',
            'cl', '',
            'ct', '',
            'cres', '',
            'exp', '',
            'rd', 'road',
            'st', 'street',
            'dist', '',
            'mt', 'mount',
            'ft', '',
            'fy', '',
            'hwy', 'highway',
            'la', '',
            'pd', '',
            'pl', '',
            'plz', '',
            'tce', '',
            'vs', '',
            'etc', '',
            'esp', '',
            'llb', '',
            'md', '',
            'bl', '',
            'ma', '',
            'ba', '',
            'lit', '',
            'fl', '',
            'ex', 'example',
            'eg', '',
            //place abbrevs
            'ala', 'alabama',
            'ariz', 'arizona',
            'ark', 'arkansas',
            'cal', 'california',
            'calif', 'california',
            'col', 'coloradoa',
            'colo', 'colorado',
            'conn', 'connecticut',
            'del', 'delaware',
            'fed', 'federal',
            'fla', 'florida',
            'ga', 'georgia',
            'ida', 'idaho',
            'id', 'idaho',
            'ill', 'illinois',
            'ind', 'indiana',
            'ia', 'iowa',
            'kan', 'kansas',
            'kans', 'kansas',
            'ken', 'kentuky',
            'ky', 'kentuky',
            'la', '',
            'me', '',
            'md', '',
            'mass', 'massachussets',
            'mich', 'michigan',
            'minn', 'minnesota',
            'miss', 'mississippi',
            'mo', 'missouri',
            'mont', 'montana',
            'neb', 'nebraska',
            'nebr', 'nebraska',
            'nev', 'nevada',
            'mex', 'mexico',
            'okla', 'oklahoma',
            'ok', 'oklahoma',
            'ore', 'oregon',
            'penna', 'pennsylvania',
            'penn', 'pennsylvania',
            'pa', 'pennsylvania',
            'dak', 'dakota',
            'tenn', 'tennessee',
            'tex', 'texas',
            'ut', 'utah',
            'vt', 'vermont',
            'va', 'virginia',
            'wash', 'washington',
            'wis', 'wisconsin',
            'wisc', 'wisconsin',
            'wy', 'wyoming',
            'wyo', 'wyoming',
            'alta', 'alberta',
            'ont', 'ontario',
            'que', 'quebec',
            'sask', 'saskatchewan',
            'yuk', 'yukon',
            //date abbrevs
            'jan', 'january',
            'feb', 'february',
            'mar', 'march',
            'apr', 'april',
            'jun', 'june',
            'jul', 'july',
            'aug', 'august',
            'sep', 'september',
            'oct', 'october',
            'nov', 'november',
            'dec', 'december',
            'sept', 'september',
            //org abbrevs
            'dept', 'department',
            'univ', 'university',
            'assn', 'association',
            'bros', 'brothers',
            'inc', 'incorported',
            'ltd', 'limited',
            'co', ''
        ],

        synonyms: 'no nah nope n	yes yeah yep yup y yah aye yea	seriously srlsy	ok k okay o.k. oki okey-dokey okey-doke	them \'em	you ya ye	your yo	because cuz	please plz	this dis	tomorrow 2moro	tonight 2nite	today 2day	great gr8	later l8r	thanks thx thks tx	are \'re	am \'m	hello hi	love <3	',

        // Abbreviation replacements
        // (populated by parser)
        abbrs_rplt: [],

        //proper nouns with exclamation marks
        exclamations: [
            'yahoo',
            'joomla',
            'jeopardy'
        ],

        // Rules from `rules.txt`,
        // populated by gulpfile and parsed by compendium.parser.js
        rules: 'VBP VB 13 MD	VBZ POS 8 NN \'s	VBZ POS 8 NNS \'s	VBZ POS 8 NNP \'s	VBZ POS 8 NNPS \'s	VBZ POS 8 VB \'s	NNS POS 8 VB \'s	NNS POS 8 NN \'	NNS POS 8 NNS \'	NNS POS 8 NNP \'	NNS POS 8 NNPS \'	NNS POS 8 NN \'s	NNS POS 8 NNS \'s	NNS POS 8 NNP \'s	NNS POS 8 NNPS \'s	RB DT 8 VBN no	RB DT 8 VBG no	RB DT 8 VBD no	RB DT 8 VBP no	RB DT 8 VB no	RB DT 8 RB no	IN RP 8 VB out	IN RP 8 VBZ out	IN RP 8 VBG out	IN RP 8 VBN out	IN RP 8 VBP out	IN RP 8 VB off	IN RP 8 VBZ off	IN RP 8 VBG off	IN RP 8 VBN off	IN RP 8 VBP off	RB DT 8 VBD no	RB DT 8 IN no	VBZ NNS 2 PRP$	VBZ NNS 2 WP$	VBZ NNS 2 VBZ	VBZ NNS 2 VBP	VBZ NNS 2 JJ	VBZ NNS 2 JJS	VBZ NNS 2 JJR	VBZ NNS 2 POS	VBZ NNS 2 CD	VBZ NNS 51 the DT	VBZ NNS 15 is	VBZ NNS 15 are	VBZ NNS 5 the	VBZ NNS 5 those	VBZ NNS 5 these	VB VBP 8 NNP have	VB VBP 8 EX have	VB VBP 8 RB have	VB VBP 8 RBR have	IN WDT 8 NNS that	VBP IN 8 JJR like	VBP IN 8 RBR like	VBP IN 8 VBD like	VBP IN 8 VBN like	VBP IN 8 RB like	VBP IN 8 VBZ like	JJ JJS 8 DT latest	PRP$ PRP 8 VBD her	JJ NN 8 CD fine	JJ RB 8 IN much	NNP MD 8 PRP may	NNP MD 8 NNP may	NNP MD 8 NN may	NNP MD 8 NNS may	NNP MD 8 CC may	JJ NN 8 CC chief	JJ VB 8 TO slow	JJ VB 8 MD slow	VB NN 2 DT	VB NN 2 JJR	VB NN 2 JJ	VB NN 2 NN	VB NN 2 IN	VB NN 2 SYM	VB NN 2 VBD	VB NN 2 VBN	VB NN 172 MD VB NN	VB NN 172 MD VB IN	NN VB 1 MD	NN VB 172 VBN TO $	NN VB 172 NNS TO JJ	NN VBG 2 PRP	NNP MD 12 may VB	VBD VBN 21 be	JJR RBR 3 JJ	JJS RBS 12 most JJ	NN RB 121 kind of	JJ IN 8 NNP in	RB DT 8 VBZ no	JJ IN 8 NN in	JJ IN 8 JJ in	JJ IN 8 RB in	JJ IN 8 VB in	JJ IN 8 NNS in	JJ IN 8 VBN in	JJ IN 8 CD in	IN RB 12 as RB	WRB RB 12 when PRP	WRB RB 8 IN how	JJ RB 8 RB much	JJR RBR 12 more JJ	IN RP 8 VBD up	IN RB 8 CD up	IN RB 8 NN up	RP RB 8 VBD up	IN RP 8 VB up	RB RP 8 VB back	RB RP 8 VBD back	RB RP 8 VBG back	RB IN 81 years ago	RB IN 81 year ago	IN WDT 8 NN that	RB WRB 12 when PRP	IN DT 12 that NN	WDT IN 12 that NN	WDT IN 12 that DT	WDT IN 12 that PRP	WRB RB 12 when DT	VB NN 3 VBZ +	VBG MD 6 \'ll	VBG MD 6 wo	VBD VBN 13 VBZ	VBN VBD 3 .	VBN VBD 3 DT	VBG VBP 2 PRP	VBN VBD 2 PRP	VBN VBD 2 NNP	VBN VBD 2 NN	VBN VBD 2 NNS	VBN VBD 2 WP	VBN VBD 14 NN RB	VBN VBD 14 PRP DT	VBD VBN 2 VBD	VBD VBN 2 VB	VBD VBN 2 VBG	VBD VBN 2 VBZ	VBD VBN 2 VBZ	VBD VBN 2 VBP	VBD VBN 14 RB TO	VBD VBN 14 VBZ JJ	VBD VBN 14 VBP JJ	VBD VBN 171 TO PRP	VBD VBN 15 by	VBN VBD 15 that	VBN VBD 5 which	VBD VBN 5 has	VBD VBN 13 VBD	VB VBN 5 has	VBD VBN 17 MD	VBG NN 2 DT	VBG NN 2 JJ	VBG NN 2 PRP$	RBS JJS 2 IN	NN VB 14 TO DT	NN VB 14 TO IN	VB VBP 14 PRP DT	VB VBP 14 PRP IN	VBP VB 14 MD VBN	MD VBG 14 VBZ TO	MD VBG 14 VBP TO	VBP VB 14 MD TO	VBP VB 14 TO DT	VBN VBD 14 PRP PRP	VB VBD 14 PRP TO	VBN VBD 14 NNP DT	VB VBP 14 PRP PRP	VB VBP 2 NNS	VBP VB 51 to TO	NNS NN 51 a DT	WRB RB 51 and CC	VBP VB 81 may have	VBP MD 6 gon	VBP VB 8 MD have	NNS VBZ 8 PRP plans	NNS VBZ 8 NN plans	NNS VBZ 8 NNP plans	NNS VBZ 8 NNPS plans	RB NNP 12 south NNP	RB NNP 12 east NNP	RB NNP 12 west NNP	RB NNP 12 north NNP	FW NNP 12 la NNP	JJ NNP 12 american NNP	VBN NNP 12 united NNP	WRB RB 12 when NN	WRB RB 12 when NNS	WRB RB 12 when NNP	WRB RB 12 when NNPS	WRB RB 12 where NN	WRB RB 12 where NNS	WRB RB 12 where NNP	WRB RB 12 where NNPS	WRB RB 12 when PRP	RBR JJR 81 year earlier	RBR JJR 81 years earlier	NN VB 14 TO VBZ	VBG NN 2 JJR	CD NN 8 DT one	CD PRP 12 one VBZ	NNS VBZ 2 PRP	VB VBP 2 WDT	VB VBP 2 WP	VB VBP 2 PRP	VBZ NNS 2 VBG	VBZ NNS 2 VBN	VBZ NNS 2 VBD	NNS VBZ 2 WDT	NNS VBZ 2 WP	VBZ NNS 2 IN	WDT IN 12 that DT	IN WDT 12 that VBP	NN UH 12 hell UH	NN UH 12 hell RB	VB NN 8 PRP$ bid	VBN JJ 51 is VBZ	VBN JJ 51 are VBP	VBN JJ 14 NN JJ	VBN JJ 14 RB NN	NN VB 5 will	JJ VB 5 will	NN NNP 5 mr.	NNS VBZ 6 has	VB NN 5 the	VBD VBN 15 with	VBN VBD 6 was	NNS VBZ 6 is	NN VBP 6 have	VBD VBP 6 have	VBN VBD 6 were	CD NN 81 no one	VBG JJ 14 , JJ	VBG JJ 14 DT JJ	VBG JJ 14 , NN	VBG JJ 14 DT NN	NNS VBZ 8 WDT \'s	NNS VBZ 8 DT \'s	NNS VBZ 8 IN \'s	NNP UH 0 RT	NNP UH 0 MT	JJ VBN 14 VBZ IN	JJ NN 14 DT IN	JJ VBN 14 VBP IN	NN JJ 6 first	NN JJ 6 last	NN JJ 6 high	NN JJ 6 low	NN JJ 6 middle	',

        // Suffixes from `suffixes.txt`
        // populated by gulpfile and parsed by compendium.parser.js
        suffixes: 'rate VB	rates VBZ	late VB	lates VBZ	nate VB	nates VBZ	izes VBZ	ize VB	ify VB	ifies VBZ	ising VBG	ism NN	able JJ	ible JJ	ical JJ	esque JJ	ous JJ 	etic JJ	atic JJ	egic JJ	ophic JJ	ish JJ	ive JJ	gic JJ	tic JJ	mic JJ	phile JJ	less JJ	ful JJ	edelic JJ	adelic JJ	aholic JJ	oholic JJ	ilar JJ	ular JJ	ly RB	like JJ	wise RB	ise VB	some JJ	escent JJ	chy JJ	thy JJ	shy JJ	sty JJ	tty JJ	bby JJ	ssy JJ	zzy JJ	mmy JJ	ppy JJ	tary JJ	nary JJ	ial JJ	alous JJ	ally RB	vid JJ	rid JJ	wards RB	iest JJS	dest JJS	rth JJ',

        // Raw emoticons, populated by compendium.parser.js
        emots: [],

        // Float char
        floatChar: ',',
        // Thousands char
        thousandChar: '.',

        multipliers: ['cent', 'mille', 'million', 'milliard', 'billion'],

        // Numbers and their value
        // (PoS tag `CD`)
        numbers: {
            zero: 0,
            one: 1,
            two: 2,
            three: 3,
            four: 4,
            five: 5,
            six: 6,
            seven: 7,
            eight: 8,
            nine: 9,
            ten: 10,
            eleven: 11,
            twelve: 12,
            thirteen: 13,
            fourteen: 14,
            fifteen: 15,
            sixteen: 16,
            seventeen: 17,
            eighteen: 18,
            nineteen: 19,
            ninteen: 19,
            twenty: 20,
            thirty: 30,
            forty: 40,
            fourty: 40,
            fifty: 50,
            sixty: 60,
            seventy: 70,
            eighty: 80,
            ninety: 90,
            hundred: 100,
            thousand: 1e3,
            million: 1e6,
            billion: 1e9,
            trillion: 1e12
        },

        nationalities: 'afghan albanian algerian argentine armenian australian aussie austrian bangladeshi belgian bolivian bosnian brazilian bulgarian cambodian canadian chilean chinese colombian croat cuban czech dominican egyptian british estonian ethiopian finnish french gambian georgian german greek haitian hungarian indian indonesian iranian iraqi irish israeli italian jamaican japanese jordanian kenyan korean kuwaiti latvian lebanese liberian libyan lithuanian macedonian malaysian mexican mongolian moroccan dutch nicaraguan nigerian norwegian omani pakistani palestinian filipino polish portuguese qatari romanian russian rwandan samoan saudi scottish senegalese serbian singaporean slovak somali sudanese swedish swiss syrian taiwanese thai tunisian ugandan ukrainian american hindi spanish venezuelan vietnamese welsh african european asian californian',

        // Negation marks
        neg: {
        },

        // Counter negation marks
        neg_neg: {
        },

        // Refusal keywords, also use as negation marks
        refusal: {
        },

        // Approval keyword
        approval: {
        },

        approval_verbs: [
        ],

        // Breakpoints words
        breakpoints: {

        },

        citations: {
            '"': '"',
            '\'': '"',
            '`': '"'
        },

        // Personal pronouns
        // filtered in lexicon (y?)
        p: {
        },

        // For date recognition
        months: {
        },
        days: {
        },

        indicators: {
        },

        // Profiling
        dirty: ''.split(' '),

        polite: ''.split(' ')
    });
}();

!function() {

    /*
        The following singularization/pluralization regexp rules have
        been extracted from http://code.google.com/p/inflection-js/
        and then improved given test unit results.
     */

    /*
      This is a list of nouns that use the same form for both singular and plural.
      This list should remain entirely in lower case to correctly match Strings.
    */
    var uncountable_words = [
            'tuna', 'trout', 'spacecraft', 'salmon', 'halibut', 'aircraft',
            'equipment', 'information', 'rice', 'money', 'species', 'series',
            'fish', 'sheep', 'moose', 'deer', 'news', 'asbestos'
        ],

        /*
          These rules translate from the singular form of a noun to its plural form.
        */
        plural_rules = [
            [/^index$/gi,                'indices'],
            [/^criterion$/gi,            'criteria'],
            [/dix$/gi,                   'dices'],
            [/(a|o)ch$/gi,               '$1chs'],
            [/(m)an$/gi,                 '$1en'],
            [/(pe)rson$/gi,              '$1ople'],
            [/(child)$/gi,               '$1ren'],
            [/^(ox)$/gi,                 '$1en'],
            [/(ax|test)is$/gi,           '$1es'],
            [/(octop|vir)us$/gi,         '$1i'],
            [/(alias|status)$/gi,        '$1es'],
            [/(bu)s$/gi,                 '$1ses'],
            [/(buffal|tomat|potat|her)o$/gi, '$1oes'],
            [/([ti])um$/gi,              '$1a'],
            [/sis$/gi,                   'ses'],
            [/(?:([^f])fe|([lr])f)$/gi,  '$1$2ves'],
            [/(hive)$/gi,                '$1s'],
            [/([^aeiouy]|qu)y$/gi,       '$1ies'],
            [/(x|ch|ss|sh)$/gi,          '$1es'],
            [/(matr|vert|ind)ix|ex$/gi,  '$1ices'],
            [/([m|l])ouse$/gi,           '$1ice'],
            [/(quiz)$/gi,                '$1zes'],
            [/^gas$/gi,                  'gases'],
            [/s$/gi,                     's'],
            [/$/gi,                      's']
        ],

        /*
          These rules translate from the plural form of a noun to its singular form.
        */
        singular_rules = [
            [/(m)en$/gi,                                                       '$1an'],
            [/(pe)ople$/gi,                                                    '$1rson'],
            [/(child)ren$/gi,                                                  '$1'],
            [/([ti])a$/gi,                                                     '$1um'],
            [/((a)naly|(b)a|(d)iagno|(p)arenthe|(p)rogno|(s)ynop|(t)he)ses/gi, '$1$2sis'],
            [/(hive)s$/gi,                                                     '$1'],
            [/(tive)s$/gi,                                                     '$1'],
            [/(curve)s$/gi,                                                    '$1'],
            [/([lr])ves$/gi,                                                   '$1f'],
            [/([^fo])ves$/gi,                                                  '$1fe'],
            [/([^aeiouy]|qu)ies$/gi,                                           '$1y'],
            [/(s)eries$/gi,                                                    '$1eries'],
            [/(m)ovies$/gi,                                                    '$1ovie'],
            [/(x|ch|ss|sh)es$/gi,                                              '$1'],
            [/([m|l])ice$/gi,                                                  '$1ouse'],
            [/(bus)es$/gi,                                                     '$1'],
            [/(o)es$/gi,                                                       '$1'],
            [/(shoe)s$/gi,                                                     '$1'],
            [/(cris|ax|test)es$/gi,                                            '$1is'],
            [/(octop|vir)i$/gi,                                                '$1us'],
            [/(alias|status)es$/gi,                                            '$1'],
            [/^(ox)en/gi,                                                      '$1'],
            [/(vert|ind)ices$/gi,                                              '$1ex'],
            [/(matr)ices$/gi,                                                  '$1ix'],
            [/(quiz)zes$/gi,                                                   '$1'],
            [/s$/gi,                                                           '']
        ],

        /*
          This is a helper method that applies rules based replacement to a String
          Signature:
            apply(str, rules, override) == String
          Arguments:
            str - String - String to modify and return based on the passed rules
            rules - Array: [RegExp, String] - Regexp to match paired with String to use for replacement
            override - String (optional) - String to return as though this method succeeded (used to conform to APIs)
          Returns:
            String - passed String modified by passed rules
          Examples:
            apply("cows", singular_rules) === 'cow'
        */
        apply =  function(str, rules, override) {
            var i,
                l;

            if (uncountable_words.indexOf(str.toLowerCase()) > -1) {
                return str;
            }

            for (i = 0, l = rules.length; i < l; i ++) {
                if (str.match(rules[i][0])) {
                    str = str.replace(rules[i][0], rules[i][1]);
                    break;
                }
            }
            return str;
        },

        match = function(str, rules) {
            var i,
                l;

            if (uncountable_words.indexOf(str.toLowerCase()) > -1) {
                return false;
            }

            for (i = 0, l = rules.length; i < l; i ++) {
                if (str.match(rules[i][0])) {
                    return true;
                }
            }
            return false;
        },


        /*
            Conjugation methods. Following methods are experimental for now.
        */

        VBZ = 'VBZ',
        VBG = 'VBG',
        VBN = 'VBN',

        conjugateLongVowelConsonant = function (vb, to) {
            if (to === VBZ) {
                return vb + 's';
            } else if (to === VBG) {
                return vb + 'ing';
            } else if (to === VBN) {
                return vb + 'ed';
            }
            return vb;
        },

        conjugateShortVowelConsonant = function (vb, to) {
            if (to === VBZ) {
                return vb + 's';
            } else if (to === VBG) {
                return vb + vb[vb.length - 1] + 'ing';
            } else if (to === VBN) {
                return vb + vb[vb.length - 1] + 'ed';
            }
            return vb;
        },

        conjugateConsonentE = function(vb, to) {
            var base = vb.slice(0, vb.length - 1);
            if (to === VBZ) {
                return vb + 's';
            } else if (to === VBG) {
                return base + 'ing';
            } else if (to === VBN) {
                return base + 'ed';
            }
            return vb;
        },

        conjugateConsonantY = function(vb, to) {
            var base = vb.slice(0, vb.length - 1);
            if (to === VBZ) {
                return base + 'ies';
            } else if (to === VBG) {
                return vb + 'ing';
            } else if (to === VBN) {
                return base + 'ied';
            }
            return vb;
        },

        conjugateEe = function(vb, to) {
            if (to === VBZ) {
                return vb + 's';
            } else if (to === VBG) {
                return vb + 'ing';
            } else if (to === VBN) {
                return vb + 'd';
            }
            return vb;
        },

        conjugateUe = function(vb, to) {
            if (to === VBZ) {
                return vb + 's';
            } else if (to === VBG) {
                return vb.slice(0, vb.length - 1) + 'ing';
            } else if (to === VBN) {
                return vb + 'd';
            }
            return vb;
        },
        conjugateIe = function(vb, to) {
            if (to === VBZ) {
                return vb + 's';
            } else if (to === VBG) {
                return vb.slice(0, vb.length - 2) + 'ying';
            } else if (to === VBN) {
                return vb + 'd';
            }
            return vb;
        };

        var conjugateSibilant = function(vb, to) {
            if (to === VBZ) {
                return vb + 'es';
            } else if (to === VBG) {
                return vb + 'ing';
            } else if (to === VBN) {
                return vb + 'ed';
            }
            return vb;
        };

    extend(inflector, {
        /**
         * Test if given token (supposed noun) is singular.
         *
         * @memberOf compendium.inflector
         * @param  {String}  str Token to test
         * @return {Boolean}     Value `true` if token is singular, `false` otherwise
         */
        isSingular: function(str) {
            return inflector.isUncountable(str) || match(str, plural_rules);
        },

        /**
         * Test if given token (supposed noun) is plural.
         *
         * @memberOf compendium.inflector
         * @param  {String}  str Token to test
         * @return {Boolean}     Value `true` if token is plural, `false` otherwise
         */
        isPlural: function(str) {
            if (str.match(/([saui]s|[^i]a)$/gi)) {
                return false;
            }
            return match(str, singular_rules);
        },

        /**
         * Test if given token (supposed noun) is uncountable.
         *
         * @memberOf compendium.inflector
         * @param  {String}  str Token to test
         * @return {Boolean}     Value `true` if token is plural, `false` otherwise
         */
        isUncountable: function(str) {
            return uncountable_words.indexOf(str) > -1;
        },

        /**
         * Singularize given noun. Will test if given noun is plural before
         * singularizing it.
         *
         * @memberOf compendium.inflector
         * @param  {String} str Noun to singularize
         * @return {String}     The singularized noun
         */
        singularize: function(str) {
            return inflector.isPlural(str) ? apply(str, singular_rules) : str;
        },

        /**
         * Pluralize given noun. Will test if given noun is singular before
         * pluralizing it.
         *
         * @memberOf compendium.inflector
         * @param  {String} str Noun to singularize
         * @return {String}     The pluralized noun
         */
        pluralize: function(str) {
            return inflector.isSingular(str) ? apply(str, plural_rules) : str;
        },



        /**
         * Conjugate a verb from its infinitive to given tense.
         *
         * @memberOf compendium.inflector
         * @param  {String} vb Infinitive verb
         * @param  {String} to PoS tag to conjugate to (`VBZ` for third-person present, `VBN` for past tense, `VBG` for gerund)
         * @return {String}    The conjugated verb if a rule has been found, `null` otherwise
         */
        conjugate: function(vb, to) {
            var l = vb[vb.length - 1];
            if (vb.match(/[^aeiou]y$/gi)) {
                return conjugateConsonantY(vb, to);
            } else if (vb.match(/[^aeiouy]e$/gi)) {
                return conjugateConsonentE(vb, to)
            } else if (vb.match(/([aeiuo][ptlgnm]|ir|cur|[^aeiuo][oua][db])$/gi)) {
                return conjugateShortVowelConsonant(vb, to)
            } else if (vb.match(/([ieao]ss|[aeiouy]zz|[aeiouy]ch|nch|rch|[aeiouy]sh|[iae]tch|ax)$/gi)) {
                return conjugateSibilant(vb, to)
            } else if (vb.match(/(ee)$/gi)) {
                return conjugateEe(vb, to)
            } else if (vb.match(/(ie)$/gi)) {
                return conjugateIe(vb, to)
            } else if (vb.match(/(ue)$/gi)) {
                return conjugateUe(vb, to)
            } else if (vb.match(/([uao]m[pb]|[oa]wn|ey|elp|[ei]gn|ilm|o[uo]r|[oa]ugh|igh|ki|ff|oubt|ount|awl|o[alo]d|[iu]rl|upt|[oa]y|ight|oid|empt|act|aud|e[ea]d|ound|[aeiou][srcln]t|ept|dd|[eia]n[dk]|[ioa][xk]|[oa]rm|[ue]rn|[ao]ng|uin|eam|ai[mr]|[oea]w|[eaoui][rscl]k|[oa]r[nd]|ear|er|it|ll)$/gi)) {
                return conjugateLongVowelConsonant(vb, to)
            }

            return null;
        },


        /**
         * Conjugate a verb from its infinitive to past tense.
         * This function is a shortcut for `compendium.inflector.conjugate(vb, 'VBN');`.
         *
         * @memberOf compendium.inflector
         * @param  {String} vb Infinitive verb
         * @return {String}    The conjugated verb if a rule has been found, `null` otherwise
         */
        toPast: function(vb) {
            return inflector.conjugate(vb, VBN);
        },


        /**
         * Conjugate a verb from its infinitive to its gerund.
         * This function is a shortcut for `compendium.inflector.conjugate(vb, 'VBG');`.
         *
         * @memberOf compendium.inflector
         * @param  {String} vb Infinitive verb
         * @return {String}    The conjugated verb if a rule has been found, `null` otherwise
         */
        toGerund: function(vb) {
            return inflector.conjugate(vb, VBG);
        },

        /**
         * Conjugate a verb from its infinitive to the third-person present tense.
         * This function is a shortcut for `compendium.inflector.conjugate(vb, 'VBZ');`.
         *
         * @memberOf compendium.inflector
         * @param  {String} vb Infinitive verb
         * @return {String}    The conjugated verb if a rule has been found, `null` otherwise
         */
        toPresents: function(vb) {
            return inflector.conjugate(vb, VBZ);
        },

        /**
         * Get the infinitive of given verb token.
         * @param  {String} vb lowercased conjugated verb
         * @return {String}    Lowercased infinitive verb if found, `null` otherwise
         */
        infinitive: function(vb) {
            // Find in lexicon
            var item = compendium.lexicon[vb];
            if (!!item && item.hasOwnProperty('infinitive')) {
                return item['infinitive'];
            }

            if (vb === 'are' || vb === 'am' || vb === '\'s') {
                return 'be';
            }

            // Otherwise return null
            return null;
        }

    });
    compendium.inflector = inflector;

}();
!function() {


    var isPlural = compendium.inflector.isPlural,

        // Brill's conditions
        STARTWORD = 0,
        PREV1OR2TAG = 1,
        PREVTAG = 2,
        NEXTTAG = 3,
        NEXTTAG2 = 4,
        PREVTAG2 = 41,
        PREVWORD = 5,
        PREVWORDPREVTAG = 51,
        CURRENTWD = 6,
        WDPREVTAG = 8,
        WDPREVWD = 81,
        NEXT1OR2OR3TAG = 9,
        NEXTBIGRAM = 10,
        NEXT2WD = 11,
        WDNEXTTAG = 12,
        WDNEXTWD = 121,
        PREV1OR2OR3TAG = 13,
        SURROUNDTAG = 14,
        SURROUNDTAGWD = 141,
        NEXTWD = 15,
        NEXT1OR2TAG = 16,
        PREV2TAG = 17,
        NEXT2TAG = 171,
        PREV2TAGNEXTTAG = 172,
        NEXT1OR2WD = 18,
        PREV2WD = 19,
        RBIGRAM = 20,
        PREV1OR2WD = 21,

        lexicon = compendium.lexicon,
        emots = cpd.emots,
        rules = cpd.rules,
        rulesLength = rules.length,
        suffixes = cpd.suffixes,
        suffixesLength = suffixes.length,

        complexFloat = new RegExp('^-?[0-9]+([\\' + cpd.thousandChar + '][0-9]+){1,}(\\' + cpd.floatChar + '[0-9]+)$'),

        jsKeywords = ['constructor'],

        removeRepetitiveChars = function(token) {
            var str = token.replace(/(.)\1{2,}/g, "$1$1"), s;
            if (compendium.lexicon.hasOwnProperty(str)) {
                return str;
            }
            s = compendium.synonym(str);
            if (s !== str) {
                return s;
            }
            str = token.replace(/(.)\1{1,}/g, "$1");
            if (compendium.lexicon.hasOwnProperty(str)) {
                return str;
            }
            s = compendium.synonym(str);
            if (s !== str) {
                return s;
            }

            return null;
        },

        matchPotentialProperNoun = function(token) {
            return token.match(/^[A-Z][a-z\.]+$/g) || token.match(/^[A-Z]+[0-9]+$/g) || token.match(/^[A-Z][a-z]+[A-Z][a-z]+$/g);
        };

    extend(pos, {


        applyRule: function(rule, token, tag, index, tokens, tags, run) {
            if (rule.from !== tag || (rule.secondRun && run === 0)) {
                return;
            }
            var type = rule.type,
                tmp,
                tmp2;
            // Start word rule is case sensitive
            if (type === STARTWORD) {
                if (index === 0 && token === rule.c1) {
                    tags[index] = rule.to;
                    return;
                }
                return;
            }


            token = token.toLowerCase();
            if (type === PREVTAG) {
                if (index > 0 && tags[index - 1] === rule.c1) {
                    tags[index] = rule.to;
                    return;
                }
            } else if (type === PREVWORDPREVTAG) {
                tmp = tokens[index - 1] || '';
                if (tags[index - 1] === rule.c2 && tmp.toLowerCase() === rule.c1) {
                    tags[index] = rule.to;
                    return;
                }
            } else if (type === NEXTTAG) {
                if (tags[index + 1] === rule.c1) {
                    tags[index] = rule.to;
                    return;
                }
            } else if (type === NEXTTAG2) {
                if (tags[index + 2] === rule.c1) {
                    tags[index] = rule.to;
                    return;
                }
            } else if (type === PREVTAG2) {
                if (tags[index - 2] === rule.c1) {
                    tags[index] = rule.to;
                    return;
                }
            } else if (type === PREV1OR2TAG) {
                if (tags[index - 1] === rule.c1 || tags[index - 2] === rule.c1) {
                    tags[index] = rule.to;
                    return;
                }
            } else if (type === PREVWORD) {
                tmp = tokens[index - 1] || '';
                if (tmp.toLowerCase() === rule.c1) {
                    tags[index] = rule.to;
                    return;
                }
            } else if (type === CURRENTWD) {
                if (token === rule.c1) {
                    tags[index] = rule.to;
                    return;
                }
            } else if (type === WDPREVTAG) {
                if (token === rule.c2 && tags[index - 1] === rule.c1) {
                    tags[index] = rule.to;
                    return;
                }
            } else if (type === WDPREVWD) {
                tmp = tokens[index - 1] || '';
                if (token === rule.c2 && tmp.toLowerCase() === rule.c1) {
                    tags[index] = rule.to;
                    return;
                }
            } else if (type === NEXT1OR2OR3TAG) {
                if (tags[index + 1] === rule.c1 || tags[index + 2] === rule.c1 || tags[index + 3] === rule.c1) {
                    tags[index] = rule.to;
                    return;
                }
            } else if (type === NEXT2WD) {
                tmp = tokens[index + 2] || '';
                if (tmp.toLowerCase() === rule.c1) {
                    tags[index] = rule.to;
                    return;
                }
            } else if (type === WDNEXTWD) {
                tmp = tokens[index + 1] || '';
                if (token === rule.c1 && tmp.toLowerCase() === rule.c2) {
                    tags[index] = rule.to;
                    return;
                }
            } else if (type === WDNEXTTAG) {
                if (token === rule.c1 && tags[index + 1] === rule.c2) {
                    tags[index] = rule.to;
                    return;
                }
            } else if (type === PREV1OR2OR3TAG) {
                if (tags[index - 1] === rule.c1 || tags[index - 2] === rule.c1 || tags[index - 3] === rule.c1) {
                    tags[index] = rule.to;
                    return;
                }
            } else if (type === SURROUNDTAG) {
                if (tags[index - 1] === rule.c1 && tags[index + 1] === rule.c2) {
                    tags[index] = rule.to;
                    return;
                }
            } else if (type === SURROUNDTAGWD) {
                if (token === rule.c1 && tags[index - 1] === rule.c2 && tags[index + 1] === rule.c3) {
                    tags[index] = rule.to;
                    return;
                }
            } else if (type === NEXTWD) {
                tmp = tokens[index + 1] || '';
                if (tmp.toLowerCase() === rule.c1) {
                    tags[index] = rule.to;
                    return;
                }
            } else if (type === NEXT1OR2TAG) {
                if (tags[index + 1] === rule.c1 || tags[index + 2] === rule.c1) {
                    tags[index] = rule.to;
                    return;
                }
            } else if (type === PREV2TAG) {
                if (tags[index - 2] === rule.c1 && tags[index - 1] === rule.c2) {
                    tags[index] = rule.to;
                    return;
                }
            } else if (type === PREV2TAGNEXTTAG) {
                if (tags[index - 2] === rule.c1 && tags[index - 1] === rule.c2 && tags[index + 1] === rule.c3) {
                    tags[index] = rule.to;
                    return;
                }
            } else if (type === NEXT2TAG) {
                if (tags[index + 1] === rule.c1 && tags[index + 2] === rule.c2) {
                    tags[index] = rule.to;
                    return;
                }
            } else if (type === NEXT1OR2WD) {
                tmp = tokens[index + 1] || '';
                tmp2 = tokens[index + 2] || '';
                if (tmp.toLowerCase() === rule.c1 || tmp2.toLowerCase() === rule.c1) {
                    tags[index] = rule.to;
                    return;
                }
            } else if (type === PREV2WD) {
                tmp2 = tokens[index - 2] || '';
                if (tmp2.toLowerCase() === rule.c1) {
                    tags[index] = rule.to;
                    return;
                }
            } else if (type === PREV1OR2WD) {
                tmp = tokens[index - 1] || '';
                tmp2 = tokens[index - 2] || '';
                if (tmp.toLowerCase() === rule.c1 || tmp2.toLowerCase() === rule.c1) {
                    tags[index] = rule.to;
                    return;
                }
            } else if (type === PREV1OR2TAG) {
                tmp = tags[index - 1] || '';
                tmp2 = tags[index - 2] || '';
                if (tmp === rule.c1 || tmp2 === rule.c1) {
                    tags[index] = rule.to;
                    return;
                }
            }

            return;
        },

        // Apply all rules on given token/tag combo
        applyRules: function(token, index, tokens, tags, run) {
            var i;
            for (i = 0; i < rulesLength; i ++) {
                pos.applyRule(rules[i], token, tags[index], index, tokens, tags, run);
            }
        },

        // Apply all rules twice on given arrays of tokens and tags
        apply: function(tokens, tags, blocked) {
            var i, l = tokens.length, j = 0;
            while (j < 2) {
                for (i = 0; i < l; i ++) {
                    if (blocked[i] !== true) {
                        this.applyRules(tokens[i], i, tokens, tags, j);
                    }
                }
                j ++;
            }
            return tags;
        },

        testSuffixes: function(token) {
            var i;
            for (i = 0; i < suffixesLength; i ++) {
                if (token.match(suffixes[i].regexp)) {
                    return suffixes[i].pos;
                }
            }

            return null;
        },

        getTag: function(token) {
            var tag,
                tagObject = factory.tag(),
                j,
                tl,
                lower,
                tmp;

            tagObject.norm = token;

            if (token.length > 1) {
                tag = null;
                for (j = 0, tl = emots.length; j < tl; j ++) {
                    if (token.indexOf(emots[j]) === 0) {
                        tagObject.tag = 'EM';
                        tagObject.blocked = true;
                        tagObject.confidence = 1;
                        return tagObject;
                    }
                }
            }

            // Attempt to get pos in a case sensitive way
            tag = compendium.lexicon[jsKeywords.indexOf(token) > -1 ? '_' + token : token];

            if (!!tag && tag !== '-') {
                tagObject.tag = tag;
                tagObject.blocked = tag.blocked;
                tagObject.confidence = 1;
                return tagObject;
            }

            lower = token.toLowerCase();

            // Test synonyms
            tmp = compendium.synonym(lower);
            if (tmp !== lower) {
                tag = compendium.lexicon[tmp];

                if (!!tag) {
                    tagObject.tag = tag;
                    tagObject.confidence = 1;
                    return tagObject;
                }
            }

            // Test chars streak
            if (lower.match(/(\w)\1+/g)) {
                tmp = removeRepetitiveChars(lower);
                if (!!tmp) {
                    tagObject.norm = tmp;
                    tag = compendium.lexicon[tmp];

                    tagObject.tag = tag;
                    tagObject.confidence = 0.8;
                    return tagObject;
                }
            }

            // If none, try with lower cased
            if (typeof token === 'string' && token.match(/[A-Z]/g)) {
                tag = compendium.lexicon[lower];

                if (!!tag && tag !== '-') {
                    tagObject.tag = tag;
                    tagObject.confidence = 0.75;
                    return tagObject;
                }
            }

            // Test common suffixes.
            tag = pos.testSuffixes(token);
            if (!!tag) {
                tagObject.tag = tag;
                tagObject.confidence = 0.25;
                return tagObject;
            }

            // If no tag, check composed words
            if (token.indexOf('-') > -1) {
                // If capitalized, likely NNP
                if (token.match(/^[A-Z]/g)) {
                    tagObject.tag = 'NNP';
                // Composed words are very often adjectives
                } else {
                    tagObject.tag = 'JJ';
                }
                tagObject.confidence /= 2;
                return tagObject;
            }

            // We default to NN if still no tag
            return tagObject;
        },

        // Tag a tokenized sentence.
        // Apply three passes:
        // 1. Guess a tag based on lexicon + prefixes (see `suffixes.txt`)
        // 2. Manual tranformation rules
        // 3. Apply Brill's conditions from `rules.txt`
        tag: function(sentence) {
            var tags = [],
                blocked = [],
                norms = [],
                token,
                tag,
                i,
                j,
                l = sentence.length,
                tl,
                lower,
                tmp,
                previous,
                inNNP = false,
                confidence = 0,

                append = function(tag, c, b) {
                    tag = typeof tag === 'object' ? tag.pos : tag;
                    tags.push(tag === '-' ? 'NN' : tag);
                    blocked.push(typeof b === 'boolean' ? b : false);
                    confidence += c;
                };

            // Basic tagging based on lexicon and
            // suffixes
            for (i = 0; i < l; i ++) {
                token = sentence[i];
                norms[i] = token;

                // Symbols
                if (token.match(/^[%\+\-\/@]$/g)) {
                    append('SYM', 1, true);
                    continue;
                }

                // Punc signs
                if (token.match(/^(\?|\!|\.){1,}$/g)) {
                    append('.', 1, true);
                    continue;
                }

                // Numbers
                if (token.match(/^-?[0-9]+([\.,][0-9]+)?$/g) ||
                    token.match(complexFloat) ||
                    // years
                    token.match(/^([0-9]{2}|[0-9]{4})s$/g) ||
                    //range
                    token.match(/^[0-9]{2,4}-[0-9]{2,4}$/g)) {
                    append('CD', 1, true);
                    continue;
                }


                tmp = pos.getTag(sentence[i]);
                append(tmp.tag, tmp.confidence, tmp.blocked);
                norms[i] = tmp.norm;
            }

            // Manual transformational rules
            for (i = 0; i < l; i ++) {
                tag = tags[i];

                if (tag === 'SYM' || tag === '.') {
                    continue;
                }



                token = sentence[i];
                lower = token.toLowerCase();
                tl = token.length;
                previous = (i === 0 ? '' : tags[i - 1]);

                // First position rules
                if (i === 0) {
                    // Special case extracted form penn treebank testin
                    if (lower === 'that') {
                        tags[i] = 'DT';
                        confidence ++;
                        continue;
                    }

                    // First position infinitive verb
                    if ((tag === 'NN' || tag === 'VB') && cpd.infinitives.indexOf(lower) > -1) {
                        tags[i] = 'VB';
                        confidence ++;
                        continue;
                    }
                }

                // Convert a noun to a past participle if token ends with 'ed'
                if (tl > 3 && token.match(/[^e]ed$/gi) &&
                    (tag.indexOf('N') === 0) &&
                    (i === 0 || !token.match(/^[A-Z][a-z]+/g))) {
                    tags[i] = 'VBN';
                    continue;
                }

                // Convert a common noun to a present participle verb (i.e., a gerund)
                if (tl > 4 && token.lastIndexOf('ing') === tl - 3 &&
                    cpd.ing_excpt.indexOf(lower) === -1 &&
                    (tag.indexOf('N') === 0 || tag === 'MD') &&
                    (i === 0 || !token.match(/^[A-Z][a-z]+/g)) &&
                    previous !== 'NN' && previous !== 'JJ' && previous !== 'DT' && previous !== 'VBG') {
                    tags[i] = 'VBG';
                    continue;
                }

                // in(g) gerund inference with missing 'g'
                // - 0.002% on Penn Treebank, but VERY useful for
                // casual language
                if (tl > 4 && lower.lastIndexOf('in') === tl - 2 && tag === 'NN' &&
                    (i === 0 || !token.match(/^[A-Z][a-z]+/g)) &&
                    previous !== 'NN' && previous !== 'JJ' && previous !== 'DT' && previous !== 'VBG') {
                    tmp = compendium.lexicon[lower + 'g'];
                    if (!!tmp && tmp.pos === 'VBG') {
                        tags[i] = 'VBG';
                        continue;
                    }
                }

                // Check if word could be an infinitive verb
                // + 0.18% on Penn Treebank
                if (previous === 'TO' && cpd.infinitives.indexOf(lower) > -1) {
                    tag = 'VB';
                }

                // Roman numerals, except for I (handled by some brill rules)
                if (previous !== 'DT' && token.match(/^[IVXLCDM]+$/g) && token !== 'I') {
                    tag = 'CD';
                }

                // Proper noun inference
                if (tag === 'NN' ||
                    tag === 'VB' ||
                    (tag === 'JJ' && cpd.nationalities.hasOwnProperty(lower) === false)) {


                    // All uppercased or an acronym, probably NNP
                    if (token.match(/^[A-Z]+$/g) || token.match(/^([a-z]{1}\.)+/gi)) {
                        tag = 'NNP';
                        inNNP = true;
                    // Capitalized words. First token is skipped for this test
                    } else if (i > 0 && matchPotentialProperNoun(token)) {
                        tag = 'NNP';
                        inNNP = true;

                        // First token handled here, avoiding most false positives
                        // of first word of sentence, that is capitalized.
                        // Put in other words, an initial NN or JJ is converted into NNP
                        // only if second word is also an NNP.
                        tmp = sentence[i - 1];
                        if (i === 1 && (previous === 'NN' || previous === 'NNS' || previous === 'JJ' || previous === 'VB') &&
                            matchPotentialProperNoun(tmp)) {
                            tags[i - 1] = 'NNP';
                        }
                    } else {
                        inNNP = false;
                    }

                // Add Roman numeral to proper nouns if we're currently in an NNP
                } else if (inNNP && ((tag === 'CD' && token.match(/^[IVXLCDM]+$/g)) || token === 'I')) {
                    tag = 'NNP';
                } else {
                    inNNP = tag === 'NNP' || tag === 'NNPS';
                }

                // Use inflector to detect plural nouns
                if (tag === 'NN' && isPlural(token)) {
                    tag = 'NNS';
                }

                tags[i] = tag;
            }

            // Finally apply Brill's rules
            pos.apply(sentence, tags, blocked);

            // Last round of manual transformational rules
            for (i = 0; i < l; i ++) {
                token = sentence[i];
                tag = tags[i];
                previous = tags[i - 1] || '';
                if (token.match(/ed$/g)) {
                    if (tag === 'JJ' && (previous === 'VBZ' || previous === 'VBP') && tags[i + 1] === 'TO') {
                        tag = 'VBN';
                    }
                }

                tags[i] = tag;
            }
            return {
                tags: tags,
                norms: norms,
                confidence: confidence / l
            };
        }

    });

    compendium.tag = pos.tag;

}();

!function(){

    var not_words = ['#', 'SYM', 'CR', 'EM'];
 

    compendium.stat = function(sentence) {
        var i,
            token,
            raw,
            l = sentence.length,
            ln = l, // Normalized length
            stats = sentence.stats,
            tokens_length = 0,
            foreign = 0,
            uppercased = 0,
            words = 0,
            tag,
            capitalized = 0;

        for (i = 0; i < l; i ++) {
            token = sentence.tokens[i];
            raw = token.raw;
            tokens_length += raw.length;
            tag = sentence.tags[i];

            if (token.attr.is_punc || not_words.indexOf(tag) > -1) {
                ln --;
                continue;
            }
            words += 1;

            if (raw.match(/^[A-Z][a-zA-Z]+$/g)) {
                capitalized ++;
            }
            if (raw.match(/[A-Z]+/) && !raw.match(/[a-z]/)) {
                uppercased ++;
            }

            if (tag === 'FW') {
                foreign ++;
            }
        }

        if (ln === 0) {
            ln = 1;
        }

        stats.words = words;
        stats.p_foreign = foreign * 100 / ln;
        stats.p_upper = uppercased * 100 / ln;
        stats.p_cap = capitalized * 100 / ln;
        stats.avg_length = tokens_length / ln;
    };
}();
!function() {
    var s = cpd.synonyms,
        l = s.length;

    compendium.synonym = function(str) {
        var i;
        for (i = 0; i < l; i ++) {
            if (s[i].indexOf(str) > 0) {
                return s[i][0];
            }
        }
        return str;
    };

}();

/*! https://mths.be/punycode v1.3.2 by @mathias */
!function() {


    /**
     * The `punycode` object.
     * @name punycode
     * @type Object
     */
    var punycode,

    /** Highest positive signed 32-bit float value */
    maxInt = 2147483647, // aka. 0x7FFFFFFF or 2^31-1

    /** Bootstring parameters */
    base = 36,
    tMin = 1,
    tMax = 26,
    skew = 38,
    damp = 700,
    initialBias = 72,
    initialN = 128, // 0x80
    delimiter = '-', // '\x2D'

    /** Regular expressions */
    regexPunycode = /^xn--/,
    regexNonASCII = /[^\x20-\x7E]/, // unprintable ASCII chars + non-ASCII chars
    regexSeparators = /[\x2E\u3002\uFF0E\uFF61]/g, // RFC 3490 separators

    /** Error messages */
    errors = {
        'overflow': 'Overflow: input needs wider integers to process',
        'not-basic': 'Illegal input >= 0x80 (not a basic code point)',
        'invalid-input': 'Invalid input'
    },

    /** Convenience shortcuts */
    baseMinusTMin = base - tMin,
    floor = Math.floor,
    stringFromCharCode = String.fromCharCode,

    /** Temporary variable */
    key;

    /*--------------------------------------------------------------------------*/

    /**
     * A generic error utility function.
     * @private
     * @param {String} type The error type.
     * @returns {Error} Throws a `RangeError` with the applicable error message.
     */
    function error(type) {
        throw new RangeError(errors[type]);
    }

    /**
     * A generic `Array#map` utility function.
     * @private
     * @param {Array} array The array to iterate over.
     * @param {Function} callback The function that gets called for every array
     * item.
     * @returns {Array} A new array of values returned by the callback function.
     */
    function map(array, fn) {
        var length = array.length;
        var result = [];
        while (length--) {
            result[length] = fn(array[length]);
        }
        return result;
    }

    /**
     * A simple `Array#map`-like wrapper to work with domain name strings or email
     * addresses.
     * @private
     * @param {String} domain The domain name or email address.
     * @param {Function} callback The function that gets called for every
     * character.
     * @returns {Array} A new string of characters returned by the callback
     * function.
     */
    function mapDomain(string, fn) {
        var parts = string.split('@');
        var result = '';
        if (parts.length > 1) {
            // In email addresses, only the domain name should be punycoded. Leave
            // the local part (i.e. everything up to `@`) intact.
            result = parts[0] + '@';
            string = parts[1];
        }
        // Avoid `split(regex)` for IE8 compatibility. See #17.
        string = string.replace(regexSeparators, '\x2E');
        var labels = string.split('.');
        var encoded = map(labels, fn).join('.');
        return result + encoded;
    }

    /**
     * Creates an array containing the numeric code points of each Unicode
     * character in the string. While JavaScript uses UCS-2 internally,
     * this function will convert a pair of surrogate halves (each of which
     * UCS-2 exposes as separate characters) into a single code point,
     * matching UTF-16.
     * @see `punycode.ucs2.encode`
     * @see <https://mathiasbynens.be/notes/javascript-encoding>
     * @memberOf punycode.ucs2
     * @name decode
     * @param {String} string The Unicode input string (UCS-2).
     * @returns {Array} The new array of code points.
     */
    function ucs2decode(string) {
        var output = [],
            counter = 0,
            length = string.length,
            value,
            extra;
        while (counter < length) {
            value = string.charCodeAt(counter++);
            if (value >= 0xD800 && value <= 0xDBFF && counter < length) {
                // high surrogate, and there is a next character
                extra = string.charCodeAt(counter++);
                if ((extra & 0xFC00) == 0xDC00) { // low surrogate
                    output.push(((value & 0x3FF) << 10) + (extra & 0x3FF) + 0x10000);
                } else {
                    // unmatched surrogate; only append this code unit, in case the next
                    // code unit is the high surrogate of a surrogate pair
                    output.push(value);
                    counter--;
                }
            } else {
                output.push(value);
            }
        }
        return output;
    }

    /**
     * Creates a string based on an array of numeric code points.
     * @see `punycode.ucs2.decode`
     * @memberOf punycode.ucs2
     * @name encode
     * @param {Array} codePoints The array of numeric code points.
     * @returns {String} The new Unicode string (UCS-2).
     */
    function ucs2encode(array) {
        return map(array, function(value) {
            var output = '';
            if (value > 0xFFFF) {
                value -= 0x10000;
                output += stringFromCharCode(value >>> 10 & 0x3FF | 0xD800);
                value = 0xDC00 | value & 0x3FF;
            }
            output += stringFromCharCode(value);
            return output;
        }).join('');
    }

    /**
     * Converts a basic code point into a digit/integer.
     * @see `digitToBasic()`
     * @private
     * @param {Number} codePoint The basic numeric code point value.
     * @returns {Number} The numeric value of a basic code point (for use in
     * representing integers) in the range `0` to `base - 1`, or `base` if
     * the code point does not represent a value.
     */
    function basicToDigit(codePoint) {
        if (codePoint - 48 < 10) {
            return codePoint - 22;
        }
        if (codePoint - 65 < 26) {
            return codePoint - 65;
        }
        if (codePoint - 97 < 26) {
            return codePoint - 97;
        }
        return base;
    }

    /**
     * Converts a digit/integer into a basic code point.
     * @see `basicToDigit()`
     * @private
     * @param {Number} digit The numeric value of a basic code point.
     * @returns {Number} The basic code point whose value (when used for
     * representing integers) is `digit`, which needs to be in the range
     * `0` to `base - 1`. If `flag` is non-zero, the uppercase form is
     * used; else, the lowercase form is used. The behavior is undefined
     * if `flag` is non-zero and `digit` has no uppercase form.
     */
    function digitToBasic(digit, flag) {
        //  0..25 map to ASCII a..z or A..Z
        // 26..35 map to ASCII 0..9
        return digit + 22 + 75 * (digit < 26) - ((flag != 0) << 5);
    }

    /**
     * Bias adaptation function as per section 3.4 of RFC 3492.
     * https://tools.ietf.org/html/rfc3492#section-3.4
     * @private
     */
    function adapt(delta, numPoints, firstTime) {
        var k = 0;
        delta = firstTime ? floor(delta / damp) : delta >> 1;
        delta += floor(delta / numPoints);
        for (/* no initialization */; delta > baseMinusTMin * tMax >> 1; k += base) {
            delta = floor(delta / baseMinusTMin);
        }
        return floor(k + (baseMinusTMin + 1) * delta / (delta + skew));
    }

    /**
     * Converts a Punycode string of ASCII-only symbols to a string of Unicode
     * symbols.
     * @memberOf punycode
     * @param {String} input The Punycode string of ASCII-only symbols.
     * @returns {String} The resulting string of Unicode symbols.
     */
    function decode(input) {
        // Don't use UCS-2
        var output = [],
            inputLength = input.length,
            out,
            i = 0,
            n = initialN,
            bias = initialBias,
            basic,
            j,
            index,
            oldi,
            w,
            k,
            digit,
            t,
            /** Cached calculation results */
            baseMinusT;

        // Handle the basic code points: let `basic` be the number of input code
        // points before the last delimiter, or `0` if there is none, then copy
        // the first basic code points to the output.

        basic = input.lastIndexOf(delimiter);
        if (basic < 0) {
            basic = 0;
        }

        for (j = 0; j < basic; ++j) {
            // if it's not a basic code point
            if (input.charCodeAt(j) >= 0x80) {
                error('not-basic');
            }
            output.push(input.charCodeAt(j));
        }

        // Main decoding loop: start just after the last delimiter if any basic code
        // points were copied; start at the beginning otherwise.

        for (index = basic > 0 ? basic + 1 : 0; index < inputLength; /* no final expression */) {

            // `index` is the index of the next character to be consumed.
            // Decode a generalized variable-length integer into `delta`,
            // which gets added to `i`. The overflow checking is easier
            // if we increase `i` as we go, then subtract off its starting
            // value at the end to obtain `delta`.
            for (oldi = i, w = 1, k = base; /* no condition */; k += base) {

                if (index >= inputLength) {
                    error('invalid-input');
                }

                digit = basicToDigit(input.charCodeAt(index++));

                if (digit >= base || digit > floor((maxInt - i) / w)) {
                    error('overflow');
                }

                i += digit * w;
                t = k <= bias ? tMin : (k >= bias + tMax ? tMax : k - bias);

                if (digit < t) {
                    break;
                }

                baseMinusT = base - t;
                if (w > floor(maxInt / baseMinusT)) {
                    error('overflow');
                }

                w *= baseMinusT;

            }

            out = output.length + 1;
            bias = adapt(i - oldi, out, oldi == 0);

            // `i` was supposed to wrap around from `out` to `0`,
            // incrementing `n` each time, so we'll fix that now:
            if (floor(i / out) > maxInt - n) {
                error('overflow');
            }

            n += floor(i / out);
            i %= out;

            // Insert `n` at position `i` of the output
            output.splice(i++, 0, n);

        }

        return ucs2encode(output);
    }

    /**
     * Converts a string of Unicode symbols (e.g. a domain name label) to a
     * Punycode string of ASCII-only symbols.
     * @memberOf punycode
     * @param {String} input The string of Unicode symbols.
     * @returns {String} The resulting Punycode string of ASCII-only symbols.
     */
    function encode(input) {
        var n,
            delta,
            handledCPCount,
            basicLength,
            bias,
            j,
            m,
            q,
            k,
            t,
            currentValue,
            output = [],
            /** `inputLength` will hold the number of code points in `input`. */
            inputLength,
            /** Cached calculation results */
            handledCPCountPlusOne,
            baseMinusT,
            qMinusT;

        // Convert the input in UCS-2 to Unicode
        input = ucs2decode(input);

        // Cache the length
        inputLength = input.length;

        // Initialize the state
        n = initialN;
        delta = 0;
        bias = initialBias;

        // Handle the basic code points
        for (j = 0; j < inputLength; ++j) {
            currentValue = input[j];
            if (currentValue < 0x80) {
                output.push(stringFromCharCode(currentValue));
            }
        }

        handledCPCount = basicLength = output.length;

        // `handledCPCount` is the number of code points that have been handled;
        // `basicLength` is the number of basic code points.

        // Finish the basic string - if it is not empty - with a delimiter
        if (basicLength) {
            output.push(delimiter);
        }

        // Main encoding loop:
        while (handledCPCount < inputLength) {

            // All non-basic code points < n have been handled already. Find the next
            // larger one:
            for (m = maxInt, j = 0; j < inputLength; ++j) {
                currentValue = input[j];
                if (currentValue >= n && currentValue < m) {
                    m = currentValue;
                }
            }

            // Increase `delta` enough to advance the decoder's <n,i> state to <m,0>,
            // but guard against overflow
            handledCPCountPlusOne = handledCPCount + 1;
            if (m - n > floor((maxInt - delta) / handledCPCountPlusOne)) {
                error('overflow');
            }

            delta += (m - n) * handledCPCountPlusOne;
            n = m;

            for (j = 0; j < inputLength; ++j) {
                currentValue = input[j];

                if (currentValue < n && ++delta > maxInt) {
                    error('overflow');
                }

                if (currentValue == n) {
                    // Represent delta as a generalized variable-length integer
                    for (q = delta, k = base; /* no condition */; k += base) {
                        t = k <= bias ? tMin : (k >= bias + tMax ? tMax : k - bias);
                        if (q < t) {
                            break;
                        }
                        qMinusT = q - t;
                        baseMinusT = base - t;
                        output.push(
                            stringFromCharCode(digitToBasic(t + qMinusT % baseMinusT, 0))
                        );
                        q = floor(qMinusT / baseMinusT);
                    }

                    output.push(stringFromCharCode(digitToBasic(q, 0)));
                    bias = adapt(delta, handledCPCountPlusOne, handledCPCount == basicLength);
                    delta = 0;
                    ++handledCPCount;
                }
            }

            ++delta;
            ++n;

        }
        return output.join('');
    }

    /**
     * Converts a Punycode string representing a domain name or an email address
     * to Unicode. Only the Punycoded parts of the input will be converted, i.e.
     * it doesn't matter if you call it on a string that has already been
     * converted to Unicode.
     * @memberOf punycode
     * @param {String} input The Punycoded domain name or email address to
     * convert to Unicode.
     * @returns {String} The Unicode representation of the given Punycode
     * string.
     */
    function toUnicode(input) {
        return mapDomain(input, function(string) {
            return regexPunycode.test(string)
                ? decode(string.slice(4).toLowerCase())
                : string;
        });
    }

    /**
     * Converts a Unicode string representing a domain name or an email address to
     * Punycode. Only the non-ASCII parts of the domain name will be converted,
     * i.e. it doesn't matter if you call it with a domain that's already in
     * ASCII.
     * @memberOf punycode
     * @param {String} input The domain name or email address to convert, as a
     * Unicode string.
     * @returns {String} The Punycode representation of the given domain name or
     * email address.
     */
    function toASCII(input) {
        return mapDomain(input, function(string) {
            return regexNonASCII.test(string)
                ? 'xn--' + encode(string)
                : string;
        });
    }

    /*--------------------------------------------------------------------------*/

    /** Define the public API */
    punycode = {
        /**
         * A string representing the current Punycode.js version number.
         * @memberOf punycode
         * @type String
         */
        'version': '1.3.2',
        /**
         * An object of methods to convert from JavaScript's internal character
         * representation (UCS-2) to Unicode code points, and back.
         * @see <https://mathiasbynens.be/notes/javascript-encoding>
         * @memberOf punycode
         * @type Object
         */
        'ucs2': {
            'decode': ucs2decode,
            'encode': ucs2encode
        },
        'decode': decode,
        'encode': encode,
        'toASCII': toASCII,
        'toUnicode': toUnicode
    };

    compendium.punycode = punycode;

}();
// Porter stemmer in Javascript. Few comments, but it's easy to follow against the rules in the original
// paper, in
//
//  Porter, 1980, An algorithm for suffix stripping, Program, Vol. 14,
//  no. 3, pp 130-137,
//
// see also http://www.tartarus.org/~martin/PorterStemmer

// Release 1 be 'andargor', Jul 2004
// Release 2 (substantially revised) by Christopher McKenzie, Aug 2009
// Release 3 as part as Compendium (again revised, mainly readability & perf) by 'Ulflander', April 2015

!function(){
    var step2list = {
            'ational' : 'ate',
            'tional' : 'tion',
            'enci' : 'ence',
            'anci' : 'ance',
            'izer' : 'ize',
            'bli' : 'ble',
            'alli' : 'al',
            'entli' : 'ent',
            'eli' : 'e',
            'ousli' : 'ous',
            'ization' : 'ize',
            'ation' : 'ate',
            'ator' : 'ate',
            'alism' : 'al',
            'iveness' : 'ive',
            'fulness' : 'ful',
            'ousness' : 'ous',
            'aliti' : 'al',
            'iviti' : 'ive',
            'biliti' : 'ble',
            'logi' : 'log'
        },

        step3list = {
            'icate' : 'ic',
            'ative' : '',
            'alize' : 'al',
            'iciti' : 'ic',
            'ical' : 'ic',
            'ful' : '',
            'ness' : ''
        },

        c = '[^aeiou]',          // consonant
        v = '[aeiouy]',          // vowel
        C = c + '[^aeiouy]*',    // consonant sequence
        V = v + '[aeiou]*',      // vowel sequence

        mgr0 = '^(' + C + ')?' + V + C,               // [C]VC... is m>0
        meq1 = '^(' + C + ')?' + V + C + '(' + V + ')?$',  // [C]VC[V] is m=1
        mgr1 = '^(' + C + ')?' + V + C + V + C,       // [C]VCVC... is m>1
        s_v = '^(' + C + ')?' + v;                   // vowel in stem

    var stemmer = function (w) {
        var     stem,
            suffix,
            firstch,
            fp,
            re,
            re2,
            re3,
            re4,
            origword = w;

        if (w.length < 3) {
            return w;
        }

        firstch = w.substr(0,1);
        if (firstch == 'y') {
            w = firstch.toUpperCase() + w.substr(1);
        }

        // Step 1a
        re = /^(.+?)(ss|i)es$/;
        re2 = /^(.+?)([^s])s$/;

        if (re.test(w)) {
            w = w.replace(re,'$1$2');
        } else if (re2.test(w)) {
            w = w.replace(re2,'$1$2');
        }

        // Step 1b
        re = /^(.+?)eed$/;
        re2 = /^(.+?)(ed|ing)$/;
        if (re.test(w)) {
            fp = re.exec(w);
            re = new RegExp(mgr0);
            if (re.test(fp[1])) {
                re = /.$/;
                w = w.replace(re,'');
            }
        } else if (re2.test(w)) {
            fp = re2.exec(w);
            stem = fp[1];
            re2 = new RegExp(s_v);
            if (re2.test(stem)) {
                w = stem;
                re2 = /(at|bl|iz)$/;
                re3 = new RegExp('([^aeiouylsz])\\1$');
                re4 = new RegExp('^' + C + v + '[^aeiouwxy]$');
                if (re2.test(w)) {  w = w + 'e'; }
                else if (re3.test(w)) { re = /.$/; w = w.replace(re,''); }
                else if (re4.test(w)) { w = w + 'e'; }
            }
        }

        // Step 1c
        re = /^(.+?)y$/;
        if (re.test(w)) {
            fp = re.exec(w);
            stem = fp[1];
            re = new RegExp(s_v);
            if (re.test(stem)) { w = stem + 'i'; }
        }

        // Step 2
        re = /^(.+?)(ational|tional|enci|anci|izer|bli|alli|entli|eli|ousli|ization|ation|ator|alism|iveness|fulness|ousness|aliti|iviti|biliti|logi)$/;
        if (re.test(w)) {
            fp = re.exec(w);
            stem = fp[1];
            suffix = fp[2];
            re = new RegExp(mgr0);
            if (re.test(stem)) {
                w = stem + step2list[suffix];
            }
        }

        // Step 3
        re = /^(.+?)(icate|ative|alize|iciti|ical|ful|ness)$/;
        if (re.test(w)) {
            fp = re.exec(w);
            stem = fp[1];
            suffix = fp[2];
            re = new RegExp(mgr0);
            if (re.test(stem)) {
                w = stem + step3list[suffix];
            }
        }

        // Step 4
        re = /^(.+?)(al|ance|ence|er|ic|able|ible|ant|ement|ment|ent|ou|ism|ate|iti|ous|ive|ize)$/;
        re2 = /^(.+?)(s|t)(ion)$/;
        if (re.test(w)) {
            fp = re.exec(w);
            stem = fp[1];
            re = new RegExp(mgr1);
            if (re.test(stem)) {
                w = stem;
            }
        } else if (re2.test(w)) {
            fp = re2.exec(w);
            stem = fp[1] + fp[2];
            re2 = new RegExp(mgr1);
            if (re2.test(stem)) {
                w = stem;
            }
        }

        // Step 5
        re = /^(.+?)e$/;
        if (re.test(w)) {
            fp = re.exec(w);
            stem = fp[1];
            re = new RegExp(mgr1);
            re2 = new RegExp(meq1);
            re3 = new RegExp('^' + C + v + '[^aeiouwxy]$');
            if (re.test(stem) || (re2.test(stem) && !(re3.test(stem)))) {
                w = stem;
            }
        }

        re = /ll$/;
        re2 = new RegExp(mgr1);
        if (re.test(w) && re2.test(w)) {
            re = /.$/;
            w = w.replace(re,'');
        }

        // and turn initial Y back to y
        if (firstch == 'y') {
            w = firstch.toLowerCase() + w.substr(1);
        }

        return w;
    };

    compendium.stemmer = stemmer;
}();
!function(){
    var Trie = function() {
        this.t_ = {};
    };
    Trie.prototype.add = function(token, meta) {
        throw new Error('Not implmented');
    };
    Trie.prototype.isset = function(token) {
        return this.get(token) !== null;
    };
    Trie.prototype.get = function(token) {
        throw new Error('Not implmented');
    };
}();
// Parses the lexicon and augment it using compendium
// Also prepare various fields of compendium: parse brill's rules, suffixes...
!function() {
    var inflector = compendium.inflector,

        // Parses a Next lexicon
        parse = function(lexicon) {
            var d = Date.now(),
                arr = lexicon.split('\t'),
                i, j,
                l,
                lastIndex,
                blocked,
                tmp,
                pt, // PoS tag
                s, // score
                c, // score pos condition
                m,
                // Lexicon
                result = {},

                // We also keep track of emoticons
                emots = [],
                item;

            // Parses lexicon
            for (i = 0, l = arr.length; i < l; i ++) {
                item = arr[i].split(' ');
                blocked = false;

                m = item.length - 1;
                pt = m > 0 ? item[1].trim() : '';

                lastIndex = pt.length - 1;
                if (lastIndex > 0 && pt[lastIndex] === '-') {
                    blocked = true;
                    pt = pt.slice(0, lastIndex);
                }

                s = 0;
                c = null;

                // Sentiment score with PoS tag condition
                if (item[m].match(/^[A-Z]{2,}\/[0-9\-]+$/g)) {
                    c = item[m].split('/')[0];
                    s = item[m].split('/')[1];
                // Simple score
                } else if (item[m].match(/^[0-9\-]+$/g) || item[m].match(/^\-{0,1}[0-4]\.[0-9]$/g)) {
                    s = item[m].indexOf('.') > 0 ? parseFloat(item[m]) : parseInt(item[m], 10);
                }
                if (pt === 'EM' && compendium.punycode.ucs2.decode(item[0]).length > 1) {
                    emots.push(item[0]);
                }

                result[item[0]] = {
                    pos: pt === '-' ? 'NN' : pt,
                    sentiment: s,
                    condition: c,
                    blocked: blocked
                };
            }

            // Loop over compendium content
            // and augment lexicon when possible:
            // works only with objects whose keys are the words
            // and values are either a part of speech tag
            // or an integer (POS tag `CD`)
            for (i in cpd) {
                if (cpd.hasOwnProperty(i) && typeof cpd[i] === 'object' && !iA(cpd[i])) {
                    item = cpd[i];
                    for (l in item) {
                        if (item.hasOwnProperty(l)) {
                            s = 0;
                            // {'word': 'PoS_TAG'}
                            if (typeof item[l] === 'string') {
                                if (result.hasOwnProperty(l)) {
                                    s = result[l].sentiment;
                                }
                                result[l] = {
                                    pos: item[l],
                                    sentiment: s,
                                    condition: null
                                };
                            // {'word': 0}
                            } else if (typeof item[l] === 'number') {
                                result[l] = {
                                    pos: 'CD',
                                    sentiment: s,
                                    value: item[l],
                                    condition: null
                                };
                            }
                        }
                    }
                }
            }


            // Prepopulate lexicon with conjugation of regular verbs
            // Reapply sentiment if base form has a score
            for (i = 0, l = cpd.verbs.length; i < l; i ++, s = 0) {
                item = cpd.verbs[i];
                cpd.infinitives.push(item);

                tmp = inflector.conjugate(item, 'VBZ');
                if (!tmp) {
                    continue;
                }
                if (result.hasOwnProperty(item)) {
                    if (result[item].pos === 'NN') {
                        result[item].pos = 'VB';
                    }
                    blocked = result[item].blocked;
                    s = result[item].sentiment;
                }

                result[tmp] = {
                    pos: 'VBZ',
                    sentiment: s,
                    condition: null,
                    infinitive: item,
                    blocked: blocked
                };

                tmp = inflector.conjugate(item, 'VBN');
                if (!result.hasOwnProperty(tmp)) {
                    result[tmp] = {
                        pos: 'VBN',
                        sentiment: s,
                        condition: null,
                        infinitive: item
                    };
                } else {
                    result[tmp].infinitive = item;
                }

                tmp = inflector.conjugate(item, 'VBG');
                if (!result.hasOwnProperty(tmp)) {
                    result[tmp] = {
                        pos: 'VBG',
                        sentiment: s,
                        condition: null,
                        infinitive: item
                    };
                } else {
                    result[tmp].infinitive = item;
                }
            }

            // Prepopulate lexion with irregular verbs
            for (i = 0, l = cpd.irregular.length; i < l; i ++, s = 0) {
                item = cpd.irregular[i];
                m = item[0];
                if (result.hasOwnProperty(m)) {
                    s = result[m].sentiment;
                    if (result[m].pos !== 'VB') {
                        result[m].pos = 'VB';
                    }
                }
                cpd.infinitives.push(m);

                for (j = 0; j < 5; j ++) {
                    item[j].split('/').map(function(o) {
                        if (!result.hasOwnProperty(o)) {
                            result[o] = {
                                pos: j === 0 ? 'VB' : j === 1 ? 'VBD' : j === 2 ? 'VBN' : j === 3 ? 'VBZ' : 'VBG',
                                sentiment: s,
                                condition: null,
                                infinitive: m
                            }
                        } else if (!result[o].infinitive) {
                            result[o].infinitive = m;
                            result[o].sentiment = s;
                        }
                    });
                }
            }


            // Register emoticons in compendium for further use by lexer
            cpd.emots = emots;

            return result;
        },

        // Parses Brill's condition
        brills = function(raw) {
            raw = raw.split('\t');
            var line,
                result = [],
                secondRun,
                i,
                l = raw.length;

            for (i = 0; i < l; i ++) {
                line = raw[i].split(' ');
                if (line[line.length - 1] === '+') {
                    line.splice(line.length - 1, 1);
                    secondRun = true;
                } else {
                    secondRun = false;
                }
                result.push({
                    from: line[0],
                    to: line[1],
                    type: parseInt(line[2], 10),
                    c1: line[3],
                    c2: line[4],
                    c3: line[5],
                    secondRun: secondRun
                });
            }
            cpd.rules = result;
        },

        // Parse compendium suffixes
        suffixes = function(raw) {
            raw = raw.split('\t');
            var i, l = raw.length, result = [], line;

            for (i = 0; i < l; i ++) {
                line = raw[i].split(' ');
                result.push({
                    regexp: new RegExp('^.{1,}' + line[0].trim() + '$', 'gi'),
                    pos: line[1]
                });
            }
            cpd.suffixes = result;
        },

        // Pluralized dirty words
        dirty = function(raw) {
            var i, l = raw.length;

            for (i = 0; i < l; i ++) {
                raw.push(inflector.pluralize(raw[i]));
            }
        },

        // Take care of abbreviations
        abbrs = function(arr) {
            var i, l = arr.length, res = [], rplt = [];
            for (i = 0; i < l; i ++) {
                if (i % 2 === 0) {
                    res.push(arr[i]);
                } else {
                    rplt.push(arr[i]);
                }
            }
            cpd.abbrs = res;
            cpd.abbrs_rplt = rplt;
        },

        nationalities = function(raw) {
            var i, l, res = {};
            raw = raw.split(' ');
            for (i = 0, l = raw.length; i < l; i ++) {
                res[raw[i]] = 'JJ';
            }
            cpd.nationalities = res;
        },

        synonyms = function(raw) {
            raw = raw.split('\t');
            var i, l = raw.length, result = [];
            for (i = 0; i < l; i ++) {
                result.push(raw[i].split(' '));
            }
            cpd.synonyms = result;
        };

    brills(cpd.rules);
    suffixes(cpd.suffixes);
    abbrs(cpd.abbrs);
    dirty(cpd.dirty);
    synonyms(cpd.synonyms);
    nationalities(cpd.nationalities);
    compendium.lexicon = parse("abandon VB -2	abandoned VBN -2	abandons VBZ -2	abducted VBN -2	abduction NN -2	abhor VB -3	abhorred VBD -3	abhorrent JJ -3	ability NN 2	aboard IN 1	absentee JJ -1	absolve VBP 2	absolved VBD 2	absolving VBG 2	absorbed VBN 1	abuse NN -3	abused VBN -3	abusive JJ -3	accept VB 1	accepted VBN 1	accepting VBG 1	accepts VBZ 1	accident NN -2	accidental JJ -2	accidentally RB -2	accomplish VB 2	accomplished VBN 2	accomplishes VBZ 2	accurate JJ 1	accusation NN -2	accuse VB -2	accused VBN -2	accuses VBZ -2	accusing VBG -2	ache NN -2	achievable JJ 1	aching VBG -2	acquit VB 2	acquitted VBN 2	acrimonious JJ -3	active JJ 1	adequate JJ 1	admire VB 3	admired VBD 3	admires VBZ 3	admiring VBG 3	admit VB -1	admits VBZ -1	admitted VBD -1	admonished VBD -2	adopt VB 1	adopts VBZ 1	adorable JJ 3	adore VBP 3	adored VBD 3	adores VBZ 3	advanced VBD 1	advantage NN 2	adventure NN 2	adventurous JJ 2	affected VBN -1	affection NN 3	affectionate JJ 3	afflicted VBN -1	affronted VBN -1	afraid JJ -2	aggravate VBP -2	aggravated VBN -2	aggravates VBZ -2	aggravating VBG -2	aggression NN -2	aggressive JJ -2	aghast JJ -2	agonize VB -3	agonized VBD -3	agonizes VBZ -3	agonizing JJ -3	agree VB 1	agreeable JJ 2	agreed VBD 1	agreement NN 1	agrees VBZ 1	alarm NN -2	alarmed VBN -2	alarmist JJ -2	alas UH -1	alert JJ -1	alienation NN -2	alive JJ 1	allergic JJ -2	allow VB 1	alone RB -2	amaze VB 2	amazed VBN 2	amazing JJ 4	ambitious JJ 2	ambivalent JJ -1	amuse VB 3	amused VBN 3	amusement NN 3	anger NN -3	angers VBZ -3	angry JJ -3	anguish NN -3	anguished JJ -3	animosity NN -2	annoy VB -2	annoyance NN -2	annoyed VBN -2	annoying JJ -2	annoys VBZ -2	antagonistic JJ -2	anti IN -1	anticipation NN 1	anxiety NN -2	anxious JJ -2	apathetic JJ -3	apathy NN -3	apocalyptic JJ -2	apologize VB -1	apologized VBD -1	apologizes VBZ -1	apologizing VBG -1	apology NN -1	appalled VBN -2	appalling JJ -2	appease VB 2	appeased VBN 2	appeasing NN 2	applaud VBP 2	applauded VBD 2	applauding VBG 2	applauds VBZ 2	applause NN 2	appreciate VB 2	appreciated VBN 2	appreciates VBZ 2	appreciating VBG 2	appreciation NN 2	apprehensive JJ -2	approval NN 2	approved VBD 2	approves VBZ 2	ardent JJ 1	arrest NN -2	arrested VBN -3	arrogant JJ -2	ashamed JJ -2	ass NN -4	assassination NN -3	asset NN 2	astonished VBN 2	astound VB 3	astounded VBN 3	astounding JJ 3	astoundingly RB 3	astounds VBZ 3	attack NN -1	attacked VBN -1	attacking VBG -1	attract VB 1	attracted VBN 1	attracting VBG 2	attraction NN 2	attracts VBZ 1	audacious JJ 3	authority NN 1	avert VB -1	averted VBN -1	averts VBZ -1	avid JJ 2	avoid VB -1	avoided VBN -1	avoids VBZ -1	aww UH 1	await VB -1	awaited VBD -1	awaits VBZ -1	award NN 3	awarded VBN 3	awesome JJ 4	awful JJ -3	awkward JJ -2	axe NN -1	backed VBN 1	backing VBG 2	bad JJ -3	badly RB -3	bailout NN -2	bamboozled VBN -2	ban NN -2	banish VB -1	bankrupt JJ -3	banned VBN -2	bargain NN 2	barrier NN -2	bastard NN -5	battle NN -1	beaten VBN -2	beatific JJ 3	beating VBG -1	beautiful JJ 3	beautifully RB 3	beautify VBP 3	belittle VBP -2	belittled JJ -2	beloved JJ 3	benefit NN 2	best JJS 3	betray VB -3	betrayal NN -3	betrayed VBN -3	betraying VBG -3	betrays VBZ -3	better JJR 2	bias NN -1	biased VBN -2	big JJ 1	bitch NN -5	bitter JJ -2	bitterly RB -2	bizarre JJ -2	blame VB -2	blamed VBD -2	blames VBZ -2	blaming VBG -2	bless VB 2	blessing NN 3	blind JJ -1	bliss NN 3	blissful JJ 3	blithe JJ 2	block NN -1	blockbuster NN 3	blocked VBN -1	blocking VBG -1	bloody JJ -3	blurry JJ -2	blush NN 3	boastful JJ -2	bold JJ 2	boldly RB 2	bomb NN -1	boost VB 1	boosted VBD 1	boosting VBG 1	bore VBD -2	bored VBN -2	boring JJ -3	bother VB -2	bothered VBN -2	bothers VBZ -2	bothersome JJ -2	boycott NN -2	boycotted VBN -2	boycotting VBG -2	brainwashing NN -3	brave JJ 2	breakthrough NN 3	breathtaking JJ 5	bribe NN -3	bright JJ 1	brightest JJS 2	brightness NN 1	brilliant JJ 4	brisk JJ 2	broke VBD -1	broken VBN -1	brooding VBG -2	bullied VBD -2	bullshit NN -4	bully NN -2	bullying VBG -2	buoyant JJ 2	burden NN -2	burdened VBN -2	burdening VBG -2	burn VB -1	calm JJ 2	calmed VBD 2	calming VBG 2	cancel VB -1	cancelled VBN -1	cancelling VBG -1	cancels VBZ -1	cancer NN -1	capable JJ 1	captivated VBN 3	care NN 2	carefree JJ 1	careful JJ 2	carefully RB 2	careless JJ -2	cares VBZ 2	casualty NN -2	catastrophe NN -3	catastrophic JJ -4	cautious JJ -1	celebrate VB 3	celebrated VBD 3	celebrates VBZ 3	celebrating VBG 3	censor VBP -2	censored VBN -2	certain JJ 1	chagrin NN -2	challenge NN -1	chance NN 2	chaos NN -2	chaotic JJ -2	charged VBN -3	charm NN 3	charming JJ 3	chastised VBD -3	chastises VBZ -3	cheat VB -3	cheated VBN -3	cheater NN -3	cheats VBZ -3	cheer NN 2	cheered VBD 2	cheerful JJ 2	cheering VBG 2	cheery JJ 3	cherish VB 2	cherished VBN 2	cherishes VBZ 2	cherishing VBG 2	chic JJ 2	childish JJ -2	chilling VBG -1	choke VB -2	choked VBD -2	choking VBG -2	clarifies VBZ 2	clarity NN 2	clash NN -2	classy JJ 3	clean JJ 2	cleaner JJR 2	clear JJ 1	cleared VBN 1	clearly RB 1	clears VBZ 1	clever JJ 2	clouded VBN -1	cock NN -5	cocky JJ -2	coerced VBN -2	collapse NN -2	collapsed VBD -2	collapses VBZ -2	collapsing VBG -2	collision NN -2	combat NN -1	comedy NN 1	comfort NN 2	comfortable JJ 2	comforting VBG 2	commend VB 2	commended VBN 2	commit VB 1	commitment NN 2	commits VBZ 1	committed VBN 1	committing VBG 1	compassionate JJ 2	compelled VBN 1	competent JJ 2	competitive JJ 2	complacent JJ -2	complain VBP -2	complained VBD -2	complains VBZ -2	comprehensive JJ 2	conciliate VB 2	condemn VB -2	condemnation NN -2	condemned VBN -2	condemns VBZ -2	confidence NN 2	confident JJ 2	conflict NN -2	conflicting VBG -2	confounded VBD -2	confuse VB -2	confused VBN -2	confusing JJ -2	congratulate VBP 2	congratulation NN 2	consent NN 2	conspiracy NN -3	constrained VBN -2	contagion NN -2	contagious JJ -1	contempt NN -2	contemptuous JJ -2	contemptuously RB -2	contend VBP -1	contender NN -1	contending VBG -1	contentious JJ -2	controversial JJ -2	convince VB 1	convinced VBN 1	convinces VBZ 1	convivial JJ 2	cool JJ 1	cornered VBN -2	corpse NN -1	costly JJ -2	courage NN 2	courageous JJ 2	courteous JJ 2	courtesy NN 2	cover-up NN -3	coward NN -2	cowardly JJ -2	coziness NN 2	cramp NN -1	crap NN -3	crash NN -2	crazy JJ -2	creative JJ 2	crestfallen JJ -2	cried VBD -2	crime NN -3	criminal JJ -3	crisis NN -3	critic NN -2	criticism NN -2	criticize VB -2	criticized VBN -2	criticizes VBZ -2	criticizing VBG -2	cruel JJ -3	cruelty NN -3	crush NN -1	crushed VBN -2	crushes VBZ -1	crushing VBG -1	cry NN -1	crying VBG -2	curious JJ 1	curse NN -1	cut VB -1	cute JJ 2	cutting VBG -1	cynic NN -2	cynical JJ -2	cynicism NN -2	damage NN -3	damn JJ -4	damned JJ -4	damnit UH -4	danger NN -2	daring JJ 2	darkest JJS -2	darkness NN -1	dauntless JJ 2	dead JJ -3	deadlock NN -2	deafening VBG -1	dear JJ 2	dearly RB 3	death NN -2	debonair JJ 2	debt NN -2	deceit NN -3	deceitful JJ -3	deceive VB -3	deceived VBN -3	deceives VBZ -3	deceiving VBG -3	deception NN -3	decisive JJ 1	dedicated VBN 2	defeated VBN -2	defect NN -3	defender NN 2	defenseless JJ -2	defer VB -1	deferring VBG -1	defiant JJ -1	deficit NN -2	degrade VB -2	degraded JJ -2	dehumanize VB -2	dehumanized VBN -2	delay NN -1	delayed VBN -1	delight NN 3	delighted VBN 3	delighting VBG 3	delights VBZ 3	demand NN -1	demanded VBD -1	demanding VBG -1	demonstration NN -1	demoralized VBN -2	denied VBN -2	denies VBZ -2	denounce VBP -2	denounces VBZ -2	deny VB -2	denying VBG -2	depressed JJ -2	depressing JJ -2	derail VB -2	derailed VBD -2	deride VBP -2	derided VBD -2	derision NN -2	desirable JJ 2	desire NN 1	desired VBN 2	desirous JJ 2	despair NN -3	despairing JJ -3	despairs VBZ -3	desperate JJ -3	desperately RB -3	despondent JJ -3	destroy VB -3	destroyed VBN -3	destroying VBG -3	destroys VBZ -3	destruction NN -3	destructive JJ -3	detached VBN -1	detain VB -2	detained VBN -2	detention NN -2	determined VBN 2	devastate VB -2	devastated VBN -2	devastating JJ -2	devoted VBN 3	diamond NN 1	die VB -3	died VBD -3	difficult JJ -1	dilemma NN -1	dire JJ -3	dirt NN -2	dirtier JJR -2	dirtiest JJS -2	dirty JJ -2	disabling VBG -1	disadvantage NN -2	disadvantaged JJ -2	disappear VB -1	disappeared VBD -1	disappears VBZ -1	disappoint VB -2	disappointed VBN -2	disappointing JJ -2	disappointment NN -2	disappoints VBZ -2	disaster NN -2	disastrous JJ -3	disbelieve VB -2	discard VB -1	discarded VBN -1	discontented JJ -2	discord NN -2	discounted VBN -1	discouraged VBN -2	discredited VBN -2	disdain NN -2	disgrace NN -2	disgraced VBN -2	disguise VB -1	disguised VBN -1	disguises VBZ -1	disgust NN -3	disgusted VBN -3	disgusting JJ -3	disheartened VBN -2	dishonest JJ -2	disillusioned VBN -2	disinclined VBN -2	disjointed VBN -2	dislike NN -2	dismal JJ -2	dismayed VBN -2	disorder NN -2	disorganized JJ -2	disoriented VBN -2	disparage VB -2	disparaged VBD -2	disparaging VBG -2	displeased VBN -2	dispute NN -2	disputed VBN -2	disqualified VBN -2	disquiet NN -2	disregard NN -2	disregarded VBD -2	disregarding VBG -2	disrespect NN -2	disruption NN -2	disruptive JJ -2	dissatisfied JJ -2	distort VB -2	distorted VBN -2	distorting VBG -2	distorts VBZ -2	distract VB -2	distracted VBN -2	distraction NN -2	distress NN -2	distressed JJ -2	distressing JJ -2	distrust NN -3	disturb VB -2	disturbed VBN -2	disturbing JJ -2	disturbs VBZ -2	dithering VBG -2	dizzy JJ -1	dodging VBG -2	doom NN -2	doomed VBN -2	doubt NN -1	doubted VBD -1	doubtful JJ -1	doubting VBG -1	downcast JJ -2	downside NN -2	drag NN -1	dragged VBN -1	drags VBZ -1	drained VBN -2	dread NN -2	dreaded VBN -2	dreadful JJ -3	dreading VBG -2	dream NN 1	dreary JJ -2	drop NN -1	drown VB -2	drowned VBN -2	drowns VBZ -2	drunk JJ -2	dubious JJ -2	dud NN -2	dull JJ -2	dumb JJ -3	dump VB -1	dumped VBD -2	dumps VBZ -1	duped VBN -2	dysfunction NN -2	eager JJ 2	earn VB 1	earnest NN 2	ease VB 2	easily RB 1	easy JJ 1	ecstatic JJ 4	eerie JJ -2	effective JJ 2	effectively RB 2	elated JJ 3	elation NN 3	elegant JJ 2	elegantly RB 2	embarrass VB -2	embarrassed VBN -2	embarrassing JJ -2	embarrassment NN -2	embittered VBN -2	embrace VB 1	emergency NN -2	empathetic JJ 2	emptiness NN -1	empty JJ -1	enchanted VBN 2	encourage VB 2	encouraged VBN 2	encouragement NN 2	encourages VBZ 2	endorse VB 2	endorsed VBN 2	endorsement NN 2	endorses VBZ 2	enemy NN -2	energetic JJ 2	engage VB 1	engrossed JJ 1	enjoy VB 2	enjoying VBG 2	enjoys VBZ 2	enlighten VB 2	enlightened JJ 2	enlightening VBG 2	ennui NN -2	enrage NN -2	enraged JJ -2	enslave VBP -2	enslaved VBN -2	ensure VB 1	ensuring VBG 1	enterprising JJ 1	entertaining VBG 2	enthusiastic JJ 3	entitled VBN 1	entrusted VBN 2	envious JJ -2	envy NN -1	erroneous JJ -2	error NN -2	escape VB -1	escaping VBG -1	esteemed VBD 2	ethical JJ 2	euphoria NN 3	euphoric JJ 4	evil JJ -3	exaggerate VB -2	exaggerated VBN -2	exaggerating VBG -2	exasperated JJ 2	excellence NN 3	excellent JJ 3	excite VB 3	excited VBN 3	excitement NN 3	exciting JJ 3	exclude VB -1	excluded VBN -2	exclusion NN -1	exclusive JJ 2	excuse NN -1	exempt JJ -1	exhausted VBN -2	exhilarated VBN 3	exhilarating JJ 3	exonerate VB 2	exonerated VBN 2	exonerating VBG 2	expand VB 1	expands VBZ 1	expel VB -2	expelled VBN -2	expelling VBG -2	exploit VB -2	exploited VBN -2	exploiting VBG -2	exploration NN 1	expose VB -1	exposed VBN -1	exposes VBZ -1	exposing VBG -1	expressionless JJ 0	extend VB 1	extends VBZ 1	exuberant JJ 4	exultantly RB 3	fabulous JJ 4	fad NN -2	fail VB -2	failed VBD -2	failing VBG -2	fails VBZ -2	failure NN -2	fair JJ 2	faith NN 1	faithful JJ 3	fake JJ -3	faking VBG -3	fallen VBN -2	falling VBG -1	falsified VBN -3	falsify VB -3	fame NN 1	fan NN 3	fantastic JJ 4	farce NN -1	fascinate VB 3	fascinated VBN 3	fascinates VBZ 3	fascinating JJ 3	fascist JJ -2	fatality NN -3	fatigue NN -2	fatigued VBN -2	favor NN 2	favored VBN 2	favorite JJ 2	favors VBZ 2	fear NN -2	fearful JJ -2	fearing VBG -2	fearless JJ 2	fearsome JJ -2	feeble JJ -2	felony NN -3	fervent JJ 2	fervid NN 2	festive JJ 2	fiasco NN -3	fight NN -1	fine JJ 2	fire NN -2	fired VBN -2	firing VBG -2	fit VB 1	fitness NN 1	flagship NN 2	flees VBZ -1	flop NN -2	flops VBZ -2	flu NN -2	flushed VBN -2	flustered VBN -2	focused VBN 2	fond JJ 2	fondness NN 2	fool NN -2	foolish JJ -2	forced VBN -1	foreclosure NN -2	forget VB -1	forgetful JJ -2	forgive VB 1	forgiving VBG 1	forgotten VBN -1	fortunate JJ 2	frantic JJ -1	fraud NN -4	fraudulent JJ -4	free JJ 1	freedom NN 2	frenzy NN -3	fresh JJ 1	friendly JJ 2	fright NN -2	frightened VBN -2	frightening JJ -3	frisky JJ 2	frowning VBG -1	frustrate VB -2	frustrated VBN -2	frustrates VBZ -2	frustrating JJ -2	frustration NN -2	fuck VB -2	fulfill VB 2	fulfilled VBN 2	fulfills VBZ 2	fuming VBG -2	fun NN 4	funeral NN -1	funky JJ 2	funnier JJR 4	funny JJ 4	furious JJ -3	futile JJ 2	gag NN -2	gagged VBN -2	gain NN 2	gained VBD 2	gaining VBG 2	gallant JJ 3	gallantry NN 3	generous JJ 2	genial JJ 3	ghost NN -1	giddy JJ -2	gift NN 2	glad JJ 3	glamorous JJ 3	glee NN 3	gleeful JJ 3	gloom NN -1	gloomy JJ -2	glorious JJ 2	glory NN 2	glum JJ -2	god NN 1	goddamn UH -3	godsend NN 4	good JJ 3	goodness NN 3	grace NN 1	gracious JJ 3	grand JJ 3	grant NN 1	granted VBN 1	granting VBG 1	grateful JJ 3	gratification NN 2	grave JJ -2	gray JJ -1	great JJ 3	greater JJR 3	greatest JJS 3	greed NN -3	greedy JJ -2	greet VB 1	greeted VBD 1	greeting NN 1	greets VBZ 1	grey JJ -1	grief NN -2	grieved VBN -2	grin NN -1	grinning VBG 3	gross JJ -2	growing VBG 1	growth NN 2	guarantee NN 1	guilt NN -3	guilty JJ -3	gullibility NN -2	gullible JJ -2	gun NN -1	ha UH 2	hacked VBD -1	hail NN 2	hailed VBD 2	hapless JJ -2	happiness NN 3	happy JJ 3	hard JJ -1	hardier JJR 2	hardship NN -2	hardy JJ 2	harm NN -2	harmed VBN -2	harmful JJ -2	harming VBG -2	harms VBZ -2	harried VBN -2	harsh JJ -2	harsher JJR -2	harshest JJS -2	hate VBP -3	hated VBD -3	hates VBZ -3	hating VBG -3	haunt VB -1	haunted VBN -2	haunting JJ 1	havoc NN -2	healthy JJ 2	heartbreaking JJ -3	heartfelt JJ 3	heaven NN 2	heavenly JJ 4	hell NN -4	help VB 0	helpful JJ 2	helping VBG 2	helpless JJ -2	helps VBZ 2	hero NN 2	heroic JJ 3	hesitant JJ -2	hesitate VB -2	hid VBD -1	hide VB -1	hiding VBG -1	highlight VB 2	hilarious JJ 2	hindrance NN -2	homesick JJ -2	honest JJ 2	honor NN 2	honored VBN 2	honoring VBG 2	honour NN 2	honoured VBN 2	hooliganism NN -2	hope NN 2	hopeful JJ 2	hopefully RB 2	hopeless JJ -2	hopelessness NN -2	hopes VBZ 2	hoping VBG 2	horrendous JJ -3	horrible JJ -3	horrific JJ -3	horrified VBN -3	hostile JJ -2	huckster NN -2	hug NN 2	huge JJ 1	humiliated VBN -3	humiliation NN -3	humor NN 2	humorous JJ 2	humour NN 2	hunger NN -2	hurrah NN 5	hurt VBN -2	hurting VBG -2	hurts VBZ -2	hushed JJ -1	hypocritical JJ -2	hysteria NN -3	hysterical JJ -3	idiot JJ -3	idiotic JJ -3	ignorance NN -2	ignorant JJ -2	ignore VB -1	ignored VBN -2	ignores VBZ -1	ill JJ -2	illegal JJ -3	illiteracy NN -2	illness NN -2	imbecile NN -3	immobilized VBN -1	immortal JJ 2	immune JJ 1	impatient JJ -2	imperfect JJ -2	importance NN 2	important JJ 2	impose VB -1	imposed VBN -1	imposes VBZ -1	imposing VBG -1	impotent JJ -2	impress VB 3	impressed VBN 3	impresses VBZ 3	impressive JJ 3	imprisoned VBN -2	improve VB 2	improved VBN 2	improvement NN 2	improves VBZ 2	improving VBG 2	inability NN -2	inaction NN -2	inadequate JJ -2	incapable JJ -2	incapacitated VBN -2	incensed VBN -2	incompetence NN -2	incompetent JJ -2	inconvenience NN -2	inconvenient JJ -2	increase NN 1	increased VBN 1	indecisive JJ -2	indestructible JJ 2	indifference NN -2	indifferent JJ -2	indignant JJ -2	indignation NN -2	indoctrinated VBN -2	indoctrinating NN -2	ineffective JJ -2	ineffectively RB -2	infatuation NN 2	infected VBN -2	inferior JJ -2	inflamed JJ -2	influential JJ 2	infringement NN -2	infuriate VB -2	infuriated VBD -2	infuriating JJ -2	inhibit VB -1	injured VBN -2	injury NN -2	injustice NN -2	innocent JJ 4	innovate VB 1	innovation NN 1	innovative JJ 2	inquisitive JJ 2	insane JJ -2	insanity NN -2	insecure JJ -2	insensitive JJ -2	insensitivity NN -2	insignificant JJ -2	insipid JJ -2	inspiration NN 2	inspirational JJ 2	inspire VB 2	inspired VBN 2	inspires VBZ 2	inspiring JJ 3	insult NN -2	insulted VBN -2	insulting JJ -2	intact JJ 2	integrity NN 2	intelligent JJ 2	intense JJ 1	interest NN 1	interested JJ 2	interesting JJ 2	interrogated VBN -2	interrupt VB -2	interrupted VBN -2	interrupting VBG -2	interruption NN -2	interrupts VBZ -2	intimidate VB -2	intimidated VBN -2	intimidates VBZ -2	intimidating VBG -2	intimidation NN -2	intricate JJ 2	invincible JJ 2	invite VB 1	inviting VBG 1	invulnerable JJ 2	irate JJ -3	ironic JJ -1	irony NN -1	irrational JJ -1	irresistible JJ 2	irresolute JJ -2	irresponsible JJ 2	irreversible JJ -1	irritate VB -3	irritated VBN -3	irritating JJ -3	isolated VBN -1	itchy JJ -2	jackass NN -4	jailed VBN -2	jaunty JJ 2	jealous JJ -2	jeopardy NN -2	jerk NN -3	jewel NN 1	jocular JJ 2	join VB 1	joke NN 2	jolly JJ 2	jovial JJ 2	joy NN 3	joyful JJ 3	joyfully RB 3	joyless JJ -2	joyous JJ 3	jubilant JJ 3	jumpy JJ -1	justice NN 2	justifiably RB 2	justified VBN 2	keen JJ 1	kill VB -3	killed VBN -3	killing VBG -3	kills VBZ -3	kind NN JJ/2	kinder JJR 2	kiss NN 2	kissing VBG 3	lack NN -2	lackadaisical JJ -2	lag VB -1	lagged VBN -2	lagging VBG -2	lags VBZ -2	lame JJ -2	landmark NN 2	laugh NN 1	laughed VBD 1	laughing VBG 1	laughs VBZ 1	launched VBN 1	lawsuit NN -2	lazy JJ -1	leak NN -1	leaked VBN -1	leave VB -1	legal JJ 1	legally RB 1	lenient JJ 1	lethargic JJ -2	lethargy NN -2	liar NN -3	libelous JJ -2	lied VBD -2	lighthearted JJ 1	like VBP 2	liked VBD 2	likes VBZ 2	limitation NN -1	limited JJ -1	litigation NN -1	litigious JJ -2	lively JJ 2	livid JJ -2	loathed VBD -3	loathes VBZ -3	loathing NN -3	lobbying VBG -2	lonely JJ -2	lonesome JJ -2	longing NN -1	loom VBP -1	loomed VBD -1	looming VBG -1	looms VBZ -1	loose JJ -3	loser NN -3	losing VBG -3	loss NN -3	lost VBD -3	lovable JJ 3	love NN 3	loved VBD 3	lovely JJ 3	loving JJ 2	low JJ -1	lowest JJS -1	loyal JJ 3	loyalty NN 3	luck NN 3	luckily RB 3	lucky JJ 3	lunatic JJ -3	lurk VB -1	lurking VBG -1	lurks VBZ -1	mad JJ -3	maddening JJ -3	made-up JJ -1	madly RB -3	madness NN -3	mandatory JJ -1	manipulated VBN -1	manipulating VBG -1	manipulation NN -1	marvel VB 3	marvelous JJ 3	mask NN -1	masterpiece NN 4	matter NN 1	mature JJ 2	meaningful JJ 2	meaningless JJ -2	medal NN 3	mediocrity NN -3	meditative JJ 1	melancholy NN -2	menace NN -2	menaced VBN -2	mercy NN 2	merry JJ 3	mess NN -2	messed VBD -2	messing VBG -2	methodical JJ 2	mindless JJ -2	miracle NN 4	mirth NN 3	misbehaving VBG -2	mischief NN -1	miserable JJ -3	misery NN -2	misinformation NN -2	misinformed VBN -2	misinterpreted VBN -2	misleading JJ -3	misread VBD -1	misrepresentation NN -2	miss VB -2	missed VBD -2	missing VBG -2	mistake NN -2	mistaken VBN -2	mistaking VBG -2	misunderstand VB -2	misunderstanding NN -2	misunderstands VBZ -2	misunderstood VBN -2	moan VB -2	moaned VBD -2	moaning VBG -2	moans VBZ -2	mock JJ -2	mocked VBN -2	mocking VBG -2	monopolize VB -2	monopolized VBD -2	monopolizing VBG -2	moody JJ -1	motivate VB 1	motivated VBN 2	motivating VBG 2	motivation NN 1	mourn VB -2	mourned VBD -2	mournful JJ -2	mourning VBG -2	mourns VBZ -2	murder NN -2	murderer NN -2	murdering VBG -3	murderous JJ -3	myth NN -1	naive JJ -2	nasty JJ -3	natural JJ 1	needy JJ -2	negative JJ -2	neglect NN -2	neglected VBN -2	neglecting VBG -2	neglects VBZ -2	nervous JJ -2	nervously RB -2	nice JJ 3	nifty JJ 2	nigger NN -5	noble JJ 2	noisy JJ -1	nonsense NN -2	notorious JJ -2	novel NN 2	numb JJ -1	obliterate VB -2	obliterated VBN -2	obnoxious JJ -3	obscene JJ -2	obsessed VBN 2	obsolete JJ -2	obstacle NN -2	obstinate JJ -2	odd JJ -2	offend VB -2	offended VBN -2	offender NN -2	offending VBG -2	offends VBZ -2	ohhdee UH 1	oks VBZ 2	ominous JJ 3	once-in-a-lifetime JJ 3	opportunity NN 2	oppressed JJ -2	oppressive JJ -2	optimism NN 2	optimistic JJ 2	outcry NN -2	outmaneuvered VBN -2	outrage NN -3	outraged VBN -3	outreach NN 2	outstanding JJ 5	overjoyed JJ 4	overload NN -1	overlooked VBN -1	overreact VB -2	overreacted VBN -2	overreaction NN -2	oversimplification NN -2	oversimplified VBN -2	overstatement NN -2	overweight JJ -1	pain NN -2	pained JJ -2	panic NN -3	panicked VBD -3	paradise NN 3	paradox NN -1	pardon VB 2	pardoned VBN 2	parley NN -1	passionate JJ 2	passive JJ -1	passively RB -1	pathetic JJ -2	pay VB -1	peace NN 2	peaceful JJ 2	peacefully RB 2	penalty NN -2	perfect JJ 3	perfected VBN 2	perfectly RB 3	peril NN -2	perjury NN -3	perpetrator NN -2	perplexed JJ -2	persecute VBP -2	persecuted VBN -2	persecuting VBG -2	persevere VB -2	perturbed JJ -2	pessimism NN -2	pessimistic JJ -2	petrified JJ -2	picturesque JJ 2	pileup NN -1	pique JJ -2	piqued VBN -2	piss VB -4	piteous JJ -2	pitied VBD -1	pity NN -2	playful JJ 2	pleasant JJ 3	please VB 1	pleased VBN 3	pleasure NN 3	poised VBN -2	poison NN -2	poisoned VBN -2	pollute VB -2	polluted JJ -2	polluter NN -2	poor JJ -2	poorer JJR -2	poorest JJS -2	popular JJ 3	positive JJ 2	positively RB 2	possessive JJ -2	postpone VB -1	postponed VBN -1	postponing VBG -1	poverty NN -1	powerful JJ 2	powerless JJ -2	praise NN 3	praised VBD 3	praises VBZ 3	praising VBG 3	pray VB 1	praying VBG 1	prays VBZ 1	prepared VBN 1	pressure NN -1	pressured VBN -2	pretend VB -1	pretending VBG -1	pretends VBZ -1	pretty RB 1	prevent VB -1	prevented VBN -1	preventing VBG -1	prevents VBZ -1	prick NN -5	prison NN -2	prisoner NN -2	privileged JJ 2	problem NN -2	progress NN 2	prominent JJ 2	promise NN 1	promised VBD 1	promises VBZ 1	promote VB 1	promoted VBN 1	promotes VBZ 1	promoting VBG 1	propaganda NN -2	prosecute VB -1	prosecuted VBN -2	prosecution NN -1	prospect NN 1	prosperous JJ 3	protect VB 1	protected VBN 1	protects VBZ 1	protest NN -2	protesting VBG -2	proud JJ 2	proudly RB 2	provoke VB -1	provoked VBD -1	provokes VBZ -1	provoking VBG -1	punish VB -2	punished VBN -2	punishes VBZ -2	punitive JJ -2	pushy JJ -1	puzzled VBN -2	quaking VBG -2	questionable JJ -2	questioned VBD -1	questioning VBG -1	racism NN -3	racist JJ -3	rage NN -2	rainy JJ -1	rant VBP -3	rape NN -4	rapist NN -4	rapture NN 2	rash NN -2	ratified VBD 2	reach VB 1	reached VBN 1	reaches VBZ 1	reaching VBG 1	reassure VB 1	reassured VBN 1	reassuring VBG 2	rebellion NN -2	recession NN -2	reckless JJ -2	recommend VB 2	recommended VBD 2	recommends VBZ 2	redeemed VBN 2	refuse VB -2	refused VBD -2	refusing VBG -2	regret VBP -2	regrets VBZ -2	regretted VBD -2	reject VB -1	rejected VBD -1	rejecting VBG -1	rejects VBZ -1	rejoice VBP 4	rejoiced VBD 4	rejoices VBZ 4	rejoicing VBG 4	relaxed VBN 2	relentless JJ -1	reliant JJ 2	relieve VB 1	relieved VBN 2	relieves VBZ 1	relieving VBG 2	relishing VBG 2	remarkable JJ 2	remorse NN -2	repulsed VBN -2	rescue NN 2	rescued VBN 2	resentful JJ -2	resign VB -1	resigned VBD -1	resigning VBG -1	resigns VBZ -1	resolute JJ 2	resolve VB 2	resolved VBN 2	resolves VBZ 2	resolving VBG 2	respected VBN 2	responsible JJ 2	responsive JJ 2	restful JJ 2	restless JJ -2	restore VB 1	restored VBN 1	restores VBZ 1	restoring VBG 1	restrict VB -2	restricted VBN -2	restricting VBG -2	restriction NN -2	restricts VBZ -2	retained VBN -1	retard VB -2	retarded JJ -2	retreat NN -1	revenge NN -2	revered VBN 2	revive VB 2	revives VBZ 2	reward NN 2	rewarded VBN 2	rewarding JJ 2	rich JJ 2	ridiculous JJ -3	rig NN -1	rigged VBN -1	rigorous JJ 3	rigorously RB 3	riot NN -2	risk NN -2	rob VB -2	robber NN -2	robed VBN -2	robing NN -2	robs VBZ -2	robust JJ 2	romance NN 2	ruin NN -2	ruined VBN -2	ruining VBG -2	sabotage NN -2	sad JJ -2	saddened JJ -2	sadly RB -2	safe JJ 1	safely RB 1	safety NN 1	salient JJ 1	sappy JJ -1	sarcastic JJ -2	satisfied VBN 2	save VB 2	saved VBN 2	scam NN -2	scandal NN -3	scandalous JJ -3	scapegoat NN -2	scare VB -2	scared VBN -2	scary JJ -2	sceptical JJ -2	scold VB -2	scoop NN 3	scorn NN -2	scornful JJ -2	scream VB -2	screamed VBD -2	screaming VBG -2	screw NN -2	screwed VBN -2	secure VB 2	secured VBN 2	sedition NN -2	seditious JJ -2	seduced VBN -1	self-confident JJ 2	self-deluded JJ -2	selfish JJ -3	selfishness NN -3	sentence NN -2	sentenced VBN -2	sentencing NN -2	serene JJ 2	severe JJ -2	sexy JJ 3	shaky JJ -2	shame NN -2	shamed VBN -2	shameful JJ -2	share NN 1	shared VBN 1	shattered VBN -2	shit NN -4	shock NN -2	shocked VBN -2	shocking JJ -2	shoot VB -1	shortage NN -2	shy JJ -1	sick JJ -2	sigh NN -2	significance NN 1	significant JJ 1	silencing VBG -1	silly JJ -1	sincere JJ 2	sincerely RB 2	sincerest JJS 2	sincerity NN 2	sinful JJ -3	skeptic NN -2	skeptical JJ -2	skepticism NN -2	slam NN -2	slash VB -2	slashed VBD -2	slashing VBG -2	slavery NN -3	sleeping VBG 0	sleeplessness NN -2	sleepy JJ 0	slick JJ 2	slicker NN 2	sluggish JJ -2	smart JJ 1	smarter RBR 2	smartest JJS 2	smear NN -2	smile NN 2	smiled VBD 2	smiling VBG 2	smirk NN 3	smog NN -2	sneaky JJ -1	snub VB -2	sob VB -4	sobering VBG 1	solemn JJ -1	solid JJ 2	solidarity NN 2	solution NN 1	solve VB 1	solved VBN 1	solves VBZ 1	solving VBG 1	somber JJ -2	son-of-a-bitch NN -5	soothe VB 3	soothed VBD 3	soothing VBG 3	sophisticated JJ 2	sore JJ -1	sorrow NN -2	sorrowful JJ -2	sorry JJ- -1	spark VB 1	sparkle NN 3	sparkles VBZ 3	sparkling JJ 3	speculative JJ -2	spic NN -5	spirit NN 1	spirited JJ 2	spiritless JJ -2	splendid JJ 3	sprightly JJ 2	squelched VBN -1	stab NN -2	stabbed VBD -2	stable JJ 2	stall NN -2	stalled VBN -2	stalling VBG -2	stamina NN 2	stampede NN -2	startled VBN -2	starve VB -2	starved VBN -2	starving VBG -2	steadfast JJ 2	steal VB -2	steals VBZ -2	steep JJ -2	stereotype NN -2	stereotyped JJ -2	stifled VBD -1	stimulate VB 1	stimulated VBN 1	stimulates VBZ 1	stimulating VBG 2	stingy JJ -2	stolen VBN -2	stop VB -1	stopped VBD -1	stopping VBG -1	stops VBZ -1	stout JJ 2	straight JJ 1	strange JJ -1	strangely RB -1	strangled VBN -2	strength NN 2	strengthen VB 2	strengthened VBN 2	strengthening VBG 2	strengthens VBZ 2	stressed VBD -2	stricken VBN -2	strike NN -1	strong JJ 2	stronger JJR 2	strongest JJS 2	struck VBD -1	struggle NN -2	struggled VBD -2	struggling VBG -2	stubborn JJ -2	stuck VBN -2	stunned VBD -2	stunning JJ 4	stupid JJ -2	stupidly RB -2	suave JJ 2	substantial JJ 1	substantially RB 1	subversive JJ -2	success NN 2	successful JJ 3	suck VB -3	sucks VBZ -3	suffer VB -2	suffering VBG -2	suffers VBZ -2	suicidal JJ -2	suicide NN -2	suing VBG -2	sulking VBG -2	sulky JJ -2	sullen JJ -2	sunglasses NN 1	sunshine NN 2	super JJ 3	superb JJ 5	superior JJ 2	support NN 2	supported VBN 2	supporter NN 1	supporting VBG 1	supportive JJ 2	supports VBZ 2	survived VBD 2	surviving VBG 2	survivor NN 2	suspect VBP -1	suspected VBN -1	suspecting VBG -1	suspects VBZ -1	suspend VB -1	suspended VBN -1	suspicious JJ -2	swear VB -2	swearing NN -2	swears VBZ -2	sweat NN -1	sweet JJ 2	swift JJ 2	swiftly RB 2	swindling VBG -3	sympathetic JJ 2	sympathy NN 2	tender NN 2	tense JJ -2	tension NN -1	terrible JJ -3	terribly RB -3	terrific JJ 4	terrified VBN -3	terror NN -3	terrorize VB -3	terrorized VBN -3	thank VB 2	thankful JJ 2	thorny JJ -2	thoughtful JJ 2	thoughtless JJ -2	threat NN -2	threaten VB -2	threatened VBN -2	threatening VBG -2	threatens VBZ -2	thrilled VBN 5	thwart VB -2	thwarted VBN -2	thwarting VBG -2	timid JJ -2	timorous JJ -2	tired VBN -2	tolerant JJ 2	toothless JJ -2	top JJ 2	torn VBN -2	torture NN -4	tortured VBN -4	totalitarian JJ -2	totalitarianism NN -2	tout VB -2	touted VBN -2	touting VBG -2	touts VBZ -2	tragedy NN -2	tragic JJ -2	tranquil JJ 2	trap NN -1	trapped VBN -2	trauma NN -3	traumatic JJ -3	travesty NN -2	treason NN -3	treasonous JJ -3	treasure NN 2	trembling VBG -2	tremulous JJ -2	tricked VBN -2	trickery NN -2	triumph NN 4	triumphant JJ 4	trouble NN -2	troubled JJ -2	true JJ 2	trust NN 1	trusted VBN 2	tumor NN -2	ugh UH -1	ugly JJ -3	unacceptable JJ -2	unamused VBN -2	unappreciated JJ -2	unapproved JJ -2	unaware JJ -2	unbelievable JJ -1	unbelieving JJ -1	unbiased JJ 2	uncertain JJ -1	unclear JJ -1	uncomfortable JJ -2	unconcerned JJ -2	unconfirmed JJ -1	unconvinced JJ -1	undecided JJ -1	underestimate VB -1	underestimated VBN -1	underestimates VBZ -1	undermine VB -2	undermined VBN -2	undermines VBZ -2	undermining VBG -2	undesirable JJ -2	uneasy JJ -2	unemployment NN -2	unequal JJ -1	unequaled JJ 2	unethical JJ -2	unfair JJ -2	unfocused JJ -2	unfulfilled JJ -2	unhappy JJ -2	unhealthy JJ -2	unified JJ 1	unimpressed JJ -2	united VBN 1	unjust JJ -2	unlovable JJ -2	unmatched JJ 1	unmotivated JJ -2	unprofessional JJ -2	unsatisfied JJ -2	unsecured JJ -2	unsettled JJ -1	unsophisticated JJ -2	unstable JJ -2	unstoppable JJ 2	unsupported JJ -2	unsure JJ -1	untarnished JJ 2	unwanted JJ -2	unworthy JJ -2	upset VBN -2	upsetting VBG -2	uptight JJ -2	urgent JJ -1	useful JJ 2	usefulness NN 2	useless JJ -2	uselessness NN -2	vague JJ -2	validate VB 1	validated VBN 1	validating VBG 1	verdict NN -1	vested VBN 1	vexing JJ -2	vibrant JJ 3	vicious JJ -2	victim NN -3	victimize VBP -3	victimized VBN -3	victimizes VBZ -3	vigilant JJ 3	vile JJ -3	vindicate VB 2	vindicated VBN 2	violate VB -2	violated VBD -2	violates VBZ -2	violating VBG -2	violence NN -3	violent JJ -3	virtuous JJ 2	virulent JJ -2	vision NN 1	visionary JJ 3	vitality NN 3	vitamin NN 1	vitriolic JJ -3	vivacious JJ 3	vociferous JJ -1	vulnerability NN -2	vulnerable JJ -2	walkout NN -2	want VBP 1	war NN -2	warfare NN -2	warm JJ 1	warmth NN 2	warn VB -2	warned VBD -2	warning NN -3	warns VBZ -2	waste NN -1	wasted VBN -2	wasting VBG -2	wavering VBG -1	weak JJ -2	weakness NN -2	wealth NN 3	wealthy JJ 2	weary JJ -2	weep VB -2	weeping VBG -2	weird JJ -2	welcome JJ 2	welcomed VBD 2	welcomes VBZ 2	whimsical JJ 1	whitewash NN -3	whore NN -4	wicked JJ -2	widowed VBN -1	willingness NN 2	win VB 4	wink NN 4	winner NN 4	winning VBG 4	wins VBZ 4	wish VBP 1	wishes VBZ 1	wishing VBG 1	withdrawal NN -3	woebegone JJ -2	woeful JJ -3	won VBD 3	wonderful JJ 4	woo VB 3	worn VBN -1	worried VBN -3	worry VB -3	worrying VBG -3	worse JJR -3	worsen VB -3	worsened VBD -3	worsening VBG -3	worsens VBZ -3	worshiped VBN 3	worst JJS -3	worth JJ 2	worthless JJ -2	worthy JJ 2	wrathful JJ -3	wreck NN -2	wrong JJ -2	wronged VBN -2	yeah UH 1	yearning NN 1	youthful JJ 2	yummy JJ 3	zealot NN -2	zealous JJ 2	😠 EM -4	😧 EM -4	😲 EM 3	😊 EM 3	😰 EM -2	😖 EM -2	😕 EM -2	😢 EM -2	😿 EM -2	😞 EM -2	😥 EM -1	😵 EM -1	😑 EM 0	😨 EM -2	😳 EM -2	😦 EM -1	😬 EM -2	😁 EM -1	😀 EM 3	😍 EM 4	😻 EM 4	😯 EM -1	👿 EM -5	😇 EM 4	😂 EM 4	😹 EM 4	😗 EM 3	😽 EM 3	😚 EM 3	😘 EM 4	😙 EM 3	😆 EM 1	😷 EM -1	😐 EM 0	😶 EM 0	😮 EM -2	😔 EM -1	😣 EM -2	😾 EM -5	😡 EM -5	😌 EM 3	😱 EM -4	🙀 EM -4	😴 EM 0	😪 EM 0	😄 EM 3	😸 EM 3	😃 EM 3	😺 EM 3	😈 EM -4	😏 EM 3	😼 EM 3	😭 EM -4	😛 EM 1	😝 EM 0	😜 EM -1	😎 EM 1	😓 EM -1	😅 EM 3	😫 EM -2	😤 EM 5	😒 EM -2	😩 EM -2	😉 EM 4	😟 EM -4	😋 EM 4	>( EM -4	>[ EM -4	>-( EM -4	>-[ EM -4	>=( EM -4	>=[ EM -4	>=-( EM -4	>=-[ EM -4	\\) EM 3	\\] EM 3	\\D EM 3	-\\) EM 3	-\\] EM 3	-\\D EM 3	=\\) EM 3	=\\] EM 3	=\\D EM 3	=-\\) EM 3	=-\\] EM 3	=-\\D EM 3	\\\\ EM -2	-/ EM -2	-\\\\ EM -2	=/ EM -2	=\\\\ EM -2	=-/ EM -2	=-\\\\ EM -2	-( EM -2	-[ EM -2	-| EM -2	'( EM -2	'[ EM -2	'| EM -2	'-( EM -2	'-[ EM -2	'-| EM -2	=( EM -2	=[ EM -2	=| EM -2	=-( EM -2	=-[ EM -2	=-| EM -2	='( EM -2	='[ EM -2	='| EM -2	='-( EM -2	='-[ EM -2	='-| EM -2	]( EM -5	][ EM -5	]-( EM -5	]-[ EM -5	]=( EM -5	]=[ EM -5	]=-( EM -5	]=-[ EM -5	o) EM 4	o] EM 4	oD EM 4	o-) EM 4	o-] EM 4	o-D EM 4	o=) EM 4	o=] EM 4	o=D EM 4	o=-) EM 4	o=-] EM 4	o=-D EM 4	O) EM 4	O] EM 4	OD EM 4	O-) EM 4	O-] EM 4	O-D EM 4	O=) EM 4	O=] EM 4	O=D EM 4	O=-) EM 4	O=-] EM 4	O=-D EM 4	0) EM 4	0] EM 4	0D EM 4	0-) EM 4	0-] EM 4	0-D EM 4	0=) EM 4	0=] EM 4	0=D EM 4	0=-) EM 4	0=-] EM 4	0=-D EM 4	-) EM 4	-] EM 4	-D EM 4	') EM 4	'] EM 4	'D EM 4	'-) EM 4	'-] EM 4	'-D EM 4	=) EM 4	=] EM 4	=D EM 4	=-) EM 4	=-] EM 4	=-D EM 4	=') EM 4	='] EM 4	='D EM 4	='-) EM 4	='-] EM 4	='-D EM 4	-* EM 3	=* EM 3	=-* EM 3	x) EM 1	x] EM 1	xD EM 1	x-) EM 1	x-] EM 1	x-D EM 1	X) EM 1	X] EM 1	X-) EM 1	X-] EM 1	X-D EM 1	-o EM -2	-O EM -2	-0 EM -2	=o EM -2	=O EM -2	=0 EM -2	=-o EM -2	=-O EM -2	=-0 EM -2	-@ EM -5	=@ EM -5	=-@ EM -5	]) EM -4	]] EM -4	]D EM -4	]-) EM -4	]-] EM -4	]-D EM -4	]=) EM -4	]=] EM -4	]=D EM -4	]=-) EM -4	]=-] EM -4	]=-D EM -4	-p EM 1	-P EM 1	-d EM 1	=p EM 1	=P EM 1	=d EM 1	=-p EM 1	=-P EM 1	=-d EM 1	xP EM 0	x-p EM 0	x-P EM 0	x-d EM 0	Xp EM 0	Xd EM 0	X-p EM 0	X-P EM 0	X-d EM 0	;p EM -1	;P EM -1	;d EM -1	;-p EM -1	;-P EM -1	;-d EM -1	8) EM 1	8] EM 1	8D EM 1	8-) EM 1	8-] EM 1	8-D EM 1	B) EM 1	B] EM 1	B-) EM 1	B-] EM 1	B-D EM 1	'=( EM -1	'=[ EM -1	'=-( EM -1	'=-[ EM -1	'=) EM 3	'=] EM 3	'=D EM 3	'=-) EM 3	'=-] EM 3	'=-D EM 3	-$ EM -2	-s EM -2	-z EM -2	-S EM -2	-Z EM -2	=$ EM -2	=s EM -2	=z EM -2	=S EM -2	=Z EM -2	=-$ EM -2	=-s EM -2	=-z EM -2	=-S EM -2	=-Z EM -2	:) EM 3	:] EM 3	:[ EM -3	:( EM -3	:| EM -1	:/ EM -1	:d EM 4	:s EM 0	:o EM -1	:-) EM 3	:-( EM -3	:-| EM -1	:-/ EM -1	:-d EM 3	:-p EM 3	^^ EM 2	;) EM 4	;] EM 4	;D EM 4	;-) EM 4	;-] EM 4	;-D EM 4	<3 EM 3	wtf UH -4	brb UH 0	btw UH 0	b4n UH 0	bcnu UH 0	bff UH 0	cya UH 0	dbeyr UH -1	ily UH 2	lmao UH 2	lol UH 3	np UH 0	oic UH 0	omg UH 0	rotflmao UH 4	stby UH -2	swak UH 2	tfh UH -2	rtm UH -1	rtfm UH -2	ttyl UH 0	tyvm UH 2	wywh UH 2	xoxo UH 2	gah UH -1	yuck UH -2	ew UH -1	eww UH -2	abbo - -5	abductions - -2	abhors - -3	absolves - 2	acquits - 2	acquitting - 2	admonish - -2	agog - 2	agonise - -3	agonised - -3	agonises - -3	agonising - -3	alarmists - -2	amazes - 2	apeshit - -3	apologise - -1	apologised - -1	apologises - -1	apologising - -1	appeases - 2	ashame - -2	assfucking - -4	asshole - -4	axed - -1	badass - -3	bamboozle - -2	bamboozles - -2	bankster - -3	benefitted - 2	benefitting - 2	bereave - -2	bereaved - -2	bereaves - -2	bereaving - -2	blah - -2	blesses - 2	bummer - -2	calms - 2	chagrined - -2	charmless - -3	chastise - -3	chastising - -3	cheerless - -2	chokes - -2	clueless - -2	cocksucker - -5	cocksuckers - -5	collide - -1	collides - -1	colliding - -1	colluding - -3	combats - -1	conciliated - 2	conciliates - 2	conciliating - 2	conflictive - -2	congrats - 2	consolable - 2	contagions - -2	contestable - -2	controversially - -2	crazier - -2	craziest - -2	cunt - -5	daredevil - 2	degrades - -2	dehumanizes - -2	dehumanizing - -2	deject - -2	dejected - -2	dejecting - -2	dejects - -2	denier - -2	deniers - -2	derails - -2	derides - -2	deriding - -2	dick - -4	dickhead - -4	diffident - -2	dipshit - -3	direful - -3	discarding - -1	discards - -1	disconsolate - -2	disconsolation - -2	disguising - -1	disparages - -2	disputing - -2	disregards - -2	disrespected - -2	distracts - -2	distrustful - -3	dodgy - -2	dolorous - -2	douche - -3	douchebag - -3	douchebaggery - -3	downhearted - -2	droopy - -2	dumbass - -3	dupe - -2	eery - -2	embarrasses - -2	enlightens - 2	enrages - -2	enraging - -2	enrapture - 3	enslaves - -2	enthral - 3	envies - -1	envying - -1	eviction - -1	exaggerates - -2	exhilarates - 3	exonerates - 2	expels - -2	exultant - 3	fag - -3	faggot - -3	faggots - -3	fainthearted - -2	fatiguing - -2	favorited - 2	fidgety - -2	fraudster - -4	fraudsters - -4	fraudulence - -4	frikin - -2	ftw - 3	fucked - -4	fucker - -4	fuckers - -4	fuckface - -4	fuckhead - -4	fucking - -2	fucktard - -4	fud - -3	fuked - -4	fuking - -4	gallantly - 3	glamourous - 3	greenwash - -3	greenwasher - -3	greenwashers - -3	greenwashing - -3	haha - 3	hahaha - 3	hahahah - 3	haplessness - -2	heartbroken - -3	heavyhearted - -2	hoax - -2	honouring - 2	hooligan - -2	hooligans - -2	humerous - 3	humourous - 2	hysterics - -3	inconsiderate - -2	indoctrinate - -2	indoctrinates - -2	infatuated - 2	infuriates - -2	innovates - 1	inquisition - -2	jackasses - -4	jesus - 1	jew - -2	laughting - 1	lawl - 3	lifesaver - 4	lmfao - 4	loathe - -3	looses - -3	lugubrious - -2	mirthful - 3	mirthfully - 3	misbehave - -2	misbehaved - -2	misbehaves - -2	mischiefs - -1	misgiving - -2	misreporting - -2	mocks - -2	mongering - -2	monopolizes - -2	mope - -1	moping - -1	moron - -3	motherfucker - -5	motherfucking - -5	mumpish - -2	n00b - -2	naïve - -2	negativity - -2	niggas - -5	noob - -2	nosey - -2	offline - -1	optionless - -2	overreacts - -2	oversell - -2	overselling - -2	oversells - -2	oversimplifies - -2	oversimplify - -2	overstatements - -2	oxymoron - -1	pardoning - 2	pensive - -1	perfects - 2	persecutes - -2	pesky - -2	phobic - -2	pissed - -4	pissing - -3	pollutes - -2	postpones - -1	prblm - -2	prblms - -2	proactive - 2	profiteer - -2	prosecutes - -1	pseudoscience - -3	rageful - -2	ranter - -3	ranters - -3	rants - -3	raptured - 2	rapturous - 4	reassures - 1	regretful - -2	regretting - -2	repulse - -1	revengeful - -2	rofl - 4	roflcopter - 4	rotfl - 4	rotflmfao - 4	rotflol - 4	sadden - -2	scumbag - -4	secures - 2	shithead - -4	shitty - -3	short-sighted - -2	short-sightedness - -2	shrew - -4	singleminded - -2	slickest - 2	slut - -5	spam - -2	spammer - -3	spammers - -3	spamming - -2	spiteful - -2	starves - -2	stressor - -2	swindle - -3	swindles - -3	tard - -2	terrorizes - -3	thwarts - -2	torturing - -4	twat - -5	uncredited - -1	underestimating - -1	undeserving - -2	unemploy - -2	unintelligent - -2	unloved - -2	unresearched - -2	validates - 1	vexation - -2	victimizing - -3	vindicates - 2	vindicating - 2	visioning - 1	wanker - -3	wetback - -5	winwin - 3	woohoo - 3	wooo - 4	woow - 4	wow - 4	wowow - 4	wowww - 4	yeees - 2	yucky - -2	zealots - -2	☺️ - 3	cold_sweat - -2	crying_cat_face - -2	disappointed_relieved - -1	dizzy_face - -1	grimacing - -2	heart_eyes - 4	heart_eyes_cat - 4	imp - -5	joy_cat - 4	kissing_cat - 3	kissing_closed_eyes - 3	kissing_heart - 4	kissing_smiling_eyes - 3	neutral_face - 0	no_mouth - 0	open_mouth - -2	pouting_cat - -5	scream_cat - -4	smile_cat - 3	smiley - 3	smiley_cat - 3	smiling_imp - -4	smirk_cat - 3	stuck_out_tongue - 1	stuck_out_tongue_closed_eyes - 0	stuck_out_tongue_winking_eye - -1	sweat_smile - 3	tired_face - -2	yum - 4	reatrdo - -5	 - undefined");
}();

!function() {
    
    var unescapes = {
            // Borrowed from https://github.com/mathiasbynens/he/blob/master/src/he.js for
            // the HTML entities, and smartquotes from GitHub issue
            // https://github.com/Ulflander/compendium-js/issues/1
            '"': /(&quot;|\u201C|\u201D)/gi,
            '&': /&amp;/gi,
            '\'': /(&#x27;|\u2018|\u2019)/gi,
            '<': /&lt;/gi,
            // See https://mathiasbynens.be/notes/ambiguous-ampersands: in HTML, the
            // following is not strictly necessary unless it’s part of a tag or an
            // unquoted attribute value. We’re only escaping it to support those
            // situations, and for XML support.
            '>': /&gt;/gi,
            // In Internet Explorer ≤ 8, the backtick character can be used
            // to break out of (un)quoted attribute values or HTML comments.
            // See http://html5sec.org/#102, http://html5sec.org/#108, and
            // http://html5sec.org/#133.
            '`': /&#x60/gi,

            // Damn generation-y and millenials, damn impoliteness
            'shit': /(s\&\^t|sh\*t)/gi,
            'fuck': /(f\*ck)/gi,
            'just kidding': 'j/k',
            'without': /w\/[to]/g,
            'with': 'w/',
            ' out of ': /\soutta\s/gi
        };

    /**
     * Decode a string: replace some HTML entities (such as `&amp;` to `&`) and some 
     * slangs that include some punctuation chars (such as `w/` to `with`). This function
     * is called by the analyser **before** tokenization.
     *
     * @memberOf compendium
     * @param  {String} txt String to decode
     * @return {String}     Decoded string
     */
    compendium.decode = function(txt) {
        var k;
        for (k in unescapes) {
            if (unescapes.hasOwnProperty(k)) {
                txt = txt.replace(unescapes[k], k);
            }
        }
        return txt;
    };
}();
/**
 * Dependency parsing v2.
 */
(function() {



    extend(parser, {
        /**
         * Parse grammar dependencies of a sentence object and
         * set the root property of the sentence.
         *
         * @param  {Sentence} sentence The sentence to be parsed.
         */
        parse: function(sentence) {
            var tags = sentence.tags,
                tree = createNode('ROOT'),
                i, k, l = sentence.length,
                stop = 0;

            var nodes = [];
            // First create a node for every token
            // of the sentence
            for (i = 0; i < l; i += 1) {
                nodes[i] = createNode(tags[i], i, i);
            }

            // First we create the chunks to assign
            // a partial structure of the sentence.
            // In Compendium, chunks are one node.
            //
            // The following will aggregate the nodes
            // that match the chunks signatures.
            //
            // The nodes array will be reduced so it
            // contains only the chunks (and the nodes)
            // that didn't match any chunk.
            buildChunks(nodes);

            // We now run the parser that
            // build the relationships until
            // we have only one node (parsing successful)
            // or we reach the recursion limit.
            //
            // The parser will populate the left and right
            // properties of each node with all their subnodes.
            // It should remain only one node, that is the ROOT.
            while (stop < 10 && nodes.length > 1) {
                buildRelationships(nodes, stop);
                stop += 1;
            }

            // At last we repair the parsing for a few
            // corner case and make it usable for the user.
            repair(nodes, tags, sentence);


            //tmp, should only have one master in all cases
            sentence.root = nodes[0];
            sentence.root.label = 'ROOT';
            if (nodes.length > 1) {
                // console.log('Failed parsing: ' + sentence.raw, nodes);
            }
        },

        connect: function(analysis) {

        }

    });

    var chunks = [
        ['NP', ['NNP', 'CD', 'NNS']],
        ['NP', ['DT', 'PRP$', 'JJ', 'JJS', '$', 'CD', '$', 'NN', 'NNS']],
        ['NP', ['DT', 'PRP$', 'JJ', 'JJS', '$', 'CD', '$', 'NNP', 'NNPS']],
        ['VP', ['MD', 'VBP', 'VB']],
        ['VP', ['MD', 'VBD']],
        ['VP', ['VBZ', 'VBG']],
        ['NP', ['NNP', 'NNPS']],
        ['ADV', ['RB', 'RB']],
        ['ADJP', ['RB', 'JJ']],
        ['PP', 'IN'],
        ['PRT', 'RP'],
        ['NP', 'PRP'],
        ['NP', 'NNP'],
        ['NP', 'NNPS'],
        ['NP', 'NN'],
        ['NP', 'DT'],
        ['ADJ', 'JJ'],
        ['NP', 'NNS'],
        ['VAUX', ['VB', 'RB']],
        ['VAUX', ['VBP', 'RB']],
        ['VP', 'VBZ'],
        ['VP', 'VBP'],
        ['VP', 'VBD'],
        ['ADV', 'WRB'],
        ['ADV', 'RB'],
        ['PUNCT', '.'],
        ['PUNCT', ','],
        ['SP', ['PP', 'NP']],
    ];

    var relationships = [
        ['NP', 'VP', 1, 'NSUBJ'],
        ['VP', 'NP', 0, 'DOBJ'],
        ['VB', 'NP', 0, 'DOBJ'],
        ['PP', 'NP', 0, 'POBJ'],
        ['NP', 'PP', 0, 'PREP'],
        ['VP', 'PP', 0, 'PREP'],
        ['VB', 'PP', 0, 'PREP'],
        ['VP', 'VP', 0, 'CCOMP'],
        ['VP', 'ADV', 0, 'ADVMOD'],
        ['VB', 'ADV', 0, 'ADVMOD'],
        ['ADV', 'PP', 0, 'PREP'],
        ['PP', 'VP', 1, 'PREP'],
        ['VP', 'ADJ', 0, 'ACOMP'],
        ['VB', 'ADJ', 0, 'ACOMP'],
        ['VB', 'VP', 1, 'AUX'],
        ['VAUX', 'VP', 1, 'AUX'],
        ['VAUX', 'VB', 1, 'AUX'],
        ['VP', 'PUNCT', 0, 'PUNCT', 1],
        ['VB', 'PUNCT', 0, 'PUNCT', 1],
        ['PUNCT', 'VP', 1, 'PUNCT', 1],
        ['PUNCT', 'VB', 1, 'PUNCT', 1],
        ['ADV', 'VP', 1, 'ADVMOD', 2],
        ['ADV', 'VB', 1, 'ADVMOD', 2],
        ['ADV', 'ADV', 1, 'ADVMOD', 2],
    ];

    function buildChunk(chunkId, chunkTags, nodes) {
        var left,
            right,
            leftNode,
            rightNode,
            isUnique = typeof chunkTags === 'string',
            l = nodes.length - (isUnique ? 1 : 2);

        for(l; l >= 0; l -= 1) {
            leftNode = nodes[l];

            if (!isUnique) {
                rightNode = nodes[l + 1];
                left = chunkTags.indexOf(leftNode.type);
                right = chunkTags.indexOf(rightNode.tags[0]);

                if (left > -1 && right > -1 && left <= right) {
                    leftNode.type = chunkId;
                    leftNode.to = rightNode.to;
                    leftNode.tags = leftNode.tags.concat(rightNode.tags);
                    nodes.splice(l + 1, 1);
                }

            } else if (chunkTags === leftNode.type) {
                leftNode.type = chunkId;
            }
        }
    }

    function buildChunks(nodes) {
        var i, l = chunks.length;
        for (i = 0; i < l; i += 1) {
            buildChunk(chunks[i][0], chunks[i][1], nodes);
        }
    }

    function testNodes(leftType, rightType, run) {
        var i, l = relationships.length, left, right;
        for (i = 0; i < l; i += 1) {
            left = relationships[i][0];
            right = relationships[i][1];

            // Dont tests some of the rules before being at given
            // Kind of repair before repair
            if (relationships[i].length > 4 && run < relationships[i][4]) {
                continue;
            }

            // If match, send back the direction of the
            // relationship and its label.
            if (left === leftType && right === rightType) {
                return [relationships[i][2], relationships[i][3]];
            }
        }
        return -1;
    }

    function buildRelationships(nodes, run) {
        var i, l, last = -1, res, leftNode, rightNode;
        for(l = nodes.length - 2; l >= 0; l -= 1) {
            leftNode = nodes[l];
            rightNode = nodes[l + 1];
            res = testNodes(leftNode.type, rightNode.type, run);

            if (res[0] === 0) {
                leftNode.right.push(rightNode);
                nodes.splice(l + 1, 1);
                rightNode.label = res[1];

            } else if (res[0] === 1) {
                if (res[1] === 'NSUBJ' && findByIs('NSUBJ', rightNode.left)) {
                    continue;
                }
                rightNode.left.push(leftNode);
                nodes.splice(l, 1);
                leftNode.label = res[1];
            }
        }
    }


    function repair(nodes, tags, sentence) {
        // If VP root,
        // no subject,
        // and one right NP,
        // then this NP become NSUBJ rather than DOBJ
        var tmpRoot = nodes[0],
            l = nodes.length;

        if (tmpRoot.type === 'VP' && !findByIs('NSUBJ', tmpRoot.left) && !findByIs('NSUBJ', tmpRoot.right)) {
            var res = findByIs('DOBJ', nodes[0].right);
            if (!!res) {
                res.label = 'NSUBJ';
            }
        }

        // If nodes last is punc and index is 1,
        // set its parent to 0
        if (l === 2 && nodes[1].type === 'PUNCT') {
            tmpRoot.right.push(nodes[1]);
            nodes[1].label = 'PUNCT';
            nodes.splice(1, 1);
        }

        // Recursively loop onto nodes in order to
        // set the raw and norm properties, make sure
        // also to reorder the children branches.
        reconstruct(nodes, tags, sentence);
    }

    function reconstruct(nodes, tags, sentence) {
        var i = 0, l = nodes.length,
            node,
            j, m,
            raw,
            norm;

        for (i; i < l;  i += 1) {
            node = nodes[i];
            raw = '';
            norm = '';
            for (j = node.from; j <= node.to; j += 1) {
                raw += ' ' + sentence.tokens[j].raw;
                norm += ' ' +sentence.tokens[j].norm;
            }
            node.raw = raw.slice(1);
            node.norm = norm.slice(1);
            reconstruct(node.left, tags, sentence);
            reconstruct(node.right, tags, sentence);
            node.left.sort(function(a, b) {return a.from - b.from;})
            node.right.sort(function(a, b) {return a.from - b.from;})
        }
    }

    function findByType(type, nodes) {
        for (var i = 0, l = nodes.length; i < l; i += 1) {
            if (nodes[i].type === type) {
                return nodes[i];
            }
        }
        return null;
    }

    function findByIs(is, nodes) {
        for (var i = 0, l = nodes.length; i < l; i += 1) {
            if (nodes[i].label === is) {
                return nodes[i];
            }
        }
        return null;
    }

    function createNode(type, from, to, tags) {
        return {
            meta: {},
            left: [],
            right: [],
            tags: tags || [type],
            from: from,
            to: to,
            raw: null,
            norm: null,
            type: type,
            is: null,
        }
    };

}());


/*
 * Dependency parsing module.
 *
 * Still experimental, requires a lot of additional rules, but is very
 * promising.
 *
 * Constraint based. Constraints are:
 *
 * - The governor is the head of the sentence (it doesnt have a master)
 * - When possible, the governor is the first conjugated verb of the sentence
 * - All other tokens must have a master
 * - A token can have one and only one master
 * - If no master is found for a token, then its master is the governor
 *
 * Inspired by [Syntex (fr)](http://slideplayer.fr/slide/1150457/).
 *
 */
!function() {

    var governors = [
            // First ranked governors
            ['VBZ', 'VBP', 'VBD', 'VBG'],
            // Second ranked governors
            ['MD', 'VB'],
            // Third rank governors
            ['NNP', 'NNPS', 'NN', 'NNS'],
            // Fourth rank
            ['WP', 'WRB'],
            // Fifth rank
            ['UH']
        ],

        default_type = 'unknown',

        // Left to right rules
        left2right = [
            ['NNP', 'NNP', 'compound'],
            ['PRP', 'VBZ', 'subj'],
            ['PRP', 'VBP', 'subj'],
            ['PRP', 'VBD', 'subj'],
            ['DT', 'VBZ', 'subj'],
            ['DT', 'VBP', 'subj'],
            ['DT', 'VBD', 'subj'],
            ['WRB', 'VBP', 'attr'],
            ['WRB', 'VBZ', 'attr'],
            ['WRB', 'VBD', 'attr'],
            ['VBG', 'VBP'],
            ['TO', 'VB'],
            ['TO', 'NN'],
            ['TO', 'NNS'],
            ['DT', 'NN', 'det'],
            ['DT', 'NNP', 'det'],
            ['PRP$', 'NN', 'poss'],
            ['RB', 'JJ', 'advmod'],
            ['JJ', 'NN', 'amod'],
            ['JJ', 'NNS', 'amod'],
            ['JJ', 'NNP', 'amod'],
            ['VBG', 'JJ'],
            ['NN', 'VBZ', 'subj'],
            ['NN', 'VBP', 'subj'],
            ['NN', 'VBD', 'subj'],
            ['NN', 'VB', 'subj'],
            ['NNP', 'VBZ', 'subj'],
            ['NNP', 'VBP', 'subj'],
            ['NNP', 'VBD', 'subj'],
            ['NNP', 'VB', 'subj']
        ],



        right2left = [
            ['PRP', 'VBZ', 'obj'],
            ['PRP', 'VBP', 'obj'],
            ['PRP', 'VBD', 'obj'],
            ['NN', 'IN', 'obj'],
            ['IN', 'VBZ'],
            ['IN', 'VBP'],
            ['IN', 'VBD'],
            ['IN', 'VBG'],
            ['JJ', 'VBD', 'acomp'],
            ['JJ', 'VBP', 'acomp'],
            ['JJ', 'VBZ', 'acomp'],
            ['IN', 'VB'],
            ['CC', 'JJ'],
            ['NNP', 'VB', 'obj'],
            ['NN', 'VB', 'obj'],
            ['VB', 'VB', 'xcomp']
        ],

        REC_LIMIT = 20;

    extend(dependencies, {

        /**
         * Expand the dependencies using existing ones.
         *
         * For example: `the quick brown fox`. We know from the initial rules that
         * `fox` is a master of `brown`.
         *
         * Then on the first left to right pass:
         * - we consider relationship of `the` to `quick` - `quick` doesn't have
         *   any master so we pass this token
         * - we consider relationship of `quick` to `brown`
         * - `brown` has a master (that is `fox`). We select it and check
         *   if a rule apply to `quick/JJ` and `fox/NN`. Indeed the rule exist (it was
         *   the same that allowed to connect `brown` to `fox`), so we now know that
         *   `quick` is also a dependency of `fox`.
         *
         * Then this pass ends. The function will return `true` (changes have been applied),
         * so we know we must call it again.
         *
         * On the second pass:
         * - we consider relationship of `the` to `quick` - `quick` doesn't have
         *   any master so we pass this token
         *
         *
         * @memberOf compendium.dependencies
         * @param  {Sentence} sentence Sentence object
         * @return {Boolean} Value `true` if some changes have been applied, false
         * otherwise
         */
        expand: function(sentence, diff) {
            var i, l = sentence.length,
                j, m = left2right.length,
                r = 0,
                tag,
                next,
                master,
                changes = false,
                token;

            // First expand from left to right
            for (i = 0; i < l - diff; i ++, r = 0) {
                token = sentence.tokens[i];
                if (typeof token.deps.master === 'number') {
                    continue;
                }

                next = sentence.tokens[i + diff];

                // If master already set
                if (token.deps.master === next.deps.master ||
                    // or next master not set
                    typeof next.deps.master !== 'number') {
                    // we skip
                    continue;
                }

                // Gather next token master
                while ((master = sentence.tokens[next.deps.master]) && next !== master && next.deps.master && token.deps.master !== next.deps.master) {
                    r ++;
                    if (r > REC_LIMIT) {
                        break;
                    }

                    tag = token.pos;

                    // And attempt to apply a rule to this combo (current token + next token master)
                    for (j = 0; j < m; j ++) {
                        if (tag === left2right[j][0] && master.pos === left2right[j][1]) {
                            token.deps.master = next.deps.master;
                            token.deps.type = left2right[j][2] || default_type;
                            // If a change is done, set the flag to true
                            changes = true;
                            break;
                        }
                    }
                    if (changes) {
                        break;
                    }
                    next = master;
                }
            }

            // And do the same backward
            for (i = l - 1, m = right2left.length; i > diff; i --) {
                token = sentence.tokens[i];
                if (typeof token.deps.master === 'number') {
                    continue;
                }

                next = sentence.tokens[i - diff];
                if (typeof next.deps.master !== 'number' || token.deps.master === next.deps.master) {
                    continue;
                }

                master = sentence.tokens[next.deps.master];
                tag = token.pos;

                for (j = 0; j < m; j ++) {
                    if (tag === right2left[j][0] && master.pos === right2left[j][1]) {
                        token.deps.master = next.deps.master;
                        token.deps.type = right2left[j][2] || default_type;
                        changes = true;
                        break;
                    }
                }
            }

            return changes;
        },

        /**
         * Loop over sentence tokens and create the dependency hierarchy.
         *
         * @memberOf compendium.dependencies
         * @param  {Sentence} sentence Sentence object
         */
        parse: function(sentence) {
            var i, l = sentence.length,
                j, m = left2right.length,
                r = 0,
                changes = true,
                governor = null,
                lastFirstRank = null,
                rank = 0,
                tag,
                next,
                compound = 0,
                token;

            // Handle special case of only one token in the sentence
            if (l === 1) {
                token = sentence.tokens[0];
                token.deps.governor = true;
                sentence.governor = 0;
                return;
            }

            // First pass: apply the left to right rules
            // so direct left to right relationships are created.
            // This pass also define the governor.
            for (i = 0; i < l - 1; i ++) {
                token = sentence.tokens[i];
                next = sentence.tags[i + 1];
                tag = token.pos;

                //  Test first ranked governors
                if (governors[rank].indexOf(tag) > -1) {
                    // If no governor set, we found one!
                    if (governor === null) {
                        governor = i;
                        lastFirstRank = i;
                    // Otherwise, governor is master of this token
                    } else {
                        token.deps.master = governor;
                    }
                    continue;
                }

                // We check each left2right rule
                for (j = 0; j < m; j ++) {
                    // And we add master if rule apply
                    if (tag === left2right[j][0] && next === left2right[j][1]) {
                        token.deps.master = i + 1;
                        token.deps.type = left2right[j][2] || default_type;
                        break;
                    }
                }
            }

            // Reinforce compounds
            for (i = l - 1; i >= 0; i --) {
                token = sentence.tokens[i];
                next = sentence.tokens[i + 1];
                if (i !== governor) {
                    // Aggregate compounds, compound means we necessarily have the right token
                    if (token.deps.type === 'compound' || token.deps.type === 'det') {
                        // We found a governor before
                        if (governor !== null && governor < i && typeof next.deps.master !== 'number') {
                            next.deps.master = governor;
                            next.deps.type = 'obj';
                        }
                        compound += 1;
                        if (compound > 1) {
                            token.deps.master = next.deps.master;
                        }
                    } else {
                        compound = 0;
                    }
                }
            }

            // Second pass: apply the right to left rules
            // so direct right to left relationships are created
            // Same process than left to right but starting from the end
            for (i = l - 1, m = right2left.length; i > 0; i --) {
                token = sentence.tokens[i];
                if (typeof token.deps.master === 'number') {
                    continue;
                }
                next = sentence.tags[i - 1];
                tag = token.pos;
                for (j = 0; j < m; j ++) {
                    if (tag === right2left[j][0] && next === right2left[j][1]) {
                        token.deps.master = i - 1;
                        token.deps.type = right2left[j][2] || default_type;
                        break;
                    }
                }
            }

            // Third pass, expand the relationships,
            // given the existing masters,
            // both from left to right and right to left.
            while (changes && r < REC_LIMIT) {
                changes = false;
                for (i = 1; i < 5; i += 1) {
                    changes = this.expand(sentence, i) || changes;
                }
                r += 1;
            }

            // If no governor,
            // loop over ranks and attempt to find one
            m = governors.length - 1;
            while (governor === null && rank < m) {
                rank ++;
                // Otherwise try second ranked governors
                for (i = 0; i < l; i ++) {
                    if (governors[rank].indexOf(sentence.tags[i]) > -1) {
                        governor = i;
                        break;
                    }
                }
            }

            if (governor !== null) {
                sentence.governor = governor;
                sentence.tokens[governor].deps.governor = true;
            }

            // Fourth pass, right to left reconnection, skipping
            // governed tokens
            this.reconnect(sentence);

            // Last pass, any token that has no master
            // gets the governor as its master (i.e. any
            // dependency issue should be solved BEFORE here)
            //
            // This pass also creates the `dependencies` array
            // for each token and collect subjects/objects...
            for (i = 0; i < l; i ++) {
                token = sentence.tokens[i];
                if (i !== governor) {
                    if (token.deps.master === null || token.deps.master === i) {
                        token.deps.master = governor;
                    }
                    if (token.deps.master !== null) {
                        sentence.tokens[token.deps.master].deps.dependencies.push(i);
                    }
                    if (token.deps.type === 'subj') {
                        sentence.deps.subjects.push(i);
                    } else if (token.deps.type === 'obj') {
                        sentence.deps.objects.push(i);
                    }
                }
            }


        },

        /**
         * This function creates relationships over existing one, from right to left.
         *
         * @memberOf compendium.dependencies
         * @param  {Sentence} sentence Sentence object
         */
        reconnect: function(sentence) {
            var i, l = sentence.length,
                j, m = right2left.length,
                k,
                tag,
                next,
                master,
                token;

            for (i = l - 1; i >= 0; i --) {
                token = sentence.tokens[i];

                // Skip if already set
                if (token.deps.governor === true || typeof token.deps.master === 'number') {
                    continue;
                }

                // Here is the trick
                // we go backward the tokens
                // until we find one that is not governed
                // by the current one
                k = i;
                master = i;
                while (master === i) {
                    k --;
                    if (k === -1) {
                        break;
                    }

                    master = sentence.tokens[k].deps.master;
                }

                // Didn't find any, skip
                if (k === -1) {
                    continue;
                }

                // Found a token! Test the right 2 left rules
                next = sentence.tags[k];
                tag = token.pos;

                for (j = 0; j < m; j ++) {
                    if (tag === right2left[j][0] && next === right2left[j][1]) {
                        token.deps.master = k;
                        token.deps.type = right2left[j][2] || default_type;
                        break;
                    }
                }
            }
        }
    });
}();

// Tiny detectors manager
!function() {
    // Lists of detectors to run before dependency parsing
    var before = {
            // Detectors that work at a token level
            t: [],
            // Detectors at the sentence level
            s: []
        },
        after = {
            // Detectors that work at a token level
            t: [],
            // Detectors at the sentence level
            s: [],
            // High level detector
            p: []
        },
        // List of function to call in order
        // to initialize the context for each sentence
        initializers = [];

    detectors.init = function(callback) {
        initializers.push(callback);
    };

    // Generate a brand new, initialized context
    detectors.context = function() {
        var context = {},
            i, l = initializers.length;
        for (i = 0; i < l; i += 1) {
            initializers[i](context);
        }
        return context;
    };

    // Add a detector of given type
    detectors.before = function (type, id, callback) {
        if (typeof id === 'function') {
            callback = id;
            id = null;
        }
        if (before.hasOwnProperty(type)) {
            before[type].push({
                id: id,
                cb: callback
            });
        } else {
            console.warn('No detector with type ' + type);
        }
    };

    detectors.add = function (type, id, callback) {
        if (typeof id === 'function') {
            callback = id;
            id = null;
        }

        console.warn('compendium.detectors.add is a deprecated function - please use compendium.detectors.after');
        return detectors.after(type, callback);
    };

    detectors.after = function (type, id, callback) {
        if (typeof id === 'function') {
            callback = id;
            id = null;
        }

        if (after.hasOwnProperty(type)) {
            after[type].push({
                id: id,
                cb: callback
            });
        } else {
            console.warn('No detector with type ' + type);
        }
    };

    // Apply all detectors of given type on given arguments
    detectors.apply = function (type, isBefore, ignore) {
        var i, l,
            args = Array.prototype.slice.call(arguments).slice(3),
            list = isBefore ? before : after,
            d;

        ignore = ignore || [];

        if (list.hasOwnProperty(type)) {
            for (i = 0, l = list[type].length; i < l; i ++) {
                d = list[type][i];
                if (ignore.indexOf(d.id) === -1) {
                    d.cb.apply(null, args);
                }
            }
        }
    };

}();
!function() {

    var isBeforeProperNoun = function (sentence, index) {
            if (index >= sentence.length) {
                return false;
            }
            var next = sentence.tags[index + 1];
            return next === 'NNP' || next == 'NNPS';
        },
        isSuitableToken = function(tag, norm) {
            return (tag === '&' || tag === 'TO') || (tag === 'CC' && norm !== 'or');
        };

    // Entity detection at the sentence level:
    // consolidate NNP and NNPS
    detectors.before('s', 'entities', function(sentence, index, sentences) {
        var i, l = sentence.length,
            stats = sentence.stats,
            tag,
            token,
            norm,
            lastIndex,
            entity;

        // If sentence is mainly uppercased or capitalized, this strategy cant work
        if (stats.p_upper > 75 || stats.p_cap > 85) {
            return;
        }

        for (i = 0; i < l; i ++) {
            tag = sentence.tags[i];
            token = sentence.tokens[i];
            norm = token.norm;
            if (token.attr.entity > - 1) {
                entity = null;
            } else if (tag === 'NN') {
                entity = null;
            } else if (tag === 'NNP' || tag === 'NNPS' ||
                (!!entity && isSuitableToken(tag, norm) && isBeforeProperNoun(sentence, i))) {
                if (!!entity) {
                    entity.raw += ' ' + token.raw,
                    entity.norm += ' ' + token.norm,
                    entity.toIndex = i;
                    token.attr.entity = lastIndex;
                } else {
                    entity = factory.entity(token, i);
                    lastIndex = token.attr.entity = sentence.entities.push(entity) - 1;
                }
            } else {
                entity = null;
            }
        }


    });
}();
!function() {

    var negations = Object.keys(cpd.neg).concat(Object.keys(cpd.refusal)),
        counterNegationTokens = Object.keys(cpd.neg_neg),
        counterNegationBigrams = [
            ['but', 'to']
        ];

    // Negation detection
    detectors.after('s', 'negation', function(sentence, index, sentences) {
        var i, l = sentence.length,
            j, m = counterNegationBigrams.length,
            previous,
            next,
            master,
            token,
            negated = false,
            ll = 0,
            n = 0;

        for (i = 0; i < l; i ++) {
            token = sentence.tokens[i];
            next = sentence.tokens[i + 1];
            if (token.profile.breakpoint || token.attr.is_punc) {
                ll = 0;
                negated = false;
            } else if (negations.indexOf(token.norm) > -1) {
                if (!negated) {
                    previous = sentence.tokens[i - 1];
                    if (token.pos === 'RB' && previous && (previous.attr.is_verb || previous.pos === 'MD')) {
                        previous.profile.negated = true;
                    }
                    n ++;
                    negated = true;
                } else {
                    negated = false;
                }
            } else if (negated && counterNegationTokens.indexOf(token.norm) > -1 && ll === 0) {
                sentence.tokens[i - 1].profile.negated = false;
                n --;
                negated = false;
            } else if (!!negated) {
                // Check bigram
                for (j = 0; j < m && i < l - 1; j += 1) {
                    if (token.norm === counterNegationBigrams[j][0] && next.norm === counterNegationBigrams[j][1]) {
                        negated = false;
                        break;
                    }
                }
                if (!!negated) {
                    n ++;
                    ll ++;
                }
            }
            token.profile.negated = negated;
        }

        sentence.profile.negated = n > 0;
    });
}();
!function() {

    var interrogative_tags = ['WP', 'WP$', 'WRB'];

    // First pass to detect the type of sentence
    // Types detected in this pass may be used by
    // sentiment analysis detector.
    detectors.after('s', 'type', function(sentence, index) {
        var l = sentence.length,
            stats = sentence.stats,
            governor = sentence.governor,
            types = sentence.profile.types,
            first = sentence.tokens[0],
            last = sentence.tokens[l - 1],
            tag,
            deps,
            i, m;

        // First type: foreign sentence
        // can only work with at least a few tokens
        if (l > 2 &&
            // if more than 10% foreign words and a somewhat low confidence
            ((stats.p_foreign >= 10 && stats.confidence < 0.5) ||
            // or if very low confidence
            stats.confidence <= 0.35)) {
            // then is foreign
            types.push(T_FOREIGN);
        }

        // Headline type: use statistics
        if (stats.p_cap > 75 && stats.p_upper < 50 && l > 10) {
            types.push(T_HEADLINE);
        }

        // Exclamatory, straightforward
        if (last.norm === '!') {
            types.push(T_EXCLAMATORY);
        } else
        // Question, obviously with final "?"
        if (last.norm === '?' ||
            // or starting with a particular tag, without breakpoint
            (interrogative_tags.indexOf(first.pos) > -1 && stats.breakpoints === 0)) {
            types.push(T_INTERROGATIVE);
        } else

        // Only if dependency parsing returned results
        if (governor > -1) {
            tag = sentence.tags[governor];
            // Interrogative
            // Governor is an interrogative tag
            if (interrogative_tags.indexOf(tag) > -1) {
                types.push(T_INTERROGATIVE);
            } else
            // Loop onto governor dependencies
            // Requires VB governor + no final `.`
            if (last.pos !== '.' && tag.indexOf('VB') === 0) {
                // check for "do i do" or "are you going"
                if (sentence.tags[governor + 1] === 'PRP' && (sentence.tags[governor + 2] || '').indexOf('VB') === 0) {
                    types.push(T_INTERROGATIVE);
                } if (governor > 1 && sentence.tags[governor - 1] === 'PRP' && sentence.tags[governor - 2].indexOf('VB') === 0) {
                    types.push(T_INTERROGATIVE);
                // check for "can i go"
                } else if (sentence.tags[governor - 1] === 'PRP' && sentence.tags[governor - 2] === 'MD') {
                    types.push(T_INTERROGATIVE);
                } else {

                    for (deps = sentence.tokens[governor].deps.dependencies, i = 0, m = deps.length; i < m; i ++) {
                        // check for left dependent interrogative tags
                        // Is interrogative tag
                        if (interrogative_tags.indexOf(sentence.tags[deps[i]]) > -1 &&
                            // AND no verb right before ("this is why" is not a question)
                            (sentence.tags[deps[i] - 1] || '').indexOf('VB') < 0) {
                            types.push(T_INTERROGATIVE);
                        }
                    }
                }
            }
        }

        if (governor > -1 && types.indexOf(T_INTERROGATIVE) === -1) {
            // Imperative
            if (sentence.tags[governor] === 'VB') {
                types.push(T_IMPERATIVE);
            }
        }

    });
}();

!function() {

    var dirty = cpd.dirty,
        polite = cpd.polite,
        emphasis_adverbs = cpd.emphasis,

        future_modals = ['wo', '\'ll', 'will'],

        // Recursive function that takes all the dependencies scores and
        // compute a token final score
        scoreDependencies = function (sentence, token) {
            var deps = token.deps.dependencies, i, l = deps.length, s = 0, t;
            // If no dependencies
            if (l === 0) {
                return;
            }

            // Loop over dependencies
            for (i = 0; i < l; i += 1) {
                t = sentence.tokens[deps[i]];
                // First recursively score their own dependencies
                scoreDependencies(sentence, t);
                // Add score
                s += t.profile.sentiment;
            }

            // Score is divided by the number of dependencies
            token.profile.sentiment += parseInt((s / l) * 100) / 100;
        };

    // Set profile
    detectors.after('s', 'sentiment', function(sentence, index, sentences) {
        var i,
            l = sentence.length,
            profile,
            score = 0,
            token,
            pos,
            norm,
            emphasis = 1,
            token_score = 0,
            amplitude = 0,
            local_emphasis = 0,
            politeness = 0,
            isPolite,
            dirtiness = 0,
            isDirty,
            min = 0,
            max = 0,
            gov = sentence.governor,
            p = sentence.profile;

        // First take in account negation for token scoring
        for (i = 0; i < l; i ++) {
            profile = sentence.tokens[i].profile;
            pos = sentence.tokens[i].pos;
            norm = sentence.tokens[i].norm;
            isDirty = dirty.indexOf(norm) > -1;
            isPolite = polite.indexOf(norm) > -1;

            if (isDirty) {
                dirtiness ++;
            } else if (isPolite) {
                politeness ++;
            }

            // Handles negation, update token profile with according sentiment score
            if (profile.negated && pos !== '.' && pos !== 'EM') {
                // If negative but dirty word, doesn't invert score
                if (isDirty) {
                    profile.sentiment = profile.sentiment / 2;
                // Normal negation: score inverted and reduced
                } else {
                    profile.sentiment = -profile.sentiment / 2;
                }
            }
        }


        // Main tense + experimental dependency scoring
        if (gov > -1) {
            token = sentence.tokens[gov];
            // Experimental recursive dependency scoring
            scoreDependencies(sentence, token);

            // Main tense detection
            pos = token.pos;
            if (token.attr.is_verb) {
                p.main_tense = (pos === 'VBD' ? 'past' : 'present');
            } else if (pos === 'MD' && future_modals.indexOf(token.norm) > -1) {
                p.main_tense = 'future';
            }
        }

        // Sentiment analysis calculation

        // If moslty uppercased, give an emphasis bonus
        if (sentence.stats.p_upper > 70) {
            emphasis = 1.2;
        }

        // Loop on tokens
        // and set emphasis
        // and initial score
        for (i = 0; i < l; i ++) {
            profile = sentence.tokens[i].profile;
            pos = sentence.tokens[i].pos;
            norm = sentence.tokens[i].norm;

            // Get token base emphasis and multiply it with global emphasis
            emphasis *= profile.emphasis;

            // Check if token is a local emphasis
            // Note: local emphasis is NOT used by the final sentence sentiment score,
            // but only to compute the score of individuals tokens.
            if (pos === 'JJS' || (pos === 'RB' && emphasis_adverbs.indexOf(norm) > -1)) {
                if (profile.negated) {
                    local_emphasis += 2;
                } else {
                    local_emphasis += 5;
                }
            }


            // Get the score using local emphasis
            token_score = profile.sentiment * (1 + (local_emphasis / 10));
            score += token_score;

            // Keep track of token min/max scores for amplitude
            if (token_score > max) {
                max = token_score;
            } else if (token_score < min) {
                min = token_score;
            }

            // Update token emphasis using local emphasis for user consumption
            profile.emphasis *= 1 + (local_emphasis / 10);

            // Update local emphasis
            if (local_emphasis > 0 && ['DT', 'POS', 'IN'].indexOf(pos) === -1) {
                local_emphasis --;
            }
        }

        if (l < 5) {
            l *= 2;
        } else if (l > 10) {
            l /= 2;
        }

        amplitude = (max + (-min)) / l;
        score *= emphasis;
        score /= l;
        if (p.types.indexOf(T_INTERROGATIVE) > -1) {
            score /= 2;
        }
        p.sentiment = score;
        p.emphasis = emphasis;
        p.amplitude = amplitude;
        p.dirtiness = dirtiness / l;
        p.politeness = politeness / l;
        if (Math.abs(amplitude) > 0.5 && Math.abs(score) < 0.5 && Math.abs(amplitude) > Math.abs(score)) {
            p.label = 'mixed';
        } else if (score <= config.profile.negative_threshold) {
            p.label = 'negative';
        } else if (score >= config.profile.positive_threshold) {
            p.label = 'positive';
        } else if (amplitude >= config.profile.amplitude_threshold) {
            p.label = 'mixed';
        }
    });
}();
!function() {

    var approval_tokens = Object.keys(cpd.approval),
        refusal_tokens = Object.keys(cpd.refusal);

    // Second pass for type of sentence detection:
    // - Refusal
    // - Approval
    //
    // Uses sentiment detection in some specific cases,
    // thus why it runs after sentiment analysis detector.
    detectors.after('s', 'type', function(sentence, index) {
        var token = sentence.tokens[0],
            token2,
            i, l,
            profile = sentence.profile,
            governor = sentence.governor > -1 ? sentence.tokens[sentence.governor] : null,
            arr = !!governor ? governor.deps.dependencies : null,
            words_count = sentence.stats.words,
            types = profile.types;

        // Any interrogative form can't be
        // a refusal nor an approval
        if (types.indexOf(T_INTERROGATIVE) > -1) {
            return;
        }

        // Refusal
        // First token is direct refusal token
        if (refusal_tokens.indexOf(token.norm) > -1) {
            types.push(T_REFUSAL);
        // One word sentence containing only one negative adjective
        } else if (words_count === 1 && token.pos === 'JJ' && profile.sentiment < 0) {
            types.push(T_REFUSAL);
        // Let's try some cases for short sentences, depends on dependency parsing
        } else if (governor) {

            // Governor is refusal token
            if (refusal_tokens.indexOf(governor.norm) > -1) {
                types.push(T_REFUSAL);
            // Imperative and action verbs
            } else if (types.indexOf(T_IMPERATIVE) > -1 && cpd.approval_verbs.indexOf(governor.norm) > -1 && governor.profile.negated) {
                types.push(T_REFUSAL);
            // Negative governor dependent interjections
            } else if (governor.pos === 'UH') {
                for (i = 0, l = arr.length; i < l; i += 1) {
                    token2 = sentence.tokens[arr[i]];
                    if ((token2.pos === 'UH' || token2.pos === 'RB') && refusal_tokens.indexOf(token2.norm) > -1) {
                        types.push(T_REFUSAL);
                    }
                }
            }
        }

        if (types.indexOf(T_REFUSAL) > -1) {
            return;
        }

        // Approval
        // First token is direct approval token
        if (approval_tokens.indexOf(token.norm) > -1) {
            types.push(T_APPROVAL);
        // One word sentence containing only one positive adjective
        } else if (words_count === 1 && token.pos === 'JJ' && profile.sentiment > 0) {
            types.push(T_APPROVAL);
        // Let's try some cases for short sentences, depends on dependency parsing
        } else if (!!governor && words_count <= 3) {

            // Governor is approval token
            if (approval_tokens.indexOf(governor.norm) > -1) {
                types.push(T_APPROVAL);
            // Imperative and action verbs
            } else if (types.indexOf(T_IMPERATIVE) > -1 && cpd.approval_verbs.indexOf(governor.norm) > -1) {
                types.push(T_APPROVAL);
            // Positive governor dependent interjections
            } else if (governor.pos === 'UH') {
                for (i = 0; i < l; i += 1) {
                    token2 = sentence.tokens[arr[i]];
                    if (token2.pos === 'UH' && approval_tokens.indexOf(token2.norm) > -1) {
                        types.push(T_APPROVAL);
                    }
                }
            }
        }
    });

}();
!function() {

    var floatChar = cpd.floatChar,
        thousandChar = cpd.thousandChar,

        reHasNumericChar = /[0-9]/,
        reInteger = /^-?[0-9]+$/,
        reFloat = new RegExp('^-?[0-9]*\\' + floatChar + '[0-9]+$'),
        reThousands = new RegExp('^-?[0-9]+([\\' + thousandChar + '][0-9]+){1,}$'),
        // following regexp also used in lexer. share it?
        reThousandsFloat = new RegExp('^-?[0-9]+([\\' + thousandChar + '][0-9]+){1,}(\\' + floatChar + '[0-9]+)$'),
        replaceThousands = new RegExp('\\' + thousandChar, 'g'),

        numWords = cpd.numbers,
        multipliers = cpd.multipliers,

        getSingleValue = function(token) {
            var norm = token.norm;

            // If value has at least one numeric char
            if (norm.match(reHasNumericChar)) {
                if (norm.match(reInteger)) {
                    return parseInt(norm, 10);
                }
                if (norm.match(reFloat)) {
                    return parseFloat(norm);
                }
                if (norm.match(reThousandsFloat)) {
                    return parseFloat(norm.replace(replaceThousands, ''));
                }
                if (norm.match(reThousands)) {
                    return parseInt(norm.replace(replaceThousands, ''), 10);
                }
            }

            norm = token.attr.singular;
            if (numWords.hasOwnProperty(norm)) {
                return numWords[norm];
            }

            return null;
        },

        getSectionValue = function(sentence, section) {
            var token,
                tokens = section[2],
                i,
                l = section[1],
                value = 0,
                single;

            // Only one token for this section
            if (section[1] === 1) {
                token = tokens[0];
                return getSingleValue(token);
            // Many tokens, loop and attempt to solve
            } else {
                for (i = 0; i < l; i += 1) {
                    token = tokens[i];
                    single = getSingleValue(token);
                    if (single === null) {
                        return null;
                    }

                    if (multipliers.indexOf(token.attr.singular) > -1) {
                        value *= single;
                    } else {
                        value += single;
                    }
                }

                return value;
            }

            return null;
        },

        applySectionValue = function(section, value) {
            var tokens = section[2], i, l = tokens.length;
            for (i = 0; i < l; i += 1) {
                tokens[i].attr.value = value;
            }
        };

    // This detector goes accross all numeric sections
    // defined by numeric token detector and attempt
    // to set the final value of the numeric section
    detectors.before('s', 'numeric', function(sentence, index, sentences, context) {
        var sections = context.numericSections, i, l = sections.length, value;
        for (var i = 0; i < l; i += 1) {
            value = getSectionValue(sentence, sections[i]);
            if (value !== null) {
                applySectionValue(sections[i], value);
            }
        }

    });

}();
!function() {

    var lexicon = compendium.lexicon;

    // This detector runs on each token
    // and set basic properties (acronyms...)
    detectors.before('t', 'basics', function(token, index, sentence) {
        var raw = token.raw,
            norm = token.norm,
            stem = token.stem,
            pos = token.pos,
            sentiment = 0,
            emphasis = 1,
            singular,
            lc,
            l,
            i;

        lc = raw.toLowerCase();
        l = lc.length;

        // Test abbreviation
        if (l > 1 &&
            (raw.indexOf('.') === l - 1 && (i = cpd.abbrs.indexOf(lc.slice(0, l - 1))) > -1)) {
            token.attr.abbr = true;
            norm = cpd.abbrs_rplt[i];
        // Test acronym
        } else if (raw.match(/^([a-z]{1}\.)+/gi)) {
            token.attr.acronym = true;
        // Test synonym
        } else {
            norm = compendium.synonym(norm);
        }

        // Emphasis
        if (pos === '.') {
            i = raw[0];
            if (i === '!' || i === '?') {
                emphasis = raw.length > 1 ? 2 : i === '?' ? 1 : 1.5;
                if (raw.length > 1) {
                    norm = raw[0];
                }
            } else if (i === '.' && raw[1] === '.') {
                emphasis = 1.2;
                norm = '...';
            }
        } else if (pos === 'EM') {
            emphasis = 1.2;
        } else if (pos === 'UH') {
            emphasis = 1.1;

        // Verbs infinitive forms
        } else if (pos.indexOf('VB') === 0) {
            token.attr.infinitive = inflector.infinitive(norm);

        // Singularization
        } else if (pos === 'NNS' || pos === 'CD') {
           singular = inflector.singularize(norm);
           token.attr.singular = singular;
        } else if (pos === 'NN') {
           token.attr.singular = norm;
        }

        // Sentiment score
        // Only if not NNP or NNPS ("Dick Cheney" is not about a dick)
        // also not if preposition ("it's like I can" - like is IN, no sentiment attached)
        if (pos !== 'NNP' && pos !== 'NNPS' && pos !== 'IN') {
            // Get one from lexicon
            if (compendium.lexicon.hasOwnProperty(norm)) {
                i = compendium.lexicon[norm];
                if (!i.condition || token.pos === i.condition) {
                    sentiment = i.sentiment;
                }
            // If not found, test singular
            } else if (pos === 'NNS' && compendium.lexicon.hasOwnProperty(singular)) {
                i = compendium.lexicon[singular];
                if (!i.condition || pos === i.condition) {
                    sentiment = i.sentiment / 2;
                }
            // If not found, test stem
            } else if (compendium.lexicon.hasOwnProperty(stem)) {
                i = compendium.lexicon[stem];
                if (!i.condition || pos === i.condition) {
                    sentiment = i.sentiment / 2;
                }
            // If not found, check polite/dirty words
            } else if (cpd.dirty.indexOf(norm) > -1) {
                sentiment = -2;
            } else if (cpd.polite.indexOf(norm) > -1) {
                sentiment = 1;
            }
        }

        token.profile.sentiment = sentiment;
        token.profile.emphasis = emphasis;
        token.norm = norm;
    });
}();
!function() {

    var pos_breakpoints = [',', ':', ';', '('],
        raw_breakpoints = ['-', '—', '/'];

	// Flag breakpoints at the token level
    detectors.before('t', 'breakpoint', function(token, index, sentence) {
        var raw = token.raw,
            pos = token.pos;

        // Simple breakpoints
        if (pos_breakpoints.indexOf(pos) > -1 || raw_breakpoints.indexOf(raw) > -1) {
            token.profile.breakpoint = true;
            sentence.stats.breakpoints ++;
        }
    });
}();
!function() {


    // Flag entities at the token level
    // Reuse same regexps than lexer, so for further optimisation
    // lexer should return the result of the detection along with the tokens
    // and this detector would just create entities objects and link them
    // to the tokens.
    detectors.before('t', 'entities', function(token, index, sentence) {
        var regexps = compendium.lexer.regexps, k, entity,
            raw = ' ' + token.norm + ' ',
            norm,
            i,
            l,
            pl;

        for (k in regexps) {
            if (regexps.hasOwnProperty(k) && raw.match(regexps[k])) {
                entity = factory.entity(token, index, k);
                token.attr.entity = sentence.entities.push(entity) - 1;
                if (entity.type === 'username' || k === 'composite') {
                    token.pos = 'NNP';
                    sentence.tags[index] = 'NNP';
                }

                // Correct sentence confidence
                sentence.stats.confidence += 1 / sentence.length;

                // Let's be kind and normalize this journo thingie
                if (k === 'pl') {
                    entity.type = 'political_affiliation';
                    norm = token.norm.split('-');
                    l = norm[1].length;
                    if (norm[0] === 'd') {
                        entity.meta.party = 'democrat';
                    } else {
                        entity.meta.party = 'republican';
                    }

                    if (norm[1][l - 1] === '.') {
                        i = cpd.abbrs.indexOf(norm[1].slice(0, l-1));
                    } else {
                        i = cpd.abbrs.indexOf(norm[1]);
                    }

                    if (i > -1) {
                        norm[1] = cpd.abbrs_rplt[i];
                    }
                    token.norm = entity.meta.party + ', ' + norm[1];
                }
            }
        }
    });
}();

!function() {

    var numbers = cpd.numbers;

    detectors.init(function(context) {
        context.numericSections = [];
        context.inNumericSection = false;
    });

    // This detector flags numeric values and build an array of
    // potential numeric values as sections
    //
    // Sections are arrays containing at following indices:
    // - 0: section start index
    // - 1: length of section
    // - 2: array of tokens references for all the section
    detectors.before('t', 'numeric', function(token, index, sentence, context) {
        var pos = token.pos,
            sections = context.numericSections;

        if (pos === 'CD' || (pos === 'NNS' && numbers.hasOwnProperty(token.attr.singular))) {
            if (!context.inNumericSection) {
                context.numericSections.push([index, 1, [token]]);
                context.inNumericSection = true;
            } else {
                sections[sections.length - 1][1] += 1;
                sections[sections.length - 1][2].push(token);
            }
        } else if (context.inNumericSection) {
            context.inNumericSection = false;
        }
    });
}();
!function() {

    // Punctuation PoS tags
    var puncs = [',', '.', ':', '"', '(', ')'];

    // Factory for analysis objects
    extend(factory, {
        entity: function(token, index, type) {
            return {
                raw: token.raw,
                norm: token.norm,
                fromIndex: index,
                toIndex: index,
                type: type || null,
                meta: {}
            };
        },
        sentence: function(str, language) {
            return {
                // Language
                language: language,
                // Time spent for sentence analysis
                // (bullshit because some detectors are not taken in account).
                time: 0,
                // Count of tokens in the sentence
                length: 0,
                // Governor of the sentence
                // (dependency parsing)
                governor: -1,
                // Raw sentence string
                raw: str,
                // Those statistics are used by the
                // detectors (entity detection, quality evalution)
                stats: {
                    // Number of words, e.g. tokens that are not emoticons or punctuation
                    words: 0,
                    // Confidence in PoS tagging
                    // - Can be used to spot foreign text as well as estimate text quality
                    confidence: 0,
                    // Percentage of foreign tokens accross all tokens
                    // - Can be used to spot foreign text
                    p_foreign: 0,
                    // Percentage of uppercased tokens
                    p_upper: 0,
                    // Percentage of tokens capitalized
                    // - Will allow to define headline sentence type
                    // - May disable entity recognition based on capitalization
                    p_cap: 0,
                    // Average token length
                    // - Can be used as indicator for text quality
                    avg_length: 0,
                    // Number of breakpoints
                    breakpoints: 0
                },
                // type: 'unknown',
                profile: {
                    label: 'neutral',
                    sentiment: 0,
                    emphasis: 1,
                    amplitude: 0,
                    politeness: 0,
                    dirtiness: 0,
                    // Types. A sentence can have many types:
                    // (e.g. `imperative` + `refusal` for `don't do it` or
                    // `foreign` + `interrogative` for `Suis-je le monstre?`)
                    // `imperative`,
                    // `declarative`,
                    // `approval`,
                    // `refusal`,
                    // `interrogative`,
                    // `headline`
                    // `foreign`
                    types: [],
                    main_tense: 'present'
                },
                has_negation: false,
                entities: [],
                deps: {
                    subjects: [],
                    objects: []
                },
                root: null,
                tokens: [],
                tags: []
            };
        },
        token: function(raw, norm, pos) {
            var tense = null,
                verb = pos.indexOf('VB') === 0;

            norm = norm.toLowerCase();

            if (pos === 'VBD' || pos === 'VBN') {
                tense = 'past';
            } else if (pos === 'VBG') {
                tense = 'gerund';
            } else {
                tense = 'present';
            }

            return {
                raw: raw,
                norm: norm,
                stem: compendium.stemmer(norm),
                pos: pos || '',
                profile: {
                    sentiment: 0,
                    emphasis: 1,
                    negated: false,
                    breakpoint: false
                },
                attr: {
                    // Numeric value
                    value: null,
                    acronym: false,
                    abbr: false,
                    is_verb: verb,
                    tense: tense,
                    infinitive: null,
                    is_noun: pos.indexOf('NN') === 0,
                    plural: null,
                    singular: null,
                    entity: -1,
                    is_punc: puncs.indexOf(pos) > -1
                },
                deps: {
                    master: null,
                    governor: false,
                    type: 'unknown',
                    dependencies: []
                }
            };
        },

        // Internal, used by PoS tagger to represent a tag probability
        tag: function(tag, confidence, norm) {
            return {
                tag: tag || 'NN',
                norm: norm,
                confidence: confidence || 0,
                blocked: false
            }
        }
    });
}();

!function() {

    var abbreviations = cpd.abbrs,

        split_sentence_regexp = /(\S.+?[….\?!\n])(?=\s+|$|")/g,

        abbrev_regexp = new RegExp("(^| |\\\(|\\\[|\{)(" + abbreviations.join("|") + ")[\.!\?] ?$", "i"),

        word_boundaries = ' !?()[]{}"\'`%•.…:;,$€£¥\\/+=\*_–',

        contractions = ['s', 'm', 't', 'll', 've', 'd', 'em', 're'],

        encoder = compendium.punycode.ucs2,

        floatChar = cpd.floatChar,

        thousandChar = cpd.thousandChar,

        // Use to determinate float parts
        cd = /^-?[0-9]+$/,
        acd = /^[0-9]+$/,
        cdf = new RegExp('^-?[0-9]+[\.,]$'),

        // Regexps that catcj complex floats,
        numbers = {
            complexFloat: '\\s(-?[0-9]+([\\' + thousandChar + '][0-9]+){1,}(\\' + floatChar + '[0-9]+))',
        },

        // Regexps that catch emoticons
        r_emots = {},

        i, l = cpd.emots.length, emot,

        isOnlyEmots = function(meta) {
            var i = 0, l = meta.length;
            for (i = 0; i < l; i += 1) {
                if (meta[i] === null || meta[i].group !== 'emoticon') {
                    return false;
                }
            }
            return true;
        },

        applyRegexps = function(restr, spotted, regexps, groupName) {
            var i, l, re, curr;

            for (i in regexps) {
                if (regexps.hasOwnProperty(i)) {
                    re = new RegExp(regexps[i], 'g');
                    while ((curr = re.exec(restr)) !== null) {
                        l = curr[0].length;
                        spotted[curr.index] = {
                            content: curr[1],
                            type: i,
                            group: groupName,
                            length: l - (l - curr[1].length)
                        }
                    }
                }
            }
        };

    // Add regexps for emoticons
    for (i = 0; i < l * 2; i += 2) {
        emot = cpd.emots[i / 2];
        r_emots['em_' + i] = '\\s(' + regexpEscape(emot) + '+)[^a-z]';
        if (!emot.match(/^[a-zA-Z]/)) {
            r_emots['em_' + (i + 1)] = '[a-zA-Z](' + regexpEscape(emot) + '+)[^a-z]';
        }
    }


    extend(compendium.lexer, {

        // Entities regexps
        regexps: {
            email: '\\s([^\\s]+@[^\\s]+(\\.[^\\s\\)\\]]+){1,})',
            composite: '\\s([a-zA-Z]&[a-zA-Z])',
            username: '\\s(@[a-zA-Z0-9_]+)',
            html_char: '\\s(&[a-zA-Z0-9]{2,4};)',
            hashtag: '\\s(#[a-zA-Z0-9_]+)',
            url: '\\s((https?|ftp):\\/\\/[\\-a-z0-9+&@#\\/%\\?=~_|!:,\\.;]*[\\-a-z0-9+&@#\\/%=~_|])',
            ip: '\\s(([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5]))\\s',
            // Political affiliation, english only
            pl: '\\s([rd]-([a-z]+\\.{0,1})+)'
        },

        // Consolidate sentences:
        // merge back sentences containing only emoticons
        consolidate: function(sentences, meta,  raws) {
            for (var i = 1, l = sentences.length; i < l; i += 1) {
                if (isOnlyEmots(meta[i])) {
                    sentences[i - 1] = sentences[i - 1].concat(sentences[i]);
                    raws[i - 1] += ' ' + raws[i];
                    sentences.splice(i, 1);
                    meta.splice(i, 1);
                    raws.splice(i, 1);
                    i -= 1;
                    l -= 1;
                }
            }
            return sentences;
        },

        // Parse into sentences
        sentences:  function(str) {
            // Split with base regexp
            var arr = str.split(split_sentence_regexp),
                i,
                l = arr.length,
                s,
                sentences = [];

            // Loop onto result
            // - merge forward when necessary
            // - cleanup
            for (i = 0; i < l; i ++) {
                s = arr[i].trim();

                // If an abreviation or acronym, merge forward
                if (s.match(abbrev_regexp) || s.match(/[ |\.][A-Za-z]\.?$/)) {
                    // If next token is not a letter
                    if (i < l - 1 && !arr[i + 1].match(/^[A-Za-z]\s/)) {
                        arr[i + 1] = s + ' ' + arr[i + 1].trim();
                    } else {
                        sentences.push(s);
                    }
                // If non empty string
                } else if (!!s) {
                    sentences.push(s);
                }
            }
            return sentences;
        },

        // UTF-8 compliant tokens splitter: parses sentence char by char after having applied general utility
        // regexps. Will seperate emojis into tokens.
        splitTokens: function(str) {
            var i,
                l = str.length,
                curr,
                // result
                res = [],
                // meta
                meta = [],
                metaObject = null,
                // trick for testing on simpler regexps
                restr = ' ' + str + ' ',
                // trick
                decoded,
                spotted = {},
                xpush = function(w) {
                    if (!w) {
                        return;
                    }
                    if (typeof w === 'object') {
                        metaObject = w;
                        w = w.content;
                    }
                    decoded = encoder.decode(w);
                    var i, l = decoded.length, curr = '';
                    for (i = 0; i < l; i ++) {
                        if (decoded[i] >= 0x1f5ff) {
                            if (!!curr) {
                                meta.push(metaObject);
                                res.push(curr);
                            }
                            meta.push({
                                group: 'emoticon'
                            });
                            res.push(encoder.encode([decoded[i]]));
                            curr = '';
                        } else {
                            curr += encoder.encode([decoded[i]]);
                        }
                    }
                    if (!!curr) {
                        meta.push(metaObject);
                        res.push(curr);
                    }
                },
                push = function(w1, w2) {
                    xpush(w1);
                    xpush(w2);
                    curr = '';
                };

            // First run some general regexps onto the texts and aggregate the indexes
            applyRegexps(restr, spotted, lexer.regexps, 'entity');
            applyRegexps(restr, spotted, r_emots, 'emoticon');
            applyRegexps(restr, spotted, numbers, 'number');

            for (curr = '', i = 0; i < l; i ++) {
                // If spotted by regexp, simply append result
                if (spotted.hasOwnProperty(i)) {
                    push(curr, spotted[i]);
                    i += spotted[i].length - 1;
                } else
                // TODO: benchmark if word_boundaries.indexOf more perf than str.match(regexp)
                if (word_boundaries.indexOf(str[i]) > -1) {
                    push(curr, str[i]);
                } else {
                    curr += str[i];
                }
            }

            push(curr);

            return {
                tokens: res,
                meta: meta
            }
        },

        // Parse each token
        tokens: function(sentence, language) {
            var split = lexer.splitTokens(sentence),
                arr = split.tokens,
                meta = split.meta,
                i,
                l = arr.length,
                tok,
                in_acronym = false,
                result = [],
                metaResult = [],
                previous = '',
                next = '',
                count = 0;

            // Loop onto result
            // - cleanup
            // - merge back when necessary:
            //      * Floats
            //      * Acronyms
            //      * Simmilar
            for (i = 0; i < l; i ++) {
                // Cleanup
                tok = arr[i].trim();

                if (!tok) {
                    in_acronym = false;
                    continue;
                }

                if (count > 0) {
                    previous = result[count - 1];
                } else {
                    previous = '';
                }

                if (i < l - 1) {
                    next = arr[i + 1];
                } else {
                    next = '';
                }

                // If dot in float or full float or thousands - multilingual
                if (((tok === '.' || tok === ',') && previous.match(cd) && next.match(acd)) ||
                    (tok.match(cd) && previous.match(cdf))) {
                    in_acronym = false;
                    result[count - 1] += tok;
                    continue;
                }

                // If abbreviation, merge back .
                // only if . is not last char of the sentence - multilingual (form compendium.compendium)
                if (tok === '.' && i < l - 1 && count > 0 && abbreviations.indexOf(previous.toLowerCase()) > -1) {
                    in_acronym = false;
                    result[count - 1] += tok;
                    continue;
                }

                // If a dot and in acronym, merge back - multilingual
                if (in_acronym && i < l -1 && tok.length === 1) {
                    result[count - 1] += tok;
                    continue;
                }

                // If any punc mark or not a letter - multilingual
                if (tok.match(/^\W+$/gi)) {
                    in_acronym = false;
                    // If same than previous one, merge back
                    if (tok === previous[previous.length - 1]) {
                        result[count - 1] += tok;
                        continue;
                    }
                // Else if single letter and in acronym, merge back - multilingual
                } else if (tok.match(/^[A-Za-z]{1}$/g) && i < l - 1 && next === '.') {
                    in_acronym = true;
                }

                if (!!tok) {
                    result.push(tok);
                    metaResult.push(meta[i]);
                    count ++;
                }
            }

            // Post process result before returning it
            return {
                result: lexer.postprocess(result, metaResult),
                meta: metaResult
            };
        },

        /**
         * Parse a string into a matrix of tokens per sentences.
         *
         * @memberOf compendium.lexer
         * @param  {String} str A string to be tokenized
         * @param  {String} language Language to be used
         * @param  {Boolean} sentenceOnly If `true`, won't lex tokens
         * @return {Array}      A matrix of tokens per sentences.
         */
        advanced: function(str, language, sentenceOnly) {
            var sentences = lexer.sentences(str), i, l = sentences.length, lexed, meta = [], raws = [];

            if (!!sentenceOnly) {
                return {
                    sentences: sentences,
                    raws: null,
                    meta: null
                };
            }

            for (i = 0; i < l; i ++) {
                raws.push(sentences[i]);
                lexed = lexer.tokens(sentences[i], language);
                meta[i] = lexed.meta;
                sentences[i] = lexed.result;
            }
            lexer.consolidate(sentences, meta, raws);
            return {
                raws: raws,
                sentences: sentences,
                meta: meta
            };
        },

        lex: function(str, language, sentenceOnly) {
            return lexer.advanced(str, language, sentenceOnly).sentences;
        }

    });


    /**
     * Parse a string into a matrix of tokens per sentences. Alias of {@link compendium.lexer.lex}.
     *
     * @function lex
     * @memberOf compendium
     * @param  {String} str A string to be tokenized
     * @return {Array}      A matrix of tokens per sentences.
     */
    compendium.lex = lexer.lex;

}();

!function() {

    var contractions = ['s', 'm', 't', 'll', 've', 'd', 'em', 're'];


    extend(compendium.lexer, {

        // Post process is a the sentence level, is given an array of tokens
        postprocess:  function(tokens, metas) {
            // Split with base regexp
            var i,
                l = tokens.length,
                result = [],
                tok,
                previous,
                next;

            for (i = 0; i < l; i += 1) {
                tok = tokens[i];
                previous = tokens[i - 1] || '';
                next = tokens[i + 1] || '';


                // If token is ' check for contraction.
                // If is contraction, merge forward
                if (tok === '\'' && contractions.indexOf(next) > -1) {
                    // If t, check for 'n' in previous
                    if (next === 't' && previous.lastIndexOf('n') === previous.length - 1) {
                        result[i - 1] = previous.slice(0, -1);
                        result.push('n' + tok + next);
                        i += 1;
                    } else {
                        result.push(tok + next);
                        i += 1;
                    }
                    continue;
                }

                // TODO: refactor
                // Special en tokens, should be handled more elegantly
                if (tok === 'cant') {
                    // Default case: add token
                    result.push('can', 'n\'t');
                    continue;
                }

                if (tok === 'cannot') {
                    // Default case: add token
                    result.push('can', 'not');
                    continue;
                }

                if (tok === 'gonna') {
                    // Default case: add token
                    result.push('gon', 'na');
                    continue;
                }

                result.push(tok);
            }

            return result;
        }
    });


}();
!function() {


    var apostrophes = ['\'', '’', 'ʼ'];

    extend(compendium.lexer, {

        // Post process is a the sentence level, is given an array of tokens
        //
        postprocess:  function(tokens) {
            // Split with base regexp
            var i,
                l = tokens.length,
                tok,
                result = [];

            for (i = 0; i < l; i += 1) {
                tok = tokens[i];

                // Handle + normalize apostrophes
                if (apostrophes.indexOf(tok) > -1) {
                    result[result.length - 1] += '\'';
                } else {
                    result.push(tok);
                }
            }

            return result;
        }
    });


}();


}(typeof exports === 'undefined'? this['compendium']={}: exports));