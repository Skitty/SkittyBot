'use strict';

class Janitor {

    constructor(ctx) {
        // Minutes
        this.every = 60;
        this.prune_after_hours = 3;
        this.temp = ctx.config.location.temp;
        this.system_database = ctx.system_database;
        this.logger = ctx.logger;
        this.last_clean = null;
        this.ID = null;
        this.fs = require('fs');
        this.path = require('path');

        process.nextTick(this.start.bind(this));
    }

    start() {
        if (!this.fs.existsSync(this.temp)) {
            this.logger.error(`JANITOR: NO TEMP DIRECTORY FOUND.`);
            return;
        }
        let time = this.every * 60000;
        if (time < 60000) {
            this.logger.warn(`Janitor function shouldn't run this often.`);
            this.every = 1;
            time = this.every * 60000;
        }
        this.ID = setInterval(this.check.bind(this), time);
        this.system_database.exec(`
            CREATE TABLE IF NOT EXISTS last_clean (
                id      INTEGER PRIMARY KEY AUTOINCREMENT,
                time    TEXT
            );
        `)
            .then(() => {
                this.system_database.get(`SELECT time FROM last_clean WHERE id = ?;`, 0).then(result => {
                    if (!result) {
                        this.logger.debug(`JANITOR: last_clean was null so creating entry!`);
                        this.system_database.run('INSERT INTO last_clean (id, time) VALUES (0, 0);');
                        process.nextTick(this.check.bind(this));
                        return;
                    }
                    this.last_clean = parseInt(result.time);
                    this.logger.debug(`JANITOR: last_clean was ${this.last_clean}!`);
                    process.nextTick(this.check.bind(this));
                });
            });
    }

    time_elapsed(last) {
        let time_elapsed = +new Date() - +new Date(last);
        return Math.floor(time_elapsed / 1000 / 60);
    }

    check() {

        let run;

        if (this.last_clean) {
            let elapsed = this.time_elapsed(this.last_clean);
            if (elapsed > this.every) run = +new Date();
        } else {
            run = +new Date();
        }

        if (run) {
            this.system_database.run(`UPDATE last_clean SET time = ? WHERE id = ?`, run, 0)
                .then(() => {
                    process.nextTick(this.clean.bind(this));
                });
        }
    }

    clean() {
        this.logger.debug(`JANITOR: executing clean event.`);
        let fs = this.fs;
        let path = this.path;
        fs.readdir(this.temp, (err, files) => {
            if (err) {
                this.logger.error(`JANITOR: clean fs.readdir raised an error.`);
                this.logger.error(err);
                return;
            }
            files
                .map(file => path.join(this.temp, file))
                .filter(file => fs.statSync(file).isFile())
                .forEach(file => {
                    fs.stat(file, (e, stats) => {
                        if (e) {
                            this.logger.error(`JANITOR: clean fs.stat raised an error.`);
                            this.logger.reportError(e);
                            return;
                        }
                        let hours = parseInt(this.time_elapsed(stats.mtimeMs) / 60);
                        if (hours > this.prune_after_hours) {
                            fs.unlink(file, er => {
                                if (er) {
                                    this.logger.error(`ERROR: UNABLE TO UNLINK FILE IN ${this.temp}`);
                                    this.logger.error(`FILE: ${file}`);
                                    this.logger.reportError(er);
                                } else {
                                    this.logger.info(`UNLINKED: ${file}`);
                                }
                            });
                        }
                    });
                });
        });
    }
}

module.exports = Janitor;
