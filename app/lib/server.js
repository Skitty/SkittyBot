'use strict';

let DEBUGMODE = process.env.BOT_DEBUGGING === 'true';

const saltRounds = 10;

const express = require('express');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const expressjwt = require('express-jwt');
const bcrypt = require('bcrypt');

const http = require('http');
const https = require('https');

const EventEmitter = require('events');
const path = require('path');
const fs = require('fs');

// Get secrets
var secret, sfile = path.join(__dirname, 'secret.json');
if (!fs.existsSync(sfile)) {
    secret = {
        secret: ([1e3] + -1e3 + -4e3 + -8e3).replace(/[018]/g, a => ((a ^ Math.random() * 16) >> a / 4).toString(16)),
        audience: 'SkittyBot-users',
        issuer: 'SkittyBot'
    };
    fs.writeFile(sfile, JSON.stringify(secret), err => {
        if (err) console.error(err);
    });
    console.warn(`Generated "${sfile}". It's recommended to set your own values in that file.`);
} else {
    try {
        secret = require(sfile);
    } catch (e) {
        throw new Error(`Cannot continue because secret.json is problematic. Delete or recreate this file: "${sfile}"`);
    }
}
const config = secret;

const domainkey = path.join(__dirname, '..', 'domain-key.txt');
const domaincert = path.join(__dirname, '..', 'domain-crt.txt');
var keyspresent = true;
{
    let dk = fs.existsSync(domainkey);
    let dc = fs.existsSync(domaincert);
    if (!dk || !dc) keyspresent = false;
}
const ENABLE_SSL = keyspresent;

var SSL_PORT = 443,
    HTTP_PORT = 80;

if (DEBUGMODE) {
    SSL_PORT = 5443;
    HTTP_PORT = 5080;
}

// Validate access_token
const jwtCheck = expressjwt({
    secret: config.secret,
    audience: config.audience,
    issuer: config.issuer
});

// Check for scope
function requireScope(scope) {
    return (req, res, next) => {
        var has_scopes = req.user.scope === scope;
        if (!has_scopes) {
            res.sendStatus(401);
            return;
        }
        next();
    };
}

const ipbl = new Map();

class SERVER_API extends EventEmitter {
    constructor(ctx) {

        super();

        this.bot = ctx;

        this.http_start_msg = null;
        this.https_start_msg = null;
        this.listening = false;
        this.app = express();

        this.app.use('/api', jwtCheck, requireScope('skittybot_manager'));
        // Note: note, never exclude these next variables. Server stops working completely.
        this.app.use((err, req, res, next) => {
            if (err.name === 'UnauthorizedError') {
                res.status(403).send();
                return false;
            }
            return next();
        });

        this.http_server = http.Server(this.app);

        if (ENABLE_SSL) {
            this.https_server = https.createServer({
                key: fs.readFileSync(path.join(__dirname, '..', 'domain-key.txt')),
                cert: fs.readFileSync(path.join(__dirname, '..', 'domain-crt.txt'))
            }, this.app);
        }

        this.http_server.on('listening', () => {
            if (this.http_start_msg) {
                this.bot.logger.log(this.http_start_msg);
            } else {
                this.bot.logger.log(`Server listening.`);
            }
            this.http_listening = true;
        });

        if (ENABLE_SSL) {
            this.https_server.on('listening', () => {
                if (this.https_start_msg) {
                    this.bot.logger.log(this.https_start_msg);
                } else {
                    this.bot.logger.log(`Server listening.`);
                }
                this.https_listening = true;
            });
        }

        this.app.disable('x-powered-by');
        this.app.use((req, res, next) => {
            if (ipbl.get(req.connection.remoteAddress)) {
                res.status(403).end();
                return false;
            }
            res.header('X-Powered-By', `SkittyBot`);
            return next();
        });

        this.app.use(bodyParser.urlencoded({ extended: false }));
        this.app.use(bodyParser.json());

        this.app.options('/*', (req, res) => {
            res.header('Access-Control-Allow-Origin', '*');
            res.header('Access-Control-Allow-Methods', 'GET,POST,HEAD,OPTIONS');
            res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
            res.sendStatus(200);
        });

        this.app.use((req, res, next) => {
            res.header('Access-Control-Allow-Origin', '*');
            res.header('Access-Control-Allow-Methods', 'GET,POST,HEAD,OPTIONS');
            res.header('Access-Control-Allow-Headers', 'Content-Type');
            return next();
        });

        this.app.use((req, res, next) => {
            if ((this.bot.config.bot_url && req.hostname !== this.bot.config.bot_url) &&
                (req.hostname !== 'localhost' || !req.headers['user-agent'])) {
                res.status(403).end();
                // optionally block the address
                // ipbl.set(req.connection.remoteAddress, true);
            } else {
                if (!this.bot.config.readystate) return next();
                let ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
                if (ip.substr(0, 7) === '::ffff:') ip = ip.substr(7);
                console.log('>%s %s [URL: %s] [UA: %s]',
                    req.method,
                    ip,
                    req.url,
                    req.headers['user-agent']
                );
                return next();
            }

            return false;
        });

        this.app.get('/robots.txt', (req, res) => {
            res.type('text/plain');
            res.end('User-agent: *\nDisallow: /');
        });

    }

    createIdToken(user) {
        return jwt.sign(user, config.secret, { expiresIn: 60 * 60 * 5 });
    }

    createAccessToken() {
        return jwt.sign({
            iss: config.issuer,
            aud: config.audience,
            exp: Math.floor(Date.now() / 1000) + (60 * 60),
            scope: 'skittybot_manager',
            sub: 'lalaland|gonto',
            jti: this.genJti(),
            alg: 'HS256'
        }, config.secret);
    }

    // Generate Unique Identifier for the access token
    genJti() {
        let jti = '';
        let possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (let i = 0; i < 16; i++) {
            jti += possible.charAt(Math.floor(Math.random() * possible.length));
        }

        return jti;
    }

    static(location) {
        this.app.use(express.static(location));
    }

    get(rout, callback) {
        this.app.get(rout, callback);
    }

    post(rout, callback) {
        this.app.post(rout, callback);
    }

    start_https(port = SSL_PORT, msg = 'https server started.') {
        this.https_start_msg = msg || null;
        if (!this.https_listening) this.https_server.listen(port);
    }

    start_http(port = HTTP_PORT, msg = 'http server started.') {
        this.http_start_msg = msg || null;
        if (!this.http_listening) this.http_server.listen(port);
    }

    stop() {
        if (this.http_listening) this.http_server.close();
        if (this.https_listening) {
            this.https_server.close();
            this.logger.log(`Server has stopped listening.`);
        }
    }

}

class CONFIG_API extends SERVER_API {
    constructor(ctx) {
        super(ctx);
        this.bot = ctx;
        this.manager_exists = false;
    }

    validate(str, exp) {
        let res = str.match(exp);
        if (!Array.isArray(res)) return false;
        if (res[0] !== str) return false;
        return true;
    }

    createServer() {

        super.static(this.bot.config.location.config_view);

        super.get('/commands.json', (req, res) => {
            if (!this.bot.config.readystate) {
                res.status(500).end(`Configuration required.`);
                return;
            }
            let keys = Array.from(this.bot.plugins.list.keys());
            let plugins = [];
            keys.forEach(key => plugins.push(this.bot.plugins.list.get(key)));

            try {
                plugins = plugins.filter(plugin => {
                    delete plugin.path;
                    if (!plugin.plugin_title || !plugin.description) return false;
                    if (!Array.isArray(plugin.commands)) return false;
                    // If the command has any flags that suggest it does not need to be shown to the world here then keep it out of sight
                    plugin.commands = plugin.commands.filter(command => !((command.owner || command.private || command.hidden) === true));
                    if (!plugin.enabled ||
                        plugin.owner ||
                        plugin.private ||
                        !plugin.commands.length
                    ) return false;
                    return true;
                });
            } catch (e) {
                SkittyBot.logger.reportError(e);
                res.status(200).end(JSON.stringify([{
                    plugin_title: 'Internal server error!',
                    description: 'Please inform my developer that the "/commands.json" gateway has suffered an error.',
                    commands: [{
                        command: 'NOTHING TO SHOW',
                        description: 'NOTHING TO SHOW'
                    }]
                }]));
                return;
            }
            res.status(200).end(JSON.stringify(plugins));
        });

        super.get('/info', (req, res) => {
            let home_server = `https://discord.gg/${this.bot.config.support_server_token}`;
            if (this.bot.config.custom_invite) home_server = this.bot.config.custom_invite;
            let info;
            if (this.bot.config.readystate && this.bot.client.user) {
                info = {
                    support_server: home_server,
                    bot_invite: `https://discordapp.com/oauth2/authorize?permissions=${this.bot.config.permissions_token}&scope=bot&client_id=${this.bot.client.user.id}`,
                    bot_name: this.bot.config.bot_name
                };
            } else {
                // Just use my current setup as of 1/23/2019
                info = {
                    support_server: 'https://discord.gg/VwGEM2s',
                    bot_invite: `https://discordapp.com/oauth2/authorize?permissions=379968&scope=bot&client_id=411787757455671296`,
                    bot_name: 'SkittyBot'
                };
            }
            let data = JSON.stringify(info);
            res.status(200).end(data);
        });

        super.get('/api/plugins.json', (req, res) => {
            if (!this.bot.config.readystate) {
                res.status(500).end(`Configuration required.`);
                return;
            }
            let keys = Array.from(this.bot.plugins.list.keys());
            let plugins = [];
            keys.forEach(key => plugins.push(this.bot.plugins.list.get(key)));
            res.status(200).end(JSON.stringify(plugins));
        });

        super.get('/api/config', (req, res) => {
            let cfg = JSON.stringify(this.bot.config);
            res.status(200).end(cfg);
        });

        super.post('/api/plugin/set-state', async (req, res) => {
            if (!this.bot.config.readystate) {
                res.status(500).end(`Configuration required.`);
                return;
            }
            let reply = {};
            if (typeof req.body === 'object' && req.body.plugin_title) {
                let r = await SkittyBot.plugins.controller(req.body);
                if (r) {
                    reply.message = `Plugin ${req.body.enabled ? 'loaded' : 'unloaded'} and ${req.body.enabled ? 'enabled' : 'disabled'}.`;
                    res.status(200).end(JSON.stringify(reply));
                } else {
                    reply.message = 'Error with plugin!';
                    res.status(400).end(JSON.stringify(reply));
                }
                return;
            }
            res.status(400).end('No.');
        });

        super.post('/api/plugin/reload', async (req, res) => {
            if (!this.bot.config.readystate) {
                res.status(500).end(`Configuration required.`);
                return;
            }
            let reply = {};
            if (typeof req.body === 'object' && req.body.plugin_title) {
                let r = await SkittyBot.plugins.reloadPlugin(req.body);
                if (r) {
                    reply.message = 'Plugin reloaded!';
                    res.status(200).end(JSON.stringify(reply));
                } else {
                    reply.message = 'Error while reloading.';
                    res.status(400).end(JSON.stringify(reply));
                }
                return;
            }
            res.status(400).end('No.');
        });

        super.post('/api/config', (req, res) => {
            let v = req.body;
            if (!this.validate(v.bot_name, /[a-zA-Z0-9]+/) ||
                !this.validate(v.bot_prefix, /[0-9a-zA-Z\-[\]/{}()*+?.^$|`=~,_!#%&"<>:;']+/) ||
                !this.validate(v.bot_token, /[a-zA-Z0-9._-]+/) ||
                (v.dbl_token && !this.validate(v.dbl_token, /[a-zA-Z0-9._-]+/)) ||
                !this.validate(v.owner_id, /[0-9]{7,30}/)) {
                res.status(200)
                    .end(JSON.stringify({
                        error: 1,
                        message: `The criteria you provided did not match what is accepted. Try again.`
                    }));
                return;
            }

            let cfg = {
                $bot_url: v.bot_url,
                $bot_name: v.bot_name,
                $owner_id: v.owner_id,
                $log_level: v.log_level,
                $bot_token: v.bot_token,
                $dbl_token: v.dbl_token,
                $bot_prefix: v.bot_prefix,
                $file_logging: v.file_logging,
                $custom_invite: v.custom_invite,
                $permissions_token: v.permissions_token,
                $support_server_token: v.support_server_token
            };

            this.bot.system_database.run(`
                UPDATE config SET 
                    bot_url              = $bot_url,
                    bot_name             = $bot_name,
                    owner_id             = $owner_id,
                    log_level            = $log_level,
                    bot_token            = $bot_token,
                    dbl_token            = $dbl_token,
                    bot_prefix           = $bot_prefix,
                    file_logging         = $file_logging,
                    custom_invite        = $custom_invite,
                    permissions_token    = $permissions_token,
                    support_server_token = $support_server_token
                WHERE id = 0;
            `, cfg);

            res.status(200)
                .end(JSON.stringify({
                    error: 0,
                    message: `My settings have been updated.`
                }));

            this.emit('settings_update');

        });

        super.post('/auth', (req, res) => {

            let fail = `Error: authentication failure. Check your inputs and try again.`;
            let username = req.body.username;
            let userpassword = req.body.password;

            if (!username || !userpassword) {
                res.status(400).send(`Insufficient parameters.`);
            } else {

                this.checkForUser(username)
                    .then(details => {
                        if (!details && !(typeof details === 'object')) {
                            res.status(401).send(fail);
                        } else {

                            bcrypt.compare(userpassword, details.hash).then(match => {
                                if (match === true) {
                                    res.status(201).send({
                                        id_token: super.createIdToken({ id: details.id, name: details.name }),
                                        access_token: super.createAccessToken()
                                    });
                                } else {
                                    res.status(401).send(fail);
                                }
                            });

                        }
                    });

            }

        });

        super.get('*', (req, res) => {
            res.sendFile(path.join(this.bot.config.location.config_view, 'index.html'));
        });

        let URL;

        if (ENABLE_SSL) {
            URL = DEBUGMODE ? `localhost:${SSL_PORT}/botsys` : `localhost/botsys`;
            super.start_https(SSL_PORT, `Admin panel running at https://${URL}`);
        }

        URL = DEBUGMODE ? `localhost:${HTTP_PORT}/botsys` : `localhost/botsys`;
        super.start_http(HTTP_PORT, `Admin panel running at http://${URL}`);

    }

    registerAdmin(usr, pswd) {
        return new Promise((res, rej) => {
            bcrypt.hash(pswd, saltRounds)
                .then(async hash => {
                    await this.bot.system_database.run(`INSERT INTO auth (user_name, pswd, manager) VALUES (?, ?, ?);`, usr, hash, 1);
                    res();
                })
                .catch(e => {
                    rej(e);
                });
        });
    }

    checkForUser(name) {

        return new Promise(resolve => {
            this.bot.system_database.get('SELECT * FROM auth WHERE user_name = ?;', name)
                .catch(console.error)
                .then(row => {
                    if (row !== undefined) {
                        resolve({
                            hash: row.pswd,
                            id: row.id,
                            name: row.name
                        });
                    } else {
                        resolve(false);
                    }
                });
        });

    }

    async checkForManager() {
        let man = await this.bot.system_database.get('SELECT * FROM auth WHERE manager = ?;', 1).catch(this.error);
        if (man) this.manager_exists = true;
    }

    async start() {

        this.createServer();
        await this.checkForManager();

        if (!this.manager_exists) {
            // only one can exist...
            super.post('/createmanager', (req, res) => {
                if (req.hostname !== 'localhost') {
                    res.status(403).end();
                    return;
                }

                let username = req.body.username;
                let userpassword = req.body.password;

                if (!username || username.length < 3) {
                    res.status(400).send(`Your username must be 3 or more characters.`);
                    return;
                }

                if (!userpassword || userpassword.length < 6) {
                    res.status(400).send(`Your password must be 6 or more characters.`);
                    return;
                }

                this.registerAdmin(username, userpassword)
                    .then(() => {
                        this.checkForUser(username)
                            .then(details => {
                                let fail = `Error: authentication failure. Check your inputs and try again.`;
                                if (!details && !(typeof details === 'object')) {
                                    res.status(401).send(fail);
                                } else {

                                    bcrypt.compare(userpassword, details.hash).then(match => {
                                        if (match === true) {
                                            res.status(201).send({
                                                id_token: super.createIdToken({ id: details.id, name: details.name }),
                                                access_token: super.createAccessToken()
                                            });
                                            this.bot.logger.info(`Your manager account was create.`);
                                            this.bot.logger.info(`Configure your bot token, prefix, owner id and bot name in order to start the bot.`);
                                        } else {
                                            res.status(401).send(fail);
                                        }
                                    });

                                }
                            });
                    });

            });

            this.bot.logger.error(`You must register a manager user name and password, then restart the bot.`);
            if (ENABLE_SSL) {
                this.bot.logger.error(`Register your manager account at https://localhost:${SSL_PORT}/botsys/sb-login`);
            } else {
                this.bot.logger.error(`Register your manager account at http://localhost:${HTTP_PORT}/botsys/sb-login`);
            }
        } else {
            // TODO: allow guild managers to login and control settings for their guilds
            super.post('/createmanager', (req, res) => {
                res.status(403).end(`You cannot create an account at this time.`);
            });
        }

    }

}

module.exports.config_api = context => new CONFIG_API(context);
module.exports.server_api = context => new SERVER_API(context);
