'use strict';

const DEBUGMODE = process.env.BOT_DEBUGGING === 'true';

const plugin_manager = require('./lib/plugin_manager');
const command_queue = require('./lib/command_queue');
const processor = require('./lib/message_processor');
const guildEvents = require('./lib/guild_events');
const janitor = require('./lib/janitor');
const logger = require('./lib/logger');
const utils = require('./lib/utility');
const server = require('./lib/server');

const dis_codes = require('./assets/dcec.json');
const EventEmitter = require('events');
const Discord = require('discord.js');
const { Intents } = Discord;
const sqlite = require('sqlite');
const path = require('path');
const fs = require('fs');

class discord_client extends EventEmitter {

    constructor() {

        super();

        this.Discord = Discord;
        this.client = new Discord.Client(ClientOptions);
        this.event = Discord.Constants.Events;

        this.ticker_id = null;

        this.client.on(this.event.DISCONNECT, this.disconnect.bind(this));
        this.client.on(this.event.RECONNECTING, this.reconnect.bind(this));

        this.client.on(this.event.ERROR, console.error);
        this.client.on(this.event.WARN, console.warn);

    }

    stop_ticker() {
        if (this.ticker_id) {
            clearInterval(this.ticker_id);
            this.ticker_id = null;
        }
    }

    start_ticker() {
        this.stop_ticker();
        let msg = [];
        let index = 0;
        if (DEBUGMODE) {
            msg.push(`DEBUG MODE`);
        } else {
            msg.push(`@mention commands`, `@mention help`, 'h-hi~', 'yay!', ':3');
            msg.push(() => `in ${SkittyBot.client.guilds.cache.size} guilds`);
            msg.push(() => `with ${SkittyBot.client.users.cache.size} users`);
            msg.push(() => `in ${SkittyBot.client.channels.cache.size} channels`);
        }
        let alternate = () => {
            let status = msg[index++ % msg.length];
            SkittyBot.client.user.setActivity(status instanceof Function ? status() : status);
        };
        // set initial status
        alternate();
        this.ticker_id = setInterval(alternate, 15000);
    }

    disconnect(event) {
        this.stop_ticker();
        SkittyBot.logger.info(`Disconnected from discord.`);
        if (event) {
            if (event.code) {
                if (typeof dis_codes[event.code] === 'object') {
                    SkittyBot.logger.info(`${process.title} ${dis_codes[event.code].Description}: ${dis_codes[event.code].Explanation}`);
                }
            }
            if (event.reason) {
                SkittyBot.logger.info(`Discord: [Code: ${event.code}] [Reason: ${event.reason}]`);
            }
        }
    }

    reconnect() {
        SkittyBot.logger.info(`Reconnecting to discord.`);
        this.start_ticker();
    }

    login() {
        if (SkittyBot.config.bot_token) {
            SkittyBot.client.login(SkittyBot.config.bot_token).then(this.logged_in.bind(this));
        } else {
            SkittyBot.logger.error(`Unable to initiate. Reason: missing "bot_token"`);
        }
    }

    async logout() {
        SkittyBot.logger.debug(`Destroying client.`);
        this.stop_ticker();
        if (SkittyBot.client) await SkittyBot.client.destroy();
        if (SkittyBot.system_database) {
            SkittyBot.logger.debug(`Closing system_database.`);
            SkittyBot.system_database.close();
        }
    }

    logged_in() {
        SkittyBot.logger.log(`Login successful: I'm ${SkittyBot.client.user.tag}!`);
        // set client id to config just so that it is there...
        SkittyBot.config.client_id = SkittyBot.client.user.id;
        this.start_ticker();
        // SkittyBot.server.start_http();
    }

    async shut_down() {
        SkittyBot.logger.log(`Shutting down.`);
        SkittyBot.server.stop();
        if (SkittyBot.processor) {
            SkittyBot.logger.debug(`Stopping processor.`);
            SkittyBot.processor.shut_down();
        }
        await this.logout();
        SkittyBot.logger.debug(`All done.`);
        process.exit(0);
    }

}

class settings_manager extends EventEmitter {

    constructor() {

        super();

        this.botClient = null;
        this.config = null;
        this.utils = utils;

    }

    async systemDatabaseRoutine() {

        logger.log('Initiating settings manager.');

        this.system_database = await sqlite.open(path.join(location.database, 'system.db'));

        await utils.read(path.join(__dirname, 'system_schema.sql'))
            .then(async sql => {
                await this.system_database.exec(sql).catch(SkittyBot.logger.reportError);
            })
            .catch(err => {
                SkittyBot.logger.reportError(err);
            });

        await this.loadConfig();

        this.emit('settings_ready', this.config);

    }

    async reloadConfig() {
        await this.loadConfig();
        this.emit('settings_new', this.config);
    }

    async loadConfig() {

        SkittyBot.logger.verbose('Loading bot config and settings.');

        await this.system_database.get('SELECT * FROM config WHERE id = ?;', 0)
            .catch(console.error)
            .then(async row => {
                if (row !== undefined) {
                    this.createConfig(row);
                } else {
                    await this.system_database.run(`INSERT INTO config (id, bot_version) VALUES (?, ?);`, 0, 3);
                    let cfg = await this.system_database.get('SELECT * FROM config WHERE id = ?;', 0);
                    this.createConfig(cfg);
                }
            });

        await this.system_database.each('SELECT * FROM prefixes;', (err, row) => {
            if (err) {
                console.error(err);
                return;
            }
            SkittyBot.guild_prefix.set(row.guild_id, row.prefix);
        }).catch(console.error);

        await this.system_database.each('SELECT * FROM personal_prefixes;', (err, row) => {
            if (err) {
                console.error(err);
                return;
            }
            SkittyBot.user_prefix.set(row.user_id, row.prefix);
        }).catch(console.error);

        await this.system_database.each('SELECT * FROM blacklist_guild;', (err, row) => {
            if (err) {
                console.error(err);
                return;
            }
            SkittyBot.blacklist_guild.set(row.guild_id, parseInt(row.blacklisted));
        }).catch(console.error);

        await this.system_database.each('SELECT user_id, ring FROM administrators;', (err, row) => {
            if (err) {
                console.error(err);
                return;
            }
            SkittyBot.administrators.set(row.user_id, row.ring);
        }).catch(console.error);

    }

    createConfig(cfg) {
        let readystate = true;
        if (!cfg.bot_token || !cfg.owner_id) readystate = false;
        this.config = {
            bot_url: cfg.bot_url,
            bot_name: cfg.bot_name,
            owner_id: cfg.owner_id,
            log_level: cfg.log_level,
            bot_token: cfg.bot_token,
            dbl_token: cfg.dbl_token,
            bot_prefix: cfg.bot_prefix,
            bot_version: cfg.bot_version,
            file_logging: cfg.file_logging,
            custom_invite: cfg.custom_invite,
            ran_dep_concat: cfg.ran_dep_concat,
            permissions_token: cfg.permissions_token,
            support_server_token: cfg.support_server_token,
            ran_package_installer: cfg.ran_package_installer,
            last_dependency_check: cfg.last_dependency_check,
            dependencies_outdated: cfg.dependencies_outdated,
            location: location,
            readystate: readystate
        };
    }

}

class API {

    constructor() {

        // Array of people's IDs who are allowed to use owner commands
        // that do not explicitly require  author.id === owner_id
        this.administrators = new Map();

        // colored timestamped logging
        this.logger = logger;

        // utility functions
        this.utils = utils;

        // SQLite API
        this.db = sqlite;

        // The main bots database
        this.system_database = null;

        // configuration cache
        this.config = {};

        // custom guild prefixes
        this.guild_prefix = new Map();

        // custom user prefixes
        this.user_prefix = new Map();

        // blacklist commands globally
        this.global_command_blacklist = new Map();

        // blacklist commands in guilds
        this.guild_command_blacklist = new Map();

        // blacklist commands in channel
        this.channel_command_blacklist = new Map();

        // blacklist module globally
        this.global_module_blacklist = new Map();

        // blacklist module in guilds
        this.guild_module_blacklist = new Map();

        // blacklist module in guilds
        this.channel_module_blacklist = new Map();

        // enable private commands
        this.private_enable = new Map();

        // blacklisted guilds
        this.blacklist_guild = new Map();

        // blacklisted users
        this.blacklist_user = new Map();

        // guild user blacklist
        this.guild_blacklist_user = new Map();

        // channel user blacklist
        this.channel_blacklist_user = new Map();

        // should change: it's used to prevent multiple message watchers for same command
        this.global_activity = new Map();

        // plugin command cache
        this.commands = new Map();

        // maps aliases to a key in our command Map()
        this.command_alias = new Map();

        // custom configurable guild command aliases
        this.custom_commands = new Map();

        // Expensive function resource
        // this allows you to queue commands for execution.
        // This is useful for commands that take a while to finish.
        // Currently using 3, so no more than 3 expensive commands can be run concurrently
        // otherwise they get queued up and wait for previous commands to finish.
        this.queue = new command_queue(3);

    }

    shut_down() {
        this.client_manager.shut_down();
    }

    registerPlugin(config) {
        let file = utils.getCaller();
        this.plugins.registerConfiguration(file, config);
    }

    onCommand(callback) {
        return this.processor.commandCallback(callback);
    }

    onRootMessage(callback) {
        return this.processor.rootmsgCallback(callback);
    }

    onUserMessage(callback) {
        return this.processor.msgCallback(callback);
    }

    deleteCallback(id) {
        this.processor.deleteMsgCallback(id);
        this.processor.deleteRootCallback(id);
        this.processor.deleteCommandCallback(id);
    }

    onGuildJoin(callback) {
        return this.guildEvents.onGuildJoinRegisterCallback(callback);
    }

    onGuildLeave(callback) {
        return this.guildEvents.onGuildLeaveRegisterCallback(callback);
    }

    onGuildMemberAdd(callback) {
        return this.guildEvents.onGuildMemberAddCallback(callback);
    }

    onGuildMemberRemove(callback) {
        return this.guildEvents.onGuildMemberRemoveCallback(callback);
    }

    unregisterGuildCallback(id) {
        this.guildEvents.delete(id);
    }

    // easily get the prefix from anywhere...
    prefix(msg) {
        let prefix;
        if (msg.guild) {
            if (this.guild_prefix.has(msg.guild.id)) prefix = this.guild_prefix.get(msg.guild.id);
        }
        if (!prefix) {
            // show possible personal prefix
            if (this.user_prefix.has(msg.author.id)) prefix = this.user_prefix.get(msg.author.id);
        }
        if (!prefix) prefix = this.config.bot_prefix;
        // pad the prefix if it's a word/name so it looks neat
        if (prefix.length > 3 || /^[a-z0-9]+$/i.test(prefix)) return `${prefix} `;
        return prefix;
    }

    getcommand(name) {
        return this.getCommand(name);
    }

    // easily get command objects when needed
    // always returns the command object as defined in plugins
    getCommand(name) {
        if (typeof name !== 'string') return false;
        name = name.toLowerCase();
        return this.commands.get(this.command_alias.get(name) || name);
    }

    // Custom reaction collector that also includes reaction remove events because discord.js doesn't appear to provide a way to catch these events
    // SkittyBot.createReactionCollector(message, reaction => { /* */ }).end(reason => { /* */ })
    onReaction(msg, callback) {
        let stop = m => {
            clearTimeout(ID);
            this.client.off(this.event.RAW, filter);
            if (end) end(m);
        };
        let end, ID = setTimeout(stop, 1000 * 60 * 5);
        let filter = event => {
            if ((event.t === 'MESSAGE_REACTION_REMOVE' || event.t === 'MESSAGE_REACTION_ADD') &&
                event.d.message_id === msg.id &&
                event.d.user_id !== msg.author.id) {
                clearTimeout(ID);
                ID = setTimeout(stop, 1000 * 60 * 5);
                // eslint-disable-next-line callback-return
                callback(Object.assign(event.d.emoji, { user_id: event.d.user_id, stop: stop }));
            }
        };
        this.client.on(this.event.RAW, filter);
        return {
            stop: stop,
            end: cb => { end = cb; }
        };
    }

}

class skittybot extends API {

    constructor() {

        super();

        this.client_manager = new discord_client();

        this.client = this.client_manager.client;
        this.event = this.client_manager.Discord.Constants.Events;

        this.initiated = false;

        const setting_broker = new settings_manager();

        setting_broker.on('settings_new', settings => {
            this.initiated = true;
            this.config = settings;
            if (this.initiated) return;
            this.run();
        });

        setting_broker.on('settings_ready', settings => {

            this.config = settings;
            this.system_database = setting_broker.system_database;

            this.admin_panel = server.config_api(this);
            this.admin_panel.on('settings_update', () => {
                setting_broker.reloadConfig();
            });
            this.server = server.server_api(this);
            this.admin_panel.start();

            if (!settings.readystate) {
                this.logger.error(`Some configuration is required before the bot can continue.`);
                return;
            }

            this.initiated = true;
            this.run();

        });

        setting_broker.systemDatabaseRoutine();

    }

    run() {

        // Startup the bots temp directory cleaning function
        this.janitor = new janitor(this);

        // message handler
        this.processor = new processor();

        // guild join/leave handler
        this.guildEvents = new guildEvents();

        // plugin API
        this.plugins = new plugin_manager();

        /*
            function: this.plugins.updatePackageJSON();

            This will concat  all dependencies from all the
            bots  plugin  package.json  files into the main
            package.json.

            This is done in order to prevent installing the
            same module twice.

            In the future, there should be a routine which
            handles dependency  version conflicts, such as
            allowing  the  user  to  select  a  version to
            install to avoid duplicate modules.

        */

        this.plugins.updatePackageJSON();
        // Go ahead and load all the plugins
        this.plugins.loadPlugins();
        // Finally log-in to discord and start getting messages
        this.client_manager.login();
    }

}

let PATH_PLUGINS = path.join(__dirname, '..', 'plugins');
if (DEBUGMODE) PATH_PLUGINS = path.join(__dirname, '..', '..', 'plugins-dev');

if (!fs.existsSync(PATH_PLUGINS)) {
    logger.info(`Creating plugin development directory: ${PATH_PLUGINS}`);
    fs.mkdirSync(PATH_PLUGINS);
}

const location = Object.freeze({
    config_view: path.join(__dirname, 'views', 'botsys'),
    master_plugins: path.join(__dirname, '..', 'plugins', 'default'),
    private_plugins: path.join(PATH_PLUGINS, 'private'),
    public_plugins: path.join(PATH_PLUGINS, 'public'),
    database: path.join(__dirname, 'database'),
    logs: path.join(__dirname, '..', 'logs'),
    views: path.join(__dirname, 'views'),
    temp: path.join(__dirname, 'temp'),
    lib: path.join(__dirname, 'lib'),
    home: path.join(__dirname, '..')
});

for (let folder in location) {
    if (!fs.existsSync(location[folder])) {
        logger.info(`Creating missing bot directory: ${location[folder]}`);
        fs.mkdirSync(location[folder]);
    }
}

const localIntent = new Intents();
localIntent.add(Intents.FLAGS.GUILDS, Intents.FLAGS.GUILD_MESSAGES, Intents.FLAGS.GUILD_PRESENCES, Intents.FLAGS.GUILD_MEMBERS, Intents.FLAGS.DIRECT_MESSAGES, Intents.FLAGS.DIRECT_MESSAGE_REACTIONS);

const ClientOptions = {
    intents: localIntent,
    disabledEvents: [
        'CHANNEL_PINS_UPDATE',
        'VOICE_STATE_UPDATE',
        'TYPING_START',
        'VOICE_SERVER_UPDATE',
        'RELATIONSHIP_ADD',
        'RELATIONSHIP_REMOVE',
        'GUILD_BAN_ADD',
        'GUILD_BAN_REMOVE',
        'MESSAGE_DELETE_BULK',
        'MESSAGE_REACTION_REMOVE_ALL'
    ],
    messageCacheMaxSize: 10
};

Object.defineProperty(global, 'SkittyBot', {
    configurable: true,
    enumerable: false,
    value: new skittybot()
});
