CREATE TABLE IF NOT EXISTS prefixes (
    guild_id    TEXT NOT NULL,
    prefix      TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS personal_prefixes (
    user_id     TEXT NOT NULL,
    prefix      TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS administrators (
    user_id    TEXT UNIQUE NOT NULL,
    ring       INTEGER     NOT NULL
);

CREATE TABLE IF NOT EXISTS blacklist_guild (
    guild_id    TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS blacklist_user (
    user_id    TEXT NOT NULL,
    guild_id   TEXT,
    channel_id TEXT,
    isglobal   INTEGER
);

CREATE TABLE IF NOT EXISTS blacklist_commands (
    guild_id   TEXT,
    channel_id TEXT,
    command    TEXT NOT NULL,
    isglobal   INTEGER
);

CREATE TABLE IF NOT EXISTS blacklist_modules (
    guild_id   TEXT,
    channel_id TEXT,
    module     TEXT NOT NULL,
    isglobal   INTEGER
);

CREATE TABLE IF NOT EXISTS enable_private (
    guild_id TEXT NOT NULL,
    command  TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS custom_commands (
    guild_id TEXT NOT NULL,
    alias    TEXT NOT NULL,
    command  TEXT NOT NULL,
    args     TEXT
);

CREATE TABLE IF NOT EXISTS event_log_channels (
    channel_id TEXT UNIQUE NOT NULL
);

CREATE TABLE IF NOT EXISTS config (
    id INTEGER PRIMARY KEY,
    bot_url TEXT,
    bot_name TEXT,
    owner_id TEXT,
    log_level TEXT,
    bot_token TEXT,
    dbl_token TEXT,
    bot_prefix TEXT,
    bot_version TEXT,
    file_logging TEXT,
    custom_invite TEXT,
    ran_dep_concat INTEGER NOT NULL DEFAULT(0),
    permissions_token TEXT,
    support_server_token TEXT,
    ran_package_installer INTEGER NOT NULL DEFAULT(0),
    last_dependency_check INTEGER NOT NULL DEFAULT(0),
    dependencies_outdated INTEGER NOT NULL DEFAULT(0)
);

CREATE TABLE IF NOT EXISTS auth (
    user_name   TEXT UNIQUE PRIMARY KEY,
    pswd        TEXT,
    manager     INTEGER NOT NULL DEFAULT(0)
);
