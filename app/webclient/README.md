# The Web App
The Polymer Project was used to create SkittyBot's built in Web App.
Learn more about the Polymer Project at https://www.polymer-project.org/.
A pre-compiled version of the bots web app is already included in this project so there is no need to download anything.
However, if you plan on modifying the web app then you will need to install `polymer-cli` and its dependencies so you can use `polymer serve` and `polymer build` to work on the project.

# Development notes
* Make sure all parent directories leading up to the build directory have purely alphanumeric naming as the build tool will quit without an explanation even with the `--verbose` flag lol. Stuff like brackets in the name will kill the build tools.
* Project includes `commands.json` and `api/plugins.json` to simulate the bots API when running `polymer serve`.