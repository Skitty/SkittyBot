'use strict';

const DEBUGMODE = process.env.BOT_DEBUGGING === 'true';
const TAG = '```';

const spawn = require('child_process').spawn;

function git_pull(msg, args) {

    if (SkittyBot.config.owner_id !== msg.author.id) {
        msg.reply(`I can't let you do that.`);
        return;
    }

    if (DEBUGMODE && !SkittyBot.DEVOVERRIDE) {
        if (msg) msg.reply(`You likely don't want to execute this in debug mode. Execute devoverride to ignore this warning.`);
        return;
    }
    let repo = SkittyBot.config.location.home;
    if (args[0]) {
        switch (args[0]) {
            case 'private': repo = SkittyBot.config.location.private_plugins; break;
            case 'public': repo = SkittyBot.config.location.public_plugins; break;
            case 'help': {
                if (msg) msg.reply(`Options: \`public, private\`\nProviding no paramaters pulls from bots main repo.`);
                return;
            }
        }
    }
    let output = '';
    let git = spawn('git', ['pull'], { cwd: repo });
    git.stdout.on('data', data => {
        output += data.toString();
    });
    git.on('close', code => {
        SkittyBot.logger.log(output);
        if (msg) msg.channel.send(`**Exit Code**: ${code}\n**Output**:\n${TAG}\n${output}${TAG}`);
    });
}

function shutdown(msg) {
    msg.channel.send(`Shutting down...`).then(() => {
        SkittyBot.shut_down();
    });
}

function npm_install(msg) {

    if (SkittyBot.config.owner_id !== msg.author.id) {
        msg.reply(`I can't let you do that.`);
        return;
    }

    let output = '';
    var npm = spawn('npm', ['install']);
    npm.stdout.on('data', data => {
        output += data.toString();
    });
    npm.on('close', code => {
        SkittyBot.logger.log(output);
        if (msg) msg.channel.send(`**Exit Code**: ${code}\n**Output**:\n${TAG}\n${output}${TAG}`);
    });
}

async function add_administrator(msg, args) {

    if (SkittyBot.config.owner_id !== msg.author.id) {
        msg.reply(`I can't let you do that.`);
        return;
    }

    if (!args[0]) {
        msg.reply(`1st argument must be a user ID, second must be an execution ring level 0-3`);
        return;
    }
    if (isNaN(args[1])) {
        msg.reply(`argument 2 should be an execution ring level 0-3`);
        return;
    }
    let res = await SkittyBot.system_database.get(`SELECT * FROM administrators WHERE user_id = ?`, args[0]);
    if (res) {
        await SkittyBot.system_database.get(`DELETE FROM administrators WHERE user_id = ?`, args[0]);
    }
    SkittyBot.system_database.run(`INSERT INTO administrators (user_id, ring) VALUES (?, ?)`, args[0], args[1])
        .then(r => {
            if (!r) {
                msg.reply(`response was null.`);
                return;
            }
            if (!r.changes) {
                msg.reply(`Looks like I wasn't able to add them as an administrator for some reason.`);
            } else {
                SkittyBot.administrators.set(args[0], args[1]);
                let user = SkittyBot.client.users.get(args[0]);
                if (user) {
                    msg.channel.send(`${user} is now a ring-${args[1]} administrator.`);
                } else {
                    msg.channel.send(`User is now a ring-${args[1]} administrator.`);
                }
            }
        });
}

function remove_administrator(msg, args) {
    if (!args.length) {
        msg.reply(`1st argument must be a user ID.`);
        return;
    }
    if (!SkittyBot.administrators.has(args[0])) {
        msg.reply(`user ID is not present in permissions ring map.`);
        return;
    }
    let executor_ring = SkittyBot.administrators.get(msg.author.id);
    let target_ring = SkittyBot.administrators.get(args[0]);
    if (executor_ring >= target_ring) {
        msg.channel.send(`Unable to comply, target user execution ring higher than or equal to executor ring.`);
        return;
    }
    SkittyBot.administrators.delete(args[0]);
    SkittyBot.system_database.run(`DELETE FROM administrators WHERE user_id = ?`, args[0]);
    msg.reply(`user removed from ring map.`);
}

const configuration = {
    commands: [
        {
            command: 'gitpull',
            aliases: [
                `pull`,
                `update`
            ],
            owner: true,
            ring: 0,
            process: git_pull
        },
        {
            command: 'shutdown',
            aliases: [
                `restart`
            ],
            owner: true,
            ring: 0,
            process: shutdown
        },
        {
            command: 'reload-module',
            aliases: [
                `reload-plugin`,
                `module-reload`,
                `plugin-reload`,
                'reload'
            ],
            owner: true,
            ring: 0,
            process: (msg, args) => {
                if ((SkittyBot.plugins instanceof Object) && (SkittyBot.plugins.hotReloadModule instanceof Function)) {
                    SkittyBot.plugins.hotReloadModule(msg, args);
                } else {
                    msg.channel.send(`hotReloadModule is not an instance of Function.`);
                }
            }
        },
        {
            command: 'reload-all-modules',
            aliases: [
                `ram`
            ],
            owner: true,
            ring: 0,
            process: (msg, args) => {
                if (typeof SkittyBot.DEVOVERRIDE === 'undefined') {
                    msg.channel.send([
                        `WARNING: You're attempting to reload all modules!`,
                        `To perform a hot-reload of all modules without shutting down the bot you must enable the administrative override.`
                    ].join('\n'));
                    return;
                }
                if (SkittyBot.DEVOVERRIDE === 0xA5501) {
                    if ((SkittyBot.plugins instanceof Object) && (SkittyBot.plugins.hotReloadModule instanceof Function)) {
                        SkittyBot.plugins.hotReloadModule(msg, args, true);
                    } else {
                        msg.channel.send(`hotReloadModule is not an instance of Function.`);
                    }
                } else {
                    msg.channel.send(`DEVOVERRIDE MODE NOT EQUAL.`);
                }
            }
        },
        {
            command: 'npmi',
            owner: true,
            ring: 0,
            process: npm_install
        },
        {
            command: 'devoverride',
            aliases: [
                'setoverride',
                'sor',
                'override'
            ],
            examples: [
                'sor set ring 1 user_id',
                'sor [true|false]'
            ],
            owner: true,
            ring: 0,
            process: (m, args) => {
                // only the actual owner can mess with this
                if (SkittyBot.config.owner_id !== m.author.id) {
                    m.reply(`I can't let you do that...`);
                    return;
                }
                // some hacks to allow adjusting execution rings for users without saving them to filesystem
                if (args[0]) {
                    switch (args[0]) {
                        case 'true': SkittyBot.DEVOVERRIDE = 0xA5501; break;
                        case 'false': delete SkittyBot.DEVOVERRIDE; break;
                        case 'set': {
                            switch (args[1]) {
                                case 'ring': {
                                    let level = parseInt(args[2]);
                                    if (isNaN(level) || level < 0 || level > 3) {
                                        m.channel.send(`Out of bounds level!`);
                                        return;
                                    }
                                    let user = SkittyBot.client.users.get(args[3]);
                                    if (user) {
                                        SkittyBot.administrators.set(user.id, level);
                                        m.channel.send(`Ok. ${user}'s execution ring has been set to ring ${level}.`);
                                        return;
                                    } else {
                                        m.reply(`User with ID ${args[3]} not found in client user cache. Unable to adjust execution ring.`);
                                        return;
                                    }
                                }
                            }
                            m.reply('set what?');
                            return;
                        }
                        case 'unset': {
                            switch (args[1]) {
                                case 'ring': {
                                    if (SkittyBot.administrators.has(args[2])) {
                                        SkittyBot.administrators.delete(args[2]);
                                        m.channel.send(`The ID was found and removed from the execution ring...`);
                                        return;
                                    } else {
                                        m.reply(`ID "${args[2]}" not found in ring registry.`);
                                        return;
                                    }
                                }
                            }
                            m.reply('set what?');
                            return;
                        }
                    }
                    m.reply(`Ok.`);
                    return;
                }
                if (SkittyBot.DEVOVERRIDE === 0xA5501) {
                    delete SkittyBot.DEVOVERRIDE;
                    m.reply(`Override disabled.`);
                } else if (SkittyBot.DEVOVERRIDE !== 0xA5501) {
                    SkittyBot.DEVOVERRIDE = 0xA5501;
                    m.reply(`Override enabled.`);
                }
            }
        },
        {
            command: 'add-admin',
            aliases: [
                'admin-add',
                'adminadd',
                'addadmin',
                'set-user-ring',
                'set-execution-ring',
                'set-ring'
            ],
            owner: true,
            ring: 0,
            process: add_administrator
        },
        {
            command: 'remove-admin',
            aliases: [
                'admin-remove',
                'removeadmin',
                'adminremove'
            ],
            owner: true,
            ring: 1,
            process: remove_administrator
        }
    ]
};

SkittyBot.registerPlugin(configuration);
