# Default Commands
This is where the the bots default commands are.
All commands here are commands intended for use by the bot owner or bot configuration oriented commands intended for guild managers.

As of the time writing this, a few of the commands in here are intended for the owner so you cannot see these commands show up in the bot web app.
They're effectively private and unlisted commands so you need to check the command definitions to see what exists.