'use strict';

const { MessageEmbed } = require('discord.js');
const moment = require('moment');

const chkExt = a => {
    if (!a) return 'png';
    return a.startsWith('a_') ? 'gif' : 'png';
};

async function useravatar(msg, args) {
    let user;
    if (msg.guild) {
        user = msg.mentions.users.last();
        if (!user && !args[0]) {
            user = msg.member.user;
        } else {
            let search = args.join(' ').toLowerCase();

            let members = msg.guild.members.cache.filter(u => {
                if (u.nickname && u.nickname.toLowerCase().includes(search)) return true;
                if (u.user.username && u.user.username.toLowerCase().includes(search)) return true;
                return false;
            });
            if (members.size >= 1) {
                let member = members.get(members.keys().next().value);
                user = member.user;
            }
        }
    } else {
        // DMs
        user = msg.author;
    }
    if (!user) {
        msg.channel.send(`Couldn't get that user.`);
    } else {
        const av = user.displayAvatarURL({
            format: chkExt(user.avatar),
            size: 2048
        });
        msg.channel.send(av);
    }
}

function guildicon(msg, args) {
    let guild = msg.guild;
    if (args.length) {
        let tGuild = SkittyBot.client.guilds.get(args[0]);
        if (tGuild) guild = tGuild;
    }
    msg.channel.send(guild.iconURL({ format: chkExt(guild.icon), size: 2048 }));
}

function bot_invite(msg) {
    if (SkittyBot.config.custom_invite) {
        if (msg === 'link') return SkittyBot.config.custom_invite;
        msg.reply(SkittyBot.config.custom_invite);
    } else {
        let p_toke = SkittyBot.config.permissions_token;
        let cid = SkittyBot.client.user.id;
        let ss_toke = SkittyBot.config.support_server_token;
        if (msg === 'link') return `https://discordapp.com/oauth2/authorize?permissions=${p_toke}&scope=bot&client_id=${cid}`;
        let embed = new MessageEmbed();
        embed.setAuthor(SkittyBot.client.user.username, SkittyBot.client.user.displayAvatarURL({ format: 'jpg' }), bot_invite('link'));
        embed.addField(`Invite`, `Use this [link](https://discordapp.com/oauth2/authorize?permissions=${p_toke}&scope=bot&client_id=${cid}) to invite the bot!`, true);
        if (ss_toke) embed.addField(`Community Server`, `Join the [community server](https://discord.gg/${ss_toke}) for any questions and or help!`, true);
        msg.channel.send(embed);
    }
    return null;
}

function send_message(msg, args, chan, title) {
    chan = SkittyBot.client.channels.get(chan);
    if (!chan) {
        msg.react(`⛔ Error: I couldn't find the channel I'm supposed to send this to.`);
        return;
    }
    let message = args.join(' ');
    if (message.length < 3) {
        msg.reply('Too short.');
        return;
    }
    if (message.length > 1500) {
        msg.reply('Sorry, your message too long... Try to limit it to 1500 characters.');
        return;
    }
    let embed = new MessageEmbed();

    if (title) embed.setTitle(title);

    let avatar = msg.author.displayAvatarURL({
        format: 'png',
        size: 256
    });
    if (avatar) embed.setThumbnail(avatar);

    let server = `Direct Message Channel`;
    let footer = `U:${msg.author.id}`;

    if (msg.guild) {
        server = msg.guild.name;
        footer += ` G:${msg.guild.id} C:${msg.channel.id}`;
        if (msg.member && msg.member.displayHexColor) embed.setColor(msg.member.displayHexColor);
    }

    embed.setDescription([
        `**From**: *${server}*`,
        `**By**: *${msg.author.tag}*\n`, // (created: ${moment(msg.author.createdAt).format('M/D/YY')})*\n`,
        `**Message**:`,
        '```',
        message.replace(/`/g, "'"),
        '```'
    ].join('\n'));

    embed.setFooter(footer);

    chan.send(embed);
    msg.reply(`message successfully posted to ${chan.guild.name}#${chan.name}, thank you for your feedback!`);
}

function bot_help_page_link(msg, args) {
    let link = SkittyBot.config.bot_url;
    if (!link) {
        msg.reply(`The bot web app is unavailable due to my owner not having configured a web app url at which it could be reached.`);
        return;
    }
    let query = args[0] ? `?search=${encodeURIComponent(args.join(' '))}` : '';
    msg.reply(`https://${link}/botsys/sb-commands${query}`);
}

function bot_help_main(msg, args) {
    let tag = '```';
    let prefix = SkittyBot.prefix(msg);

    let embed = new MessageEmbed();
    embed.setAuthor(SkittyBot.client.user.username, SkittyBot.client.user.displayAvatarURL({ format: 'jpg' }), bot_invite('link'));

    let target = args.join(' ');
    if (target) target = target.toLowerCase();

    if (target) {

        let command_object = SkittyBot.commands.get(SkittyBot.command_alias.get(target) || target);

        // do not list private commands
        // DO however show info on hidden commands!
        if (command_object !== undefined && !command_object.private) {
            let c = command_object;
            embed.setTitle(`${c.plugin_title} Command: ${c.command}`);
            if (c.description) {
                if (!Array.isArray(c.description)) c.description = [c.description];
                embed.setDescription(`${tag}\n${c.description.join('\n')}${tag}`);
            }
            if (c.aliases) {
                if (!Array.isArray(c.aliases)) c.aliases = [c.aliases];
                embed.addField('Alias(es)', `${tag}\n${c.aliases.join(', ')}${tag}`);
            }
            if (c.example) {
                if (!Array.isArray(c.example)) c.example = [c.example];
                embed.addField('Example(s)', `${tag}\n${prefix}${c.example.join(`\n${prefix}`)}${tag}`);
            }
            if (c.client) {
                if (!Array.isArray(c.client)) c.client = [c.client];
                embed.addField('Permission(s) I need', `${tag}\n${c.client.join(`, `)}${tag}`);
            }
            if (c.member) {
                if (!Array.isArray(c.member)) c.member = [c.member];
                embed.addField('Permission(s) YOU need', `${tag}\n${c.member.join(`, `)}${tag}`);
            }
            if (c.guildOnly) embed.addField('Guild only', `${tag}\nThis command can only be executed in a guild channel!\nNo DMs.${tag}`);
            if (c.owner) embed.addField('Bot owner only', `${tag}\nOnly a bot administrator with required execution elevation ring can execute this command!${tag}`);
            if (c.noDisable) embed.addField('Cannot disable', `${tag}\nThis command cannot be disabled!${tag}`);
            msg.reply(embed);
            return;
        }

        let mod, list = [];
        let keys = Array.from(SkittyBot.plugins.list.keys());
        keys.forEach(key => {
            let p = SkittyBot.plugins.list.get(key);
            if ((!p.private && p.enabled) && p.plugin_title.toLowerCase() === target) mod = p;
        });
        if (mod) {
            mod.commands.forEach(c => {
                if (!c.owner && !c.private && !c.hidden) {
                    if (msg.channel.nsfw) {
                        list.push(c.command);
                        return;
                    }
                    if (c.nsfw) return;
                    list.push(c.command);
                }
            });
            if (!list.length) {
                msg.reply(`No available commands for this module.`);
                return;
            }
            embed.addField(`Module commands`, `\`\`\`\n${list.join(', ')}\`\`\``);
            msg.reply(embed);
        } else {
            msg.reply(`"${target}" is an unrecognized command or module name.`);
        }

    } else {

        let link = SkittyBot.config.bot_url;

        embed.setTitle(`Information`);

        if (!link) {
            embed.setDescription([
                `A bot web app URL has not been configured.`,
                `Due to this I cannot produce a web app link containing all existing commands.`,
                `You will have to make use of the modules and help command to explore any existing commands instead.`
            ].join('\n'));
        } else {
            embed.setDescription([
                `All my commands can be found [here!](https://${link}/botsys/sb-commands)`,
                `For a quick list of commands use \`${prefix}commands\``
            ]);
        }

        embed.addField('Using commands', [
            `You can use this command with command names or their aliases.`,
            `You can also use this command with module names.`,
            `The \`modules\` command is used to list all my module names.`,
            `Note: Modules are basically sets of command categories.`,
            `**Some examples below**:`,
            tag,
            `${prefix}modules`,
            `${prefix}help graphics`,
            `${prefix}help complain`,
            `${prefix}complain this is too difficult!!1`,
            tag,
            `The \`help-docs\` command can be used to get a direct link to the webpage that has all my commands.`,
            `Generate a quick command search link with the \`help-docs\`.`,
            `Example: \`${prefix}help-docs eyes\`.`
        ].join('\n'));

        let ss_toke = SkittyBot.config.support_server_token;

        if (ss_toke) {
            embed.addField(`Home server`,
                [
                    `Having trouble with the bot? Like to ask a question? Make a suggestion, `,
                    `complaint, and anything else, you're welcome to join my [home server](https://discord.gg/${ss_toke}).`
                ].join(' ')
            );
        }

        if (SkittyBot.announcements && SkittyBot.announcements.size) {
            let announce = [];
            let ao, n = 0;
            for (let key of SkittyBot.announcements.keys()) {
                if (n > 1) break;
                ao = SkittyBot.announcements.get(key);
                announce.push(`__*${moment(ao.logged).format('lll')}*__`);
                announce.push(tag);
                announce.push(`${SkittyBot.utils.trunc.clip(ao.message, 200)}`);
                announce.push(tag);
                n++;
            }
            announce.push(`*For a bigger list of announcements, use the \`announcements\` command!*`);
            embed.addField(`Bot announcements`, announce.join('\n'));
        }
        
        embed.addField(`Development`, `I'm an open source bot called [SkittyBot](https://gitlab.com/Skitty/SkittyBot) hosted on GitLab.`);

        if (SkittyBot.config.dbl_token) {
            embed.addField(`Voting`, `If you like my bot please consider [voting](https://discordbots.org/bot/${SkittyBot.client.user.id}) for it :3c`);
        }

        msg.reply({ embeds: [embed]});

    }
}

function bot_admin_help(msg, args) {

    if (!SkittyBot.administrators.has(msg.author.id)) {
        msg.channel.send(`This command is only available to bot administrators.`);
        return;
    }

    let tag = '```';
    let prefix = SkittyBot.prefix(msg);
    let ring = SkittyBot.administrators.get(msg.author.id);

    if (isNaN(ring)) {
        msg.reply(`Something is wrong.`);
        return;
    }

    let embed = new MessageEmbed();
    embed.setAuthor(SkittyBot.client.user.username, SkittyBot.client.user.displayAvatarURL({ format: 'jpg' }), bot_invite('link'));

    let target = args.join(' ');
    if (target) target = target.toLowerCase();

    if (target) {

        let command_object = SkittyBot.commands.get(SkittyBot.command_alias.get(target) || target);

        if (command_object !== undefined) {
            let c = command_object;
            embed.setTitle(`${c.plugin_title} Command: ${c.command}`);
            if (c.description) {
                if (!Array.isArray(c.description)) c.description = [c.description];
                embed.setDescription(`${tag}\n${c.description.join('\n')}${tag}`);
            }
            if (c.aliases) {
                if (!Array.isArray(c.aliases)) c.aliases = [c.aliases];
                embed.addField('Alias(es)', `${tag}\n${c.aliases.join(', ')}${tag}`);
            }
            if (c.example) {
                if (!Array.isArray(c.example)) c.example = [c.example];
                embed.addField('Example(s)', `${tag}\n${prefix}${c.example.join(`\n${prefix}`)}${tag}`);
            }
            if (c.client) {
                if (!Array.isArray(c.client)) c.client = [c.client];
                embed.addField('Permission(s) I need', `${tag}\n${c.client.join(`, `)}${tag}`);
            }
            if (c.member) {
                if (!Array.isArray(c.member)) c.member = [c.member];
                embed.addField('Permission(s) YOU need', `${tag}\n${c.member.join(`, `)}${tag}`);
            }
            if (!isNaN(c.ring)) {
                if (!Array.isArray(c.member)) c.member = [c.member];
                embed.addField('Execution ring', `${tag}\n${c.ring}${tag}`);
            }
            if (c.guildOnly) embed.addField('Guild only', `${tag}\nThis command can only be executed in a guild channel!\nNo DMs.${tag}`);
            if (c.owner) embed.addField('Bot owner only', `${tag}\nOnly a bot administrator with the required execution elevation ring can execute this command!${tag}`);
            if (c.noDisable) embed.addField('Cannot disable', `${tag}\nThis command cannot be disabled!${tag}`);
            msg.author.send(embed);
            return;
        }

        let mod, list = [];
        let keys = Array.from(SkittyBot.plugins.list.keys());
        keys.forEach(key => {
            let p = SkittyBot.plugins.list.get(key);
            if (p.enabled && p.plugin_title.toLowerCase() === target) mod = p;
        });
        if (mod) {
            mod.commands.forEach(c => {
                if (!isNaN(c.ring) && c.ring >= ring) {
                    list.push(c.command);
                }
            });
            if (!list.length) {
                msg.reply(`No available commands for this module.`);
                return;
            }
            embed.addField(`Module commands`, `\`\`\`\n${list.join(', ')}\`\`\``);
            msg.reply(embed);
        } else {
            msg.reply(`"${target}" is an unrecognized command or module name.`);
        }

    } else {

        embed.setDescription(`To view commands available to your execution ring use \`admin-commands\`.`);
        msg.author.send(embed);

    }
}

function bot_commands(msg) {

    // list of modules for which to present commands from
    let target = {
        graphics: null,
        entertainment: null,
        searches: null,
        misc: null,
        roleplay: null
    };

    let mod_keys = Object.keys(target);
    let keys = Array.from(SkittyBot.plugins.list.keys());

    keys.forEach(key => {
        let p = SkittyBot.plugins.list.get(key);
        let title = p.plugin_title.toLowerCase();
        if ((!p.private && p.enabled) && mod_keys.includes(title)) target[title] = p;
    });

    let embed = new MessageEmbed();
    embed.setTitle(`Command ShortList`);
    embed.setDescription(`*For a full list of commands use the online docs or help & modules command.*`);
    embed.setAuthor(SkittyBot.client.user.username, SkittyBot.client.user.displayAvatarURL({ format: 'jpg' }), bot_invite('link'));

    mod_keys.forEach(title => {
        let list = [];
        if (!target[title]) return;
        target[title].commands.forEach(c => {
            if (!c.owner && !c.private && !c.hidden) {
                if (msg.channel.nsfw) {
                    list.push(c.command);
                    return;
                }
                if (c.nsfw) return;
                list.push(c.command);
            }
        });
        if (list.length) embed.addField(title, `\`\`\`\n${list.join(', ')}\`\`\``, true);
    });

    msg.reply(embed);

}

function bot_admin_commands(msg, args) {

    let nofile = false;
    let tag = '```';
    let ring = SkittyBot.administrators.get(msg.author.id);

    if (!SkittyBot.administrators.has(msg.author.id)) {
        msg.channel.send(`This command is only available to bot administrators.`);
        return;
    }

    if (args[0]) args[0] = args[0].toLowerCase();
    switch (args[0]) {
        case 'nofile': {
            nofile = true;
            break;
        }
        case 'as': {
            if (isNaN(args[1])) {
                msg.reply(`second argument should be a ring level.`);
                return;
            }
            ring = args[1];
        }
    }

    if (isNaN(ring)) {
        msg.reply(`Ring is not a level.`);
        return;
    }

    let embed = new MessageEmbed();
    embed.setTitle(`Administrator Command`);
    embed.setDescription(`*This is a list of commands available to your current execution ring.*`);
    embed.setAuthor(SkittyBot.client.user.username, SkittyBot.client.user.displayAvatarURL({ format: 'jpg' }), bot_invite('link'));

    let commands = {};
    let c_added = false;

    SkittyBot.commands.forEach(c => {
        if (c.owner && !isNaN(c.ring)) {
            if (c.ring >= ring) {
                let P_TITLE = `MODULE: ${c.plugin_title}`;
                let CMD = `COMMAND: ${c.command}`;

                if (!commands[P_TITLE]) commands[P_TITLE] = {};
                if (!commands[P_TITLE][CMD]) commands[P_TITLE][CMD] = {};

                if (c.description) commands[P_TITLE][CMD].DESC = c.description;
                if (c.example) commands[P_TITLE][CMD].EXAMPLE = c.example;
                if (c.aliases) commands[P_TITLE][CMD].ALIASES = c.aliases;
                if (c.private) commands[P_TITLE][CMD].PRIVATE = c.private;
                if (c.hidden) commands[P_TITLE][CMD].HIDDEN = c.hidden;
                if (c.guildOnly) commands[P_TITLE][CMD].GUILD_ONLY = c.guildOnly;

                commands[P_TITLE][CMD].RING = c.ring;

                c_added = true;
            }
        }
    });

    if (!c_added) {
        msg.reply(`Nothing to show...`);
        return;
    }

    let show = [
        `# RING-${ring}-COMMANDS`,
        '# This is a list of commands available to your execution rings privilege.',
        '# Rings are 0-3 where 0 has highest execution privilege.',
        `# Commands that require an elevation ring higher than your current ring-${ring} will not show up in this list.`,
        `# Your execution ring has access to any ring-n commands equal to or lower than ring-${ring}.`,
        `# Notes on boolean keys like "PRIVATE" and "GUILD_ONLY".`,
        `#     GUILD_ONLY: when true, command only works in guild channels and not a DM.`,
        `#     PRIVATE   : when true, the command is not listed publicly and is effectively an unlisted or secret command.`,
        '# P.S Do not share this file please -Skitty',
        ''
    ];

    show.push(SkittyBot.utils.treeify({ COMMANDS: commands }).join('\n'));
    let all = show.join('\n');
    if (all.length > 1990 && nofile) {
        let message_array = all.split('\n');
        let concat = '';
        message_array.forEach((chunk, i) => {
            if ((concat.length + chunk.length) > 1990) {
                msg.author.send(`${tag}\n${concat}${tag}`);
                concat = '';
            }
            concat += `${chunk}\n`;
            if (i === message_array.length - 1) { // catch last always
                msg.author.send(`${tag}\n${concat}${tag}`);
            }
        });
    } else {
        msg.author.send({ files: [{ attachment: Buffer.from(show.join('\n')), name: `commands.txt` }] });
    }

}

function bot_help_modules(msg, args) {

    if (args.length) {
        msg.reply(`I think you meant to use the help command.`).then(() => bot_help_main(msg, args));
        return;
    }

    let list = [];
    let prefix = SkittyBot.prefix(msg);

    SkittyBot.plugins.list.forEach(p => {
        // Do we have anything to show? If so, list container title
        if (!p.private && p.enabled) {
            if (Array.isArray(p.commands) && p.commands.filter(c => {
                if (!c.owner && !c.private && !c.hidden) {
                    if (msg.guild) {
                        if (msg.channel.nsfw) return true;
                        return !c.nsfw;
                    }
                    return true;
                }
                return false;
            }).length) list.push(p.plugin_title);
        }
    });

    if (list.length) {
        let tag = '```', embed;
        for (let n = 30, i = n, p; ; i += n) {

            p = list.slice(i - n, i);
            if (!p.length) break;

            embed = new MessageEmbed()
                .setTitle(`Modules Names`)
                .setDescription(`${tag}\n${p.join(', ')}${tag}`)
                .setAuthor(SkittyBot.client.user.username, SkittyBot.client.user.displayAvatarURL({ format: 'jpg' }), bot_invite('link'));

            msg.channel.send(embed);

        }

        let info = [];

        info.push(`The above are all module names separated by comma.`);
        info.push(`A list of commands within a module can be queried using the modules name. Example: \`${prefix}help graphics\``);
        info.push(`It is recommended to use the help page instead, where you have the ability to search for specific commands.`);
        info.push(`NOTICE: Module names are **NOT COMMANDS**.`);
        info.push(`If you are having difficulty understanding why I do not respond to things like \`!twitter bot\`, please remove me immediately.`);

        msg.channel.send(info.join('\n'));

    } else {
        msg.reply(`I can't get a list of my modules for some reason.`);
    }
}

function announcements(msg, args) {
    let posts = 3;
    if (args[0]) {
        posts = parseInt(posts);
        if (isNaN(posts)) {
            msg.reply(`That doesn't look like a numberino there boi~`);
            return;
        }
        if (posts > 10) {
            msg.reply(`Try less than ten... Sorry..`);
            return;
        }
        if (posts < 0) {
            msg.reply(`I just won't post anything instead...`);
            return;
        }
    }
    if (SkittyBot.announcements && SkittyBot.announcements.size) {
        let tag = '```';
        let embed;
        let announce;
        let ao, n = 1;
        for (let key of SkittyBot.announcements.keys()) {
            if (n > posts) break;
            embed = new MessageEmbed();
            ao = SkittyBot.announcements.get(key);
            announce = [];
            announce.push(`__*${moment(ao.logged).format('lll')}*__`);
            announce.push(tag);
            announce.push(`${SkittyBot.utils.trunc.clip(ao.message, 2000)}`);
            announce.push(tag);
            embed.setTitle(`${SkittyBot.config.bot_name} Announcements`)
                .setDescription(announce.join('\n'));
            msg.channel.send(embed);
            n++;
        }
    } else {
        msg.reply(`There's nothing to show at this time.`);
    }
}

function role_info(msg, args) {
    if (!args[0]) {
        msg.reply(`This command requires either a case-sensitive role name, role mention or role ID.`);
        return;
    }
    let role;
    let full_concat = args.join(' ');
    if (isNaN(full_concat)) {
        let role_id = full_concat.match(/<@&(\d+)>/i);
        if (role_id) {
            role_id = role_id.pop();
            role = msg.guild.roles.get(role_id);
        } else {
            role = msg.guild.roles.find(r => r.name === full_concat);
            if (!role) {
                role = msg.guild.roles.find(r => r.name.toLowerCase() === full_concat.toLowerCase());
            }
        }
    } else {
        // Must be role ID
        role = msg.guild.roles.get(full_concat);
    }
    if (role) {
        let embed = new MessageEmbed();
        embed.setTitle(role.name);
        embed.addField('ID', role.id, true);
        embed.addField('Mentionable', role.mentionable, true);
        embed.addField('Members', role.members.size, true);
        if (role.color) {
            embed.addField('Color', role.hexColor, true);
            embed.setColor(role.hexColor);
        } else {
            embed.addField('Color', 'None', true);
        }
        embed.addField('Editable by me', role.editable, true);
        embed.addField('Hoist', role.hoist, true);
        embed.addField('Position', role.position, true);
        if (role.deleted) embed.addField('Deleted', 'This role no longer exists.', true);
        msg.reply(embed);
        console.log(role);
    } else {
        msg.reply(`Couldn't find that role.`);
    }
}

const configuration = {
    commands: [
        {
            command: 'help',
            description: [
                'Shows you information on a command, like this one.'
            ],
            example: [
                `help avatar`
            ],
            throttle: 3000,
            noDisable: true,
            process: bot_help_main,
        },
        {
            command: 'admin-help',
            description: [
                'Bot Admins only.',
                'Shows you unlisted administrator commands available to your execution ring.'
            ],
            throttle: 3000,
            noDisable: true,
            process: bot_admin_help,
        },
        {
            command: 'commands',
            description: [
                'Shows you a brief list of some of my commands.'
            ],
            throttle: 5000,
            noDisable: true,
            process: bot_commands,
        },
        {
            command: 'admin-commands',
            description: [
                'Bot Admins only.',
                'This command generates a list of commands available to your execution privilege in a file.',
                `If you'd rather have the commands posted to your DMs, pass nofile as an argument to this command.`
            ],
            example: [
                'admin-commands nofile',
                'admin-commands as 1'
            ],
            throttle: 5000,
            noDisable: true,
            process: bot_admin_commands,
        },
        {
            command: 'help-docs',
            aliases: [
                'helpdocs',
                'helppage'
            ],
            description: [
                'Gives you a link to my help page.',
                'Any text after the command are arguments which will be used to filter the commands in the command help page to commands that have the arguments you provided.'
            ],
            example: [
                `help-docs avatar`,
                `help-docs random image`
            ],
            throttle: 3000,
            noDisable: true,
            process: bot_help_page_link,
        },
        {
            command: 'modules',
            description: [
                'Displays a lit of command modules.',
                'Pass a module name to the command to see all its commands.'
            ],
            example: [
                `module`,
                `modules graphics`
            ],
            noDisable: true,
            throttle: 3000,
            process: bot_help_modules,
        },
        {
            command: 'invite',
            description: 'Provides an invite link for the bot.',
            throttle: 7000,
            noDisable: true,
            process: bot_invite
        },
        {
            command: 'vote',
            description: 'Gives you a link to the bots voting page.',
            noDisable: true,
            throttle: 7000,
            process: msg => msg.reply(`https://discordbots.org/bot/${SkittyBot.client.user.id}/vote`)
        },
        {
            command: 'announcements',
            example: [
                `announcements`,
                `announcements 10`
            ],
            description: [
                'Shows you a more complete list of announcements from the bot developer.',
                'By default the bot will spit out 3 of the most recent announcements, to make post more announcements, give it a number.'
            ],
            throttle: 3000,
            process: announcements
        },
        {
            command: 'avatar',
            example: 'avatar @mention',
            aliases: [
                'av'
            ],
            throttle: 1000,
            description: `Posts a mobile friendly link to the mentioned user's avatar`,
            process: useravatar
        },
        {
            command: 'guild-icon',
            example: [
                'guild-icon',
                'guild-icon guildID'
            ],
            aliases: [
                'server-icon',
                'guildicon',
                'servericon'
            ],
            throttle: 1000,
            description: `Posts the current guild icon. If you provide a guild ID and the bot is in that guild, it'll post its icon instead.`,
            process: guildicon
        },
        {
            command: 'roleinfo',
            example: 'roleinfo <role-name|@rolemention|role-id>',
            description: 'Posts info about a role.',
            aliases: [
                'ri'
            ],
            throttle: 3000,
            process: role_info
        },
        {
            command: 'bot-suggest',
            aliases: [
                `suggestion`,
                `suggest`
            ],
            example: [
                `suggest delete your bot`
            ],
            description: [
                `Have a feature request? Idea for the bot? Join my server with the invite command or make a suggestion using this command!`
            ],
            process: (msg, args) => {
                send_message(msg, args, '621887078883655691', 'Suggestion!');
            },
            throttle: 1000 * 60 * 3
        },
        {
            command: 'bot-complain',
            aliases: [
                `bot-complaint`,
                `complain`
            ],
            example: [
                `complain youre bot suks`
            ],
            description: [
                `Are you mad at the bot? You can complain about it or something with this.`,
                `For compliments, use bot-compliment instead.`,
                `For error reporting, use bot-report instead.`
            ],
            process: (msg, args) => {
                send_message(msg, args, '621887078883655691', '[-] Complaint!');
            },
            throttle: 1000 * 60 * 3
        },
        {
            command: 'bot-compliment',
            aliases: [
                `compliment`
            ],
            example: [
                `compliment thank you`
            ],
            description: [
                `Want to say something nice? You can compliment the bot or say something positive with this.`,
                `For complaints, use bot-complain instead.`
            ],
            process: (msg, args) => {
                send_message(msg, args, '621887078883655691', '[+] Compliment!');
            },
            throttle: 1000 * 60 * 3
        },
        {
            command: 'bot-report',
            aliases: [
                `report`
            ],
            example: [
                `bot-report something`
            ],
            description: [
                `USe this to report errors, bugs or anything else related to me!`
            ],
            process: (msg, args) => {
                send_message(msg, args, '621887078883655691', '[X] Report!');
            },
            throttle: 1000 * 60 * 3
        }
    ]
};

SkittyBot.registerPlugin(configuration);
