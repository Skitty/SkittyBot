# Example public plugin
This plugin contains example usage of the bots API which should help you get an idea of how to use the bots global `SkittyBot` object.
You can also take a look at the default commands which are basically plugins for more complex usage.

# package.json
The `package.json` we use for our plugins are a bit different from conventional `package.json` files.
They contain key value pairs used to provide some information about the plugin to the bot.

```json
{
    // the main file the bot will require()
    "main": "index.js",
    // This is your plugin's title which will be displayed in the bots web app when viewing the command help section
    "plugin_title": "Example Plugin", 
    // The plugin's description that will accompany the plugins title
    "description": "An example plugin to help you get started creating your own plugins.",
    // [Optional] tells the bots plugin loader to skip loading this plugins commands.
    "disabled": true,
    // And finally, you plugin's dependencies.
    // The bot will read this package.json file and concat its dependencies into the main bots package.json.
    // If it failed loading a plugin because you didn't npm install its dependencies yourself, you can always npm install in the bots root folder
    // which will include these dependencies.
    "dependencies": {
        "coolpackage": ">=1.0.0"
    }
}
```
Note: the actual `package.json` files do not support comments like the above. So never include the `// comments` you see here.