'use strict';

// Example command function for command example1 with alias ex1.
function example1(msg, args) {
    // All command functions should have a msg and args paramter.
    // msg is discord.js's message object
    // args is a tokenized version of the message content without the command in it.
    let arglen = args.length;
    let argstring = args.join(', ');
    let additional = arglen ? ` Arguments are: \`${argstring}\`` : '';
    msg.reply(`Example1: You gave me ${arglen} arguments!${additional}`);
}

function example2(msg, args, somethingelse) {
    let arglen = args.length;
    let argstring = args.join(', ');
    let additional = arglen ? ` Arguments are: \`${argstring}\`` : '';
    msg.reply(`Example2 [${somethingelse}]: You gave me ${arglen} arguments!${additional}`);
}

function queuedFunction(msg, args) {
    msg.reply(`This command will take 10 seconds to finish.`);
    return new Promise(resolve => {
        setTimeout(() => {
            let params = args.join(', ');
            msg.reply(`queuedFunction finished!${args[0] ? ` Your args: \`${params}\`` : ''}`);
            resolve();
        }, 10000);
    });
}

/*
    onLoad: This function is executed before the commands
    are processed by the bot when it is defined as async.
    Otherwise it will continue loading the next plugin
    before this function is finished executing.

    Using an async function for the onLoad routine is useful
    in the event that you want to have control over the commands definitions
    in the configuration object. For example, you could only define a certain function
    when an environment variable exists.

    In this example we will set up some notifications for when
    the bot joins and leaves a guild and react to messages from users
    who have the keyword "yay" in their text.
*/

function onLoad() {

    // SkittyBot.config has all the configuration settings you added via the web app.
    // Take a look at the settings_manager class to see all the values this can contain.

    const OWNER_ID = SkittyBot.config.owner_id;
    if (!OWNER_ID) {

        // SkittyBot has its owner logger method which includes all the typical Console() methods. You can use this to print colored timestamped logs.
        SkittyBot.logger.error(`I don't know who my owner is!`);

        return;
    }

    // SkittyBot.client is where you can get a handle on the discord.js client
    // discord.js is outside scope of this example so I will assume you are already familiar with it.

    let getOwner = () => {
        const OWNER_CHANNEL = SkittyBot.client.users.get(OWNER_ID);
        if (!OWNER_CHANNEL) {
            SkittyBot.logger.error(`I can't find my owner!`);
            return false;
        }
        return OWNER_CHANNEL;
    };

    SkittyBot.onGuildJoin(guild => {
        // onGuildJoin registers a callback function that is executed in the event that the bot joins a guild. It returns the guild it left.
        // This is exactly like using Discord.client.on(Discord.events.GUILD_JOIN, guild => { /* noop */ })
        let owner = getOwner();
        if (owner) owner.send(`Whoa, someone just invited me to ${guild.name}!`);
    });

    SkittyBot.onGuildLeave(guild => {
        // You can do the same for events where the bot leaves a guild.
        let owner = getOwner();
        if (owner) owner.send(`I've just been removed from ${guild.name}!`);
    });

    // You can also create message listeners yourself which will allow you to process messages that aren't commands.
    // this is useful in the event that you would like to create some kind of auto-reaction.
    // This is exactly like doing Discord.client.on(Discord.events.MESSAGE_CREATE, msg => { /* noop */ });
    // There is also SkittyBot.rootDispatch() which works exactly like this except it will also provide bot messages.
    SkittyBot.onUserMessage(msg => {
        // respond to anyone who has "yay" anywhere in their text.
        let content = msg.content.toLowerCase();
        if (content.indexOf('yay') !== -1) {
            msg.channel.send(`Yay!`);
        }
    });
}

/**
 * CONFIGURATION OPTIONS
 *
 * This is the command configuration object which contains the command definitions for SkittyBot.
 * This object is always passed to the global SkittyBot.registerPlugin function to make them live and ready to use.
 *
 * The configuration object can have some properties that tell the bot what to do with these commands.
 *
 * @example
 *
 *  let commands = [
 *      {
 *          // [Required] The command name people will use when executing your function. This cannot have spaces.
 *          command: 'commandname',
 *          // [optional] Any aliases for this command. This cannot have spaces.
 *          aliases: [`ex1`],
 *          // [optional] Any examples of how to use this command
 *          example: [`ex1 param1 param2`],
 *          // [optional] Your commands description
 *          description: [`Simple command example showing how to use the plugin API.`],
 *          // [optional] An array of permissions required by guild members in order to use this command.
 *          member: ['MANAGE_MESSAGES'],
 *          // [optional] An array of permissions required by SkittyBot to execute this command in a guild channel.
 *          client: ['MANAGE_MESSAGES'],
 *          // [optional] Your command function reference. This is optional because sometimes you can create a message listener which handles this function elsewhere but you still want it listed in the bots web application.
 *          process: Function,
 *          // [optional] Time in milliseconds a user must wait before being able to use this command again.
 *          throttle: 1000,
 *          // [optional] Tell the bot whether this command is for the bot owner only or public.
 *          owner: true,
 *          // [optional] Tells the bot that this command cannot be disabled by commands like guilddisablecommand and similar commands.
 *          noDisable: true,
 *          // [optional] Tells the bot it is a private command and the command will only work in places where the owner runs the command "toggleprivatecommand <command name>". The owner can still execute private commands. Private commands don't show up in the web app help section.
 *          private: true,
 *          // [optional] Tells the bot that the command should only run in guilds, no DMs.
 *          guildOnly: true,
 *          // [optional] Tells the bot whether the command is a NSFW command to avoid using it in SFW channels
 *          nsfw: true
 *      },
 *      {
 *          ...
 *      }
 *  ];
 *
 *  let configuration = {
 *      // [optional] Tells the bot it is a private plugin and the command within it will not show up in the web app help section.
 *      private: true,
 *      // [optional] Tells the bot that this command cannot be disabled by commands like guilddisablecommand and similar commands.
 *      noDisable: true,
 *      // [optional] A function reference to your plugins startup routine. Asynchronous functions will stop the bots plugin loading process until it has returned. Once an async function completed, the bot will process the commands array.
 *      onload: Function,
 *      // [optional] A function reference to your plugins unloading routine. Works similarly to onload. Async functions will stop the bot from proceeding with the unloading process until this function completes.
 *      onunload: Function,
 *      // [required] An array of objects with your command definitions, their descriptions, aliases, examples etc...
 *      commands: Array<Object>
 *  }
 *
 *  // Finally feed the configuration object to the bot to begin the plugin registration routine.
 *  SkittyBot.registerPlugin(configuration);
 */

// Example configuration in action
const configuration = {
    onload: onLoad,
    // Command definitions
    commands: [
        {
            command: 'example1',
            aliases: `ex1`,
            example: `ex1 param1 param2`,
            description: `Simple command example showing how to use the plugin API.`,
            process: example1,
            throttle: 1000
        },
        {
            command: 'example2',
            aliases: [
                'ex2',
                'examp2'
            ],
            example: [
                `example2 1 2 3`,
                `ex2 test`
            ],
            description: [
                `This is example2`,
                `Test it out.`
            ],
            process: (msg, args) => {
                // Add some parameter.
                example2(msg, args, 'with additional paramter!');
            },
            throttle: 15000
        },
        {
            command: 'queuedcommand',
            aliases: [
                'qc'
            ],
            example: [
                `qc 1 2 3`
            ],
            description: [
                `This shows hwo the queue works.`,
                `Try running it more than 3 times.`
            ],
            process: (msg, args) => {
                // SkittyBot can queue expensive commands which can take a long time to execute.
                // By default it is set to only allow 3 expensive commands to run at any given time.
                // You can change that in the skittybot.js file inside the API class.
                SkittyBot.queue.queue(queuedFunction, msg, args);
            }
        }
    ]
};

// Finally feed the configuration object to the bot to begin the plugin registration routine.
SkittyBot.registerPlugin(configuration);
